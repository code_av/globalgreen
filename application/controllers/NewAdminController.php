<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NewAdminController extends CI_Controller
{
//functions for calling pages
    function __construct()
    {
        //load database in autoload libraries
        parent::__construct();
        $this->load->model('User_model');
    }

    public function adminDashboard()
    {
        //for Counting number of Products
        $this->load->model('Product_model');
        $products=$this->Product_model->fetch_productId();
        $count=0;
        foreach ($products as $pro)
        {
            $count++;
        }
        $data['count']=$count;
        //for counting number of new orders
        $this->load->model('Order_model');
        $order_details=$this->Order_model->fetch_orders();
        $count_orders=0;
        foreach ($order_details as $pro)
        {
            if($pro->order_status==0) {
                $count_orders++;
            }
        }
        $data['count_orders']=$count_orders;

        //for client details to send on dashboard
        $this->load->model('Order_model');
        $client_details=$this->Order_model->fetch_orders();
        $data['client_details']=$client_details;


        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/dashboard', $data);
        $this->load->view('new-admin/ajax-footer');
    }



    public function Category()
    {
        $this->load->model('Category_model');
        $category = $this->Category_model->fetchCat();
        $data['category'] = $category;

        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/category', $data);
    }

    public function submitCategory()
    {
        $this->load->helper('new_helper');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('categoryname', 'category name', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', '* marked feilds are requireds');
            redirect_back();

        } else {


            $this->session->set_flashdata('success', 'Category Added');
            $this->load->model('Category_model');
            $this->load->helper('new_helper');
            $category = $this->Category_model->add();
            redirect_back();
            //$category=new Category_model;

        }
    }
    public function subCategory()
    {
        $this->load->model('Category_model');
        $category = $this->Category_model->fetch();
        $data['category'] = $category;

        //for details of category and sub category
        $this->load->model('Sub_category_model');
        $sub_category = $this->Sub_category_model->fetch();
        $data['sub_category'] = $sub_category;

        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/sub-category', $data);
    }

    public function submitSubCategory()
    {
        $this->load->helper('new_helper');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('subcategoryname', 'Sub category name', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', '* marked feilds are requireds');
            redirect_back();

        } else {
            $this->session->set_flashdata('success', 'Sub Category Added.');
            $this->load->model('Sub_category_model');
            $category = $this->Sub_category_model->add();
            redirect_back();
        }
    }

    public function addDistributor()
    {
        $this->load->model('Distributor_model');
        $distributor = $this->Distributor_model->fetch();
        $data['distributor'] = $distributor;


        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/distributor', $data);
    }

    public function submitDistributor()
    {
        $this->load->helper('new_helper');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('distributorname', 'Distributor name', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', '* marked feilds are requireds');
            redirect_back();

        } else {
        $this->session->set_flashdata('success', 'Distributor Added');
        $this->load->model('Distributor_model');
        $add = $this->Distributor_model->add();
        redirect_back();
    }}

    public function addProduct()
    {

            $this->load->model('Category_model');
            $category = $this->Category_model->fetch();
            $data['category'] = $category;

            $this->load->model('Distributor_model');
            $distributor = $this->Distributor_model->fetch();
            $data['distributor'] = $distributor;

            $this->load->model('Product_model');
            $product_details = $this->Product_model->fetch();
            $data['product_details'] = $product_details;

            $this->load->view('new-admin/header');
            $this->load->view('new-admin/menu');
            $this->load->view('new-admin/product', $data);
            $this->load->view('new-admin/ajax-footer');


    }
    public function get_sub_category()
    {
        //$id = $this->input->post('category');
        $this->load->model('Sub_category_model');
        $id = $this->uri->segment(2);
        $sub_category = $this->Sub_category_model->fetch_sub($id);
        $data['sub_category'] = $sub_category;
        //var_dump($sub_category);
        header('Content-Type: application/json');
        echo json_encode($sub_category);
        return;
    }

    public function submitProduct()
    {
        $this->load->helper('new_helper');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('productcode', 'productcode', 'required');
        $this->form_validation->set_rules('productname', 'product  name', 'required');
        $this->form_validation->set_rules('productdescription', 'product description', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', '* marked feilds are requireds');
            redirect('product');

        } else {


            $this->load->model('Product_model');
            $product_id = $this->Product_model->add();
            $this->load->model('Product_image_model');
            $config['upload_path'] = 'public/upload/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 5120;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;
            $count = count($_FILES['product_img']['size']);
            foreach ($_FILES as $key => $value) {
                for ($s = 0; $s <= $count - 1; $s++) {

                    $_FILES['file']['name'] = time() . $value['name'][$s];
                    $_FILES['file']['type'] = $value['type'][$s];
                    $_FILES['file']['tmp_name'] = $value['tmp_name'][$s];
                    $_FILES['file']['error'] = $value['error'][$s];
                    $_FILES['file']['size'] = $value['size'][$s];
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('error', 'Required Image Dimensions Not Matched.');

                    } else {
                        $data = $this->upload->data();
                        //var_dump($data);
                        //echo 'success';
                        $result_set = $this->Product_image_model->setSlider('public/upload/' . $data['file_name'], $product_id);
                    }
                }
            }
            $this->session->set_flashdata('success','product added successfully');
            redirect('product');
        }
    }

    public function addProductImages()
    {
        $this->load->model('Product_image_model');
        $this->load->model('Product_model');
        $products= $this->Product_model->fetch_productId();
        $data['products']=$products;
        //var_dump($products);
        $images = $this->Product_image_model->getSlider();
        $data['images'] = $images;
        $this->load->view('admin/header');
        $this->load->view('admin/add-product-images',$data);
        $this->load->view('admin/footer');

    }
    //saving produvt images..
    public function submitImage()
    {
        $this->load->helper('new_helper');

        $this->load->model('Product_image_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Image title', 'required');
        if ($this->form_validation->run() === false) {
            $data = $this->SliderModel->getSlider();
            $data['data'] = $data;

            $this->load->view('admin/header');
            $this->load->view('admin/add-product-image',$data);
            $this->load->view('admin/footer');
        }
        else {
            if (file_exists("public/upload/" . $_FILES["logo"]["name"]))
            {
                echo $_FILES["logo"]["name"] . " already exists. ";
            }
            // uploading logo
            // if logo image is selected
            $logo_url = null;
            if ( ! empty($_FILES['logo']['name'])) {
                $file_name = str_replace(' ', '-', $this->input->post('name'));
                $file_name = time().$file_name;
                $config['upload_path']          = 'public/upload/';
                $config['allowed_types']        = 'jpg|png|jpeg|gif';
                $config['max_size']             = 5120;
                $config['max_width']            = 2000;
                $config['max_height']           = 2000;
                $config['file_name']            = $file_name.'.'.pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);

                $logo_url = 'public/upload/'.$file_name.'.'.pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('logo'))
                {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('error', 'Required Image Dimensions Not Matched.');
                    redirect_back();
                    return;
                }
                else
                {
                    $data = array('upload_data' => $this->upload->data());
                }
            }


            // insert data into database
            $this->Product_image_model->setSlider($logo_url);
            $this->session->set_flashdata('success', 'New Image has been successfully added.');
            redirect('add-product-images');
        }
    }
    public function deleteImage()
    {
        $this->load->helper('new_helper');
        $image_id = $this->uri->segment(2);
        $this->db->delete('product_gallery', array('gallery_id' => $image_id));
        $this->session->set_flashdata('success', 'Image Successfully deleted.');
        //redirect($url);

        redirect_back();

    }
    public function viewDetails($parameter1)
    {
        $pro_id = $parameter1;
        $this->load->model('Product_model');
        $products = $this->Product_model->fetchProductDetails($pro_id);
        $data['products'] = $products;
            $this->load->view('new-admin/header');
            $this->load->view('new-admin/menu');
            $this->load->view('new-admin/product-details',$data);

    }
        //edit update and delete functions
    public function editCategory()
    {
        $category_id=$this->uri->segment(2);
        //echo $category_id;
        $this->load->model('Category_model');
        $category_details=$this->Category_model->fetch_by_categoryid($category_id);
        $data['category_details']=$category_details;
        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/edit-category',$data);
    }
    public function updateCategory()
    {
        $this->load->model('Category_model');
        $this->Category_model->update();
        $this->session->set_flashdata('success', 'category Updated.');
        redirect('ncategory');
    }
    public function editSubcategory()
    {
        $sub_category_id=$this->uri->segment(2);
        //echo $category_id;
        $this->load->model('Sub_category_model');
        $sub_category_details=$this->Sub_category_model->fetch_by_sub_category_id($sub_category_id);
        $data['sub_category_details']=$sub_category_details;
        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/edit-sub-category',$data);
    }
    public function updateSubcategory()
    {
        $this->load->model('Sub_category_model');
        $this->Sub_category_model->update();
        $this->session->set_flashdata('success', 'sub category Updated.');
        redirect('sub-category');
    }
    public function editDistributor()
    {
        $distributor_id=$this->uri->segment(2);
        //echo $category_id;
        $this->load->model('Distributor_model');
        $distributor_details=$this->Distributor_model->fetch_by_distributorid($distributor_id);
        $data['distributor_details']=$distributor_details;
        //var_dump($sub_category_details);
        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/edit-distributor',$data);
    }
    public function updateDistributor()
    {
        $this->load->model('Distributor_model');
        $this->Distributor_model->update();
        $this->session->set_flashdata('success', 'Distributor Updated.');
        redirect('distributor');
    }
    public function deleteDistributor()
    {
        $Distributor_id=$this->uri->segment(2);
        $this->load->model('Distributor_model');
        $this->Distributor_model->delete($Distributor_id);
        $this->session->set_flashdata('success', ' Distributor Successfully deleted.');
        redirect('distributor');

    }
    public function editProduct()
    {
        $this->load->model('Distributor_model');
        $distributor=$this->Distributor_model->fetch();
        $data['distributor'] = $distributor;
        $product_id=$this->uri->segment(2);
        //echo $category_id;
        $this->load->model('Product_model');
        $products = $this->Product_model->fetchProductDetails($product_id);
        $data['products'] = $products;

        $product_details=$this->Product_model->fetch_product_by_productid($product_id);
        $data['product_details']=$product_details;
        //var_dump($product_details);
        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/edit-product',$data);
        $this->load->view('new-admin/ajax-footer');
    }


    public function updateProduct()
    {
        $this->load->helper('new_helper');
        $product_id=$this->input->post('productid');
       // echo $product_id;
        $this->load->model('Product_model');
        $product_details=$this->Product_model->update($product_id);
        $data['product_details']=$product_details;
        //var_dump($product_details);
        //die();
            $this->load->model('Product_image_model');
            $config['upload_path'] = 'public/upload/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 5120;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;
            $count = count($_FILES['product_img']['size']);
            //var_dump($_FILES['product_img']['name']);
            //return;
            foreach ($_FILES as $key => $value) {
                for ($s = 0; $s <= $count - 1; $s++) {

                    $_FILES['file']['name'] = time() . $value['name'][$s];
                    $_FILES['file']['type'] = $value['type'][$s];
                    $_FILES['file']['tmp_name'] = $value['tmp_name'][$s];
                    $_FILES['file']['error'] = $value['error'][$s];
                    $_FILES['file']['size'] = $value['size'][$s];
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('error', 'Required Image Dimensions Not Matched.');
                        //var_dump($error);
                    } else {
                        $data = $this->upload->data();
                        //var_dump($data);
                        //echo 'success';
                        $result_set = $this->Product_image_model->setSlider('public/upload/' . $data['file_name'], $product_id);
                    }
                }

        }

            $this->session->set_flashdata('success','product updated');
        redirect ('product');

    }
    public function deleteProduct()
    {
        $product_id=$this->uri->segment(2);
        $this->load->model('Product_model');
        $this->Product_model->delete($product_id);
        $this->session->set_flashdata('success', ' Product deleted Successfully.');
        redirect('product');
    }
    public function editGallery()
    {

        $this->load->model('Product_model');
        $product=$this->Product_model->fetch_productId();
        $data['product']=$product;
        $this->load->view('admin/header');
        $this->load->view('admin/edit-gallery',$data);
        $this->load->view('admin/footer');
    }
//edit update and delete functions end
    public function activateCategory()
    {
        $id=$this->uri->segment(2);
        $this->load->model('Category_model');
        $this->Category_model->activateCategory($id);
        $this->load->model('Product_model');
        $this->Product_model->activateProduct($id);
        $this->session->set_flashdata('success','category Activated');
        redirect('category');

    }
    public function deactivateCategory()
    {
        $id=$this->uri->segment(2);
        $this->load->model('Category_model');
        $this->Category_model->deactivateCategory($id);
        $this->load->model('Product_model');
        $this->Product_model->deactivateProduct($id);
        $this->load->model('Sub_category_model');
        $this->Sub_category_model->deactivateSubcategoryByCategoryId($id);
        $this->session->set_flashdata('success','category De-activated');
        redirect('category');

    }
    public function activateProduct()
    {
        $this->load->helper('new_helper');
        $product_id = $this->uri->segment(2);
        $this->load->model('Product_model');
        $product_detail = $this->Product_model->fetch_product_by_productid($product_id);
        $category_id = "";
        $sub_category_id = "";
        foreach ($product_detail as $data) {
            $category_id = $data->category_id;
            $sub_category_id = $data->sub_category_id;
        }

        //fetch sub category status
        $this->load->model('Sub_category_model');
        $sub_category_status = $this->Sub_category_model->fetch_by_sub_category_id($sub_category_id);
        foreach ($sub_category_status as $data) {
            if ($data->sub_category_status == 0) {
                $this->session->set_flashdata('error', 'you can not activate product without activating its sub category');
                redirect_back();
                return;
            } else {
                $this->load->model('Category_model');
                $category_status = $this->Category_model->fetch_by_categoryid($category_id);
                foreach ($category_status as $data) {
                    if ($data->status == 0) {
                        $this->session->set_flashdata('error', 'you can not activate product without activating its category');
                        redirect_back();
                    } else {
                        $this->Product_model->activateProductByProductId($product_id);
                        $this->session->set_flashdata('success', 'Product Activated');
                        redirect('product');
                    }
                }

            }
        }
    }

    public function deactivateProduct()
    {
        $id=$this->uri->segment(2);
        $this->load->model('Product_model');
        $this->Product_model->deactivateProductByProductId($id);
        $this->session->set_flashdata('success','Product De-activated');
        redirect('product');

    }
    public function activateSubcategory()
    {
        $this->load->helper('new_helper');
        $sub_category_id=$this->uri->segment(2);
        $this->load->model('Sub_category_model');
        $this->load->model('Category_model');
        $this->load->model('Product_model');
        $category_status=$this->Category_model->fetch_by_subcategoryid($sub_category_id);
        if($category_status[0]->status==0)
        {
            $this->session->set_flashdata('error','category of this sub category is not activated. first activate the category');
            redirect_back();
        }
        else{
            $this->Sub_category_model->activate($sub_category_id);
            $this->Product_model->activateProductBySubcategoryId($sub_category_id);
            $this->session->set_flashdata('success','Sub Category Activated.');
            redirect_back();
        }
        /*
        $this->Sub_category_model->activate($sub_category_id);
        $this->session->set_flashdata('success','Sub Category De-activated');*/

    }
    public function deactivateSubcategory()
    {
        $this->load->helper('new_helper');
        $sub_category_id=$this->uri->segment(2);
        $this->load->model('Sub_category_model');
        $this->load->model('Product_model');
        $this->Sub_category_model->deactivate($sub_category_id);
        $this->Product_model->deactivateProductBySubcategoryId($sub_category_id);
        $this->session->set_flashdata('success','Sub Category De-activated.');
        redirect_back();

    }

   public function viewOrder()
   {
       $order_id=$this->uri->segment(2);
       $this->load->model('Order_details_model');
       $this->load->model('Order_model');
       $client_details=$this->Order_model->fetchByOrderId($order_id);
       $order_detail=$this->Order_details_model->fetchByOrderId($order_id);
       $data['order_detail']=$order_detail;
       $data['client_detail']=$client_details;
       $this->load->view('new-admin/header');
       $this->load->view('new-admin/menu');
        $this->load->view('new-admin/view-order',$data);

   }
   public function deleteOrder()
   {
       $id=$this->uri->segment(2);
       $this->load->helper('new_helper');
       $this->db->delete('orders', array('order_id' => $id));
       $this->db->delete('order_details', array('order_id' => $id));
       $this->session->set_flashdata('success', ' Order successfully deleted.');
       redirect_back();
   }
    public function viewAllProduct()
    {
        $this->load->model('Category_model');
        $category = $this->Category_model->fetch();
        $data['category'] = $category;
            $this->load->view('new-admin/header');
            $this->load->view('new-admin/menu');
            $this->load->view('new-admin/view-all-product',$data);
            $this->load->view('new-admin/ajax-footer');
    }

    public function viewProduct()
    {
        $this->load->model('Product_model');
        $sub_category_id=$this->input->post('subcategory');
        $category_id=$this->input->post('category');
        if($category_id==0)
        {
            $this->session->set_flashdata('error','please select category');
            redirect_back();
        }
        if($sub_category_id=="")
        {
            $pro=$this->Product_model->fetch_by_categoryid($category_id);
            $this->load->model('Category_model');
            $category = $this->Category_model->fetch();
            $data['category'] = $category;
            $data['products'] = $pro;
            $this->load->view('new-admin/header');
            $this->load->view('new-admin/menu');
            $this->load->view('new-admin/view-all-product',$data);
            $this->load->view('new-admin/ajax-footer');

        }
        else {
            //echo $sub_category_id;
            $products = $this->Product_model->fetch_by_subcategoryid($sub_category_id);
            $this->load->model('Category_model');
            $category = $this->Category_model->fetch();
            $data['category'] = $category;
            $data['products'] = $products;

            $this->load->view('new-admin/header');
            $this->load->view('new-admin/menu');
            $this->load->view('new-admin/view-all-product', $data);
            $this->load->view('new-admin/ajax-footer');
        }

    }
    public function changeOrderToSeen()
    {
        $this->load->helper('new_helper');
        $order_id=$this->uri->segment(2);
        $this->load->model('Order_model');
        $this->Order_model->changeToSeen($order_id);
        $this->session->set_flashdata('success','Order Moved To Viewed List.');
        redirect_back();

    }
    public function changeOrderToUnseen()
    {
        $this->load->helper('new_helper');
        $order_id=$this->uri->segment(2);
        $this->load->model('Order_model');
        $this->Order_model->changeToUnseen($order_id);
        $this->session->set_flashdata('success','Order Moved To New Orders List.');
        redirect_back();

    }

    public function changePassword()
    {
        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/change-password');
    }
    public function sliderImage()
    {
        $this->load->model('Main_slider_model');
        $header_slider=$this->Main_slider_model->get_slider();
        $this->load->model('Sub_slider_model');
        $slider=$this->Sub_slider_model->get_sub_slider();
        $data['header_slider'] = $header_slider;
        $data['slider'] = $slider;
        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/change-slider-image', $data);
        $this->load->view('new-admin/footer');
    }
    public function addHeaderSliderImage()
    {
        try {
            $this->load->helper('new_helper');
            $this->load->model('Main_slider_model');
            $image_url = null;
            if (!empty($_FILES['slider_image']['name'])) {
                $fname = $_FILES['slider_image']['name'];
                $file_name = str_replace(' ', '-', $this->input->post('slider_image'));
                $file_name = time() . $file_name;
                $config['upload_path'] = 'public/slider/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 10024;
                $config['max_width'] = 1960;
                $config['max_height'] = 650;
                $config['file_name'] = $file_name . '.' . pathinfo($_FILES['slider_image']['name'], PATHINFO_EXTENSION);
                $image_url = 'public/slider/' . $file_name . '.' . pathinfo($_FILES['slider_image']['name'], PATHINFO_EXTENSION);
                $this->load->library('upload',$config);

                if ($this->upload->do_upload('slider_image')) {
                    $data = array('upload_data' => $this->upload->data());
                    $this->Main_slider_model->addSlider($image_url);
                    $this->session->set_flashdata('success', 'Slider Image Added Successfully.');
                    redirect_back();
                } else
                {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect_back();                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('error', 'Some Error occured ! Try Again');
        }


    }

    public function addSubSlider()
    {
        try {
            $this->load->helper('new_helper');
            $this->load->model('Sub_slider_model');
            $image_url = null;
            if (!empty($_FILES['slider_image']['name'])) {
                $fname = $_FILES['slider_image']['name'];
                $file_name = str_replace(' ', '-', $this->input->post('slider_image'));
                $file_name = time() . $file_name;
                $config['upload_path'] = 'public/slider/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 10024;
                $config['max_width'] = 1960;
                $config['max_height'] = 250;
                $config['file_name'] = $file_name . '.' . pathinfo($_FILES['slider_image']['name'], PATHINFO_EXTENSION);
                $image_url = 'public/slider/' . $file_name . '.' . pathinfo($_FILES['slider_image']['name'], PATHINFO_EXTENSION);
                $this->load->library('upload',$config);

                if ($this->upload->do_upload('slider_image')) {
                    $data = array('upload_data' => $this->upload->data());
                    $this->Sub_slider_model->addSubSlider($image_url);
                    $this->session->set_flashdata('success', 'Slider Image Added Successfully.');
                    redirect_back();
                } else
                {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect_back();
                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('error', 'Some Error occured ! Try Again');
        }


    }

    public function deleteSlider()
    {
        try {
            $this->load->helper('new_helper');
            $id = $this->uri->segment(2);
            $this->db->delete('main_slider', array('slider_id' => $id));
            $this->session->set_flashdata('success', 'Slider Image Successfully Deleted.');
            redirect_back();

        } catch (Exception $e) {
            $this->session->set_flashdata('error', 'Some Error Occurred, Please Try Again.');
            redirect_back();
        }
    }

    public function deleteSubSlider()
    {
        try{
            $this->load->helper('new_helper');
            $id=$this->uri->segment(2);
            $this->db->delete('sub_slider', array('sub_slider_id' => $id));
            $this->session->set_flashdata('success','Slider Image Successfully Deleted.');
            redirect_back();

        }
        catch(Exception $e)
        {
            $this->session->set_flashdata('error','Some Error Occurred, Please Try Again.');
            redirect_back();
        }


    }

    public function projects()
    {
        $this->load->helper('new_helper');
        $this->load->model('Project_model');
        $projects=$this->Project_model->get_project();
        $data['projects']=$projects;
        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/Project', $data);
        $this->load->view('new-admin/footer');
    }

    public function addProject()
    {
        try {
            $this->load->helper('new_helper');
            $this->load->model('Project_model');
            $image_url = null;
            if (!empty($_FILES['project_image']['name'])) {
                $fname = $_FILES['project_image']['name'];
                $file_name = str_replace(' ', '-', $this->input->post('project_image'));
                $file_name = time() . $file_name;
                $config['upload_path'] = 'public/projects/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 2048;
                $config['max_width'] = 400;
                $config['max_height'] = 300;
                $config['file_name'] = $file_name . '.' . pathinfo($_FILES['project_image']['name'], PATHINFO_EXTENSION);
                $image_url = 'public/projects/' . $file_name . '.' . pathinfo($_FILES['project_image']['name'], PATHINFO_EXTENSION);
                $this->load->library('upload',$config);

                if ($this->upload->do_upload('project_image')) {
                    $data = array('upload_data' => $this->upload->data());
                    $this->Project_model->addProject($image_url);
                    $this->session->set_flashdata('success', 'Project Added Successfully.');
                    redirect_back();
                } else
                {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect_back();                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('error', 'Some Error occured ! Try Again');
        }


    }

    public function deleteProject()
    {
        try {
            $this->load->helper('new_helper');
            $id = $this->uri->segment(2);
            $this->db->delete('projects', array('project_id' => $id));
            $this->session->set_flashdata('success', 'Project Deleted Successfully.');
            redirect_back();
        } catch (Exception $e) {
            $this->session->set_flashdata('error', 'Some Error Occurred, Please Try Again.');
            redirect_back();
        }
    }

    public function manageServices()
    {
        $this->load->model('Service_model');
        $services=$this->Service_model->get_Services();
        $data['services']=$services;
        $this->load->view('new-admin/header');
        $this->load->view('new-admin/menu');
        $this->load->view('new-admin/services', $data);
        $this->load->view('new-admin/footer');
    }

    public function addService()
    {
        try {
            $this->load->helper('new_helper');
            $this->load->model('Service_model');
            $image_url = null;
            if (!empty($_FILES['service_icon']['name'])) {
                $fname = $_FILES['service_icon']['name'];
                $file_name = str_replace(' ', '-', $this->input->post('service_icon'));
                $file_name = time() . $file_name;
                $config['upload_path'] = 'public/services/';
                $config['allowed_types'] = 'jpg|png|jpeg';
                $config['max_size'] = 1024;
                $config['max_width'] = 200;
                $config['max_height'] = 200;
                $config['file_name'] = $file_name . '.' . pathinfo($_FILES['service_icon']['name'], PATHINFO_EXTENSION);
                $image_url = 'public/services/' . $file_name . '.' . pathinfo($_FILES['service_icon']['name'], PATHINFO_EXTENSION);
                $this->load->library('upload',$config);

                if ($this->upload->do_upload('service_icon')) {
                    $data = array('upload_data' => $this->upload->data());
                    $this->Service_model->addService($image_url);
                    $this->session->set_flashdata('success', 'Service Added Successfully.');
                    redirect_back();
                } else
                {
                    $this->session->set_flashdata('error', $this->upload->display_errors());
                    redirect_back();                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('error', 'Some Error occured ! Try Again');
        }


    }

    public function deleteService()
    {
        try {
            $this->load->helper('new_helper');
            $id = $this->uri->segment(2);
            $this->db->delete('services', array('service_id' => $id));
            $this->session->set_flashdata('success', 'Service Removed Successfully.');
            redirect_back();
        } catch (Exception $e) {
            $this->session->set_flashdata('error', 'Some Error Occurred, Please Try Again.');
            redirect_back();
        }
    }
}

?>