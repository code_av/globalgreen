<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PagesController extends CI_Controller
{
//functions for calling pages
    public function index()
    {
        $this->load->model('Main_slider_model');
        $this->load->model('Sub_slider_model');
        $this->load->model('Category_model');
        $main_slider=$this->Main_slider_model->get_slider();
        $sub_slider=$this->Sub_slider_model->get_sub_slider();
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $data['main_slider']= $main_slider;
        $data['sub_slider']= $sub_slider;
        $this->load->helper('url');
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('index');
        $this->load->view('footer');
    }
    public function contact()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
       $this->load->helper('url');
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('contact');
        $this->load->view('footer');
    }

    public function aboutus()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('about-us-one');
        $this->load->view('footer');
    }

    public function projects()
    {
        $this->load->model('Project_model');
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $projects=$this->Project_model->get_project();
        $data['category'] = $category;
        $data['projects'] = $projects;
        $this->load->helper('url');
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('projects',$data);
        $this->load->view('footer');
    }

    public function services()
    {
        $this->load->model('Category_model');
        $this->load->model('Service_model');
        $category=$this->Category_model->fetch();
        $services=$this->Service_model->get_services();
        $data['category'] = $category;
        $data['services'] = $services;
        $this->load->helper('url');
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('services');
        $this->load->view('footer');
    }

    public function wpcDoor()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $this->load->helper('url');
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('wpcDoor');
        $this->load->view('side-bar');
        $this->load->view('footer');
    }

//end functions for calling pages
    public function mail()
    {

        $this->load->helper('url');
           $subject = "messages by customers";
           $name = $this->input->post('fname');
           $email = $this->input->post('email');
           $message = $this->input->post('message');
           $to   		= 'orders@globalgreeneco.com';//replace with your email

           $message = "Name:".$name."\r\nEmail:".$email."\r\nsubject:".$subject."\r\nMessage: ".$message;

           if(mail($to, $subject, $message)) {
                 echo "<script>alert('Your Message has been received. We will contact you soon.')

                             window.location.href='index';</script>";
           }
           else{
                 echo "<script>alert('There is some problem in sending message, please contact us on mobile number.')
                            window.location.href='index';</script>";
           }

           
    }


    public function mail_orders()
    {
        $this->load->helper('new_helper');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('address', 'address', 'required|max_length[100]');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', 'address should be less than 100 words');
            $this->load->model('Category_model');
            $category=$this->Category_model->fetch();
            $data['category'] = $category;
            $this->load->view('header');
            $this->load->view('menu',$data);
            $this->load->view('cart');
            $this->load->view('footer');
        }
        $this->form_validation->set_rules('message', 'Message', 'required|max_length[200]');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', 'message should be less than 200 words');
            $this->load->model('Category_model');
            $category=$this->Category_model->fetch();
            $data['category'] = $category;
            $this->load->view('header');
            $this->load->view('menu',$data);
            $this->load->view('cart');
            $this->load->view('footer');
            }
        $this->form_validation->set_rules('phone', 'phone', 'required|regex_match[/^[0-9]{10}$/]|min_length[10]|max_length[10]');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', 'only numbers are allowed in mobile number.');
            $this->load->model('Category_model');
            $category=$this->Category_model->fetch();
            $data['category'] = $category;
            $this->load->view('header');
            $this->load->view('menu',$data);
            $this->load->view('cart');
            $this->load->view('footer');
        } else {
            //on suceess
            $this->load->helper('url');
            $this->load->model('Order_model');
            $order_id = $this->Order_model->add_orders();
            //echo $order_id;
            $this->load->model('Order_details_model');
            $this->Order_details_model->add_order_detail($order_id);
                $subject = "cart orders";
                $name = $_POST['fname'];
                $phone = $_POST['phone'];
                $message = $_POST['message'];
                $cart_items="";
                $session_data = $this->session->userdata('cart');
                    if(count($session_data) > 0) {
                        $count = 0;
                        foreach ($session_data as $key => $value) {
                            $cart_items = $cart_items . ',' . $value["product_name"];
                        }
                    }
                $to= 'orders@globalgreeneco.com';//replace with your email
                $message = "Name:".$name."\r\nphone number:".$phone."\r\nsubject:".$subject."\r\nMessage: ".$message."\r\nitems:".$cart_items;

                if(mail($to, $subject, $message)) {
                    $this->session->sess_destroy();
                    $this->session->set_flashdata('success', 'Your Message has been received. We will contact you soon.');
                    redirect('index');
                    return;
                     }
                else{
                    $this->session->set_flashdata('error', 'There is some problem in sending message, please contact us on mobile number.');
                    redirect('index');
                    return;
                    }
         }
        }
    public function contactMail()
    {
        $this->load->helper('url');
           $subject = "message from contact us form";
           $name = $this->input->post('username');
           $email = $this->input->post('email');
           $subject = $this->input->post('subject');
           $message = $this->input->post('message');
           $to = 'orders@globalgreeneco.com';//replace with your email

           $message = "Name:".$name."\r\nEmail:".$email."\r\nsubject:".$subject."\r\nMessage: ".$message;
  if(mail($to, $subject, $message)) {
                 echo "<script>alert('Your Message has been received. We will contact you soon.')

                             window.location.href='contact';</script>";
           }
           else{
                 echo "<script>alert('There is some problem in sending message, please contact us on mobile number.')
                                window.location.href='contact';</script>";
           }
    }
    public function subCategoryDetails($parameter)
    {
        $cat_id = $this->uri->segment(3);
        $this->load->model('Product_model');
        $products = $this->Product_model->fetchProductDetailWithGallery($cat_id);
        $data['products'] = $products;
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $this->load->model('Sub_category_model');
        $id = $this->uri->segment(3);
        $sub_category=$this->Sub_category_model->fetch_sub($id);
        $data['sub_category'] = $sub_category;
        //for distributors
        $this->load->model('Distributor_model');
        $distributor=$this->Distributor_model->fetch();
        $data['distributor'] = $distributor;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('wpcDoor',$data);
        $this->load->view('side-bar',$data);
        $this->load->view('footer');

    }

    public function viewProductDetails($parameter1 , $parameter2)
    {
        $pro_id = $this->uri->segment(3);
        $cat_id = $this->uri->segment(4);
        $this->load->model('Product_model');
        $products = $this->Product_model->fetchProductDetails($pro_id);
        $data['products'] = $products;


        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;


        $this->load->model('Sub_category_model');
        $id = $this->uri->segment(2);
        $sub_category=$this->Sub_category_model->fetch_sub($cat_id);
        $data['sub_category'] = $sub_category;
        //for distributors
        $this->load->model('Distributor_model');
        $distributor=$this->Distributor_model->fetch();
        $data['distributor'] = $distributor;


        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('viewProductDetails',$data);
        $this->load->view('side-bar',$data);
        $this->load->view('footer');

    }

    public function viewSubCategoryProducts($parameter1 , $parameter2)
    {
        //echo $parameter;
        $sub_cat_id = $this->uri->segment(3);
        $cat_id = $this->uri->segment(4);
        $this->load->model('Product_model');
        $products = $this->Product_model->fetchSubCategoryProducts($sub_cat_id);
        $data['products'] = $products;
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $this->load->model('Sub_category_model');
        $sub_category=$this->Sub_category_model->fetch_sub($cat_id);
        $data['sub_category'] = $sub_category;
        //for distributors
        $this->load->model('Distributor_model');
        $distributor=$this->Distributor_model->fetch();
        $data['distributor'] = $distributor;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('subCategoryProducts',$data);
        $this->load->view('side-bar',$data);
        $this->load->view('footer');

    }

    public function viewDistributorProducts($parameter)
    {
        $id = $this->uri->segment(3);
        $this->load->model('Product_model');
        $products = $this->Product_model->fetchDistributorProducts($id);
        $data['products'] = $products;
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $this->load->model('Distributor_model');
        $distributor=$this->Distributor_model->fetch();
        $data['distributor'] = $distributor;
        $data['distributorId'] = $id;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('distributorProducts',$data);
        $this->load->view('distributorSideBar',$data);
        $this->load->view('footer');
    }

    //sub category with product overview
    public function subCategory()
    {
        //category for menu
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        //sub category for side bar according to category
        $this->load->model('Sub_category_model');
        $id = $this->uri->segment(2);
        $sub_category=$this->Sub_category_model->fetch_sub($id);
        $data['sub_category'] = $sub_category;
        //for distributors
        $this->load->model('Distributor_model');
        $distributor=$this->Distributor_model->fetch();
        $data['distributor'] = $distributor;
         //for product with images
        $this->load->model('Product_model');
        $products=$this->Product_model->products($id);
        $data['products'] = $products;

        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('products_overview',$data);
        $this->load->view('side-bar',$data);
        $this->load->view('footer');

    }
    public function product_details()
    {
        //category for menu
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        //sub category for side bar according to category
        $this->load->model('Sub_category_model');
        $sub_category_id=$this->uri->segment(2);
        $sub_category=$this->Sub_category_model->fetch_sub($sub_category_id);
        $data['sub_category'] = $sub_category;
        //for distributors
        $this->load->model('Distributor_model');
        $distributor=$this->Distributor_model->fetch();
        $data['distributor'] = $distributor;
        //to select products
        $product_id=$this->uri->segment(3);
        $this->load->model('Product_model');
        $products=$this->Product_model->fetch_product_by_productid($product_id);
        $data['products'] = $products;
        //to selct product images..
        $this->load->model('Product_image_model');
        $images=$this->Product_image_model->fetch_by_productid($product_id);
        $data['images'] = $images;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('products_details',$data);
        $this->load->view('side-bar',$data);
        $this->load->view('footer');
    }
    public function distributor()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;

        $this->load->model('Sub_category_model');
        $dist_id = $this->uri->segment(3);
        $sub_category=$this->Sub_category_model->fetch_sub($dist_id);
        $data['sub_category'] = $sub_category;
        //for distributors
        $this->load->model('Distributor_model');
        $distributor=$this->Distributor_model->fetch();
        $data['distributor'] = $distributor;

        $this->load->model('Product_model');
        $product=$this->product_model->fetch_distributor();

        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('wpcDoor');
        $this->load->view('side-bar',$data);
        $this->load->view('footer');

    }
    public function addTocart()
    {
        $this->load->helper('new_helper');
        $product_id=$this->uri->segment(2);
        $this->load->model('Product_model');
        $product_details=$this->Product_model->fetch_product_by_productid($product_id);
        $product_code="";
        $product_name="";
        /* $this->session->sess_destroy();*/
        $session_data = $this->session->userdata('cart');
        if(count($session_data) > 0)
        {
            foreach ($session_data as $key=>$value)
            {
                if($value["product_id"] == $product_id)
                {
                    unset($session_data[$key]);
                    $this->session->set_userdata('cart', $session_data);
                }
            }

        }

        $session_data = array();
        foreach ($product_details as $products)
        {
            $product_code=$products->product_code;;
            $product_name=$products->name;
        }

        $session_data = $this->session->userdata('cart');
        $session_data[] = array(
            'product_id'=>$product_id,
            'product_code'=> $product_code,
            'product_name'=> $product_name
        );

        $this->session->set_userdata("cart", $session_data);
        $sess = $this->session->userdata("cart");
        //var_dump($sess);
        $this->session->set_flashdata('success', 'product added to orders');

            redirect_back();

    }
    public function cart()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;

        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('cart');
        $this->load->view('footer');

    }
    public function removeCartItem($param)
    {
        $this->load->helper('new_helper');
        $p_id = $param;
        $session_data = $this->session->userdata('cart');
        if(count($session_data) > 0)
        {
            foreach ($session_data as $key=>$value)
            {
                if($value["product_id"] == $p_id)
                {
                    unset($session_data[$key]);
                    $this->session->set_userdata('cart', $session_data);
                }
            }
        }
        redirect_back();
    }
    public function modularKitchen()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;

        $this->load->model('Distributor_model');
        $distributor=$this->Distributor_model->fetch();
        $data['distributor'] = $distributor;

        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('modular-kitchen');
        $this->load->view('static-side-bar',$data);
        $this->load->view('footer');

    }
    public function ozone()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('ozone');
        $this->load->view('footer');
    }
    public function axilam()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('axilam');
        $this->load->view('footer');
    }
    public function alstone()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('alstone');
        $this->load->view('footer');
    }

    public function upvcDoorWindow()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('upvc-doors-windows');
        $this->load->view('footer');
    }

    public function wpcBoards()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('wpc-chaukhat-boards');
        $this->load->view('footer');
    }

    public function overviewModularKitchen()
    {
        $this->load->model('Category_model');
        $category=$this->Category_model->fetch();
        $data['category'] = $category;
        $this->load->view('header');
        $this->load->view('menu',$data);
        $this->load->view('overview-modular-kitchen');
        $this->load->view('footer');
    }


}
?>