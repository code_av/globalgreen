<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller
{
//functions for calling pages
    function __construct()
    {
        //load database in autoload libraries
        parent::__construct();
        $this->load->model('User_model');
    }

    public function adminDashboard()
    {
        /*$this->session->set_flashdata('success', 'Welcome to Global Green Eco Technologies');*/
        $this->load->model('Product_model');
        $products=$this->Product_model->fetch_productId();
        $count=0;
        foreach ($products as $pro)
        {
            $count++;
        }
        $data['count']=$count;

        $this->load->model('Order_details_model');
        $order_details=$this->Order_details_model->fetchOrderDetails();
        $count_orders=0;
        foreach ($order_details as $pro)
        {
            $count_orders++;
        }
        $data['count_orders']=$count_orders;

        $data['order_details']=$order_details;

        $this->load->view('admin/header');
        $this->load->view('admin/dashboard', $data);
        $this->load->view('admin/footer');
    }



    public function addCategory()
    {
        $this->load->model('Category_model');
        $category = $this->Category_model->fetchCat();
        $data['category'] = $category;


        $this->load->view('admin/header');
        $this->load->view('admin/add-category', $data);
        $this->load->view('admin/footer');
    }

    public function submitCategory()
    {
        $this->load->helper('new_helper');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('categorycode', 'category code', 'required');
        $this->form_validation->set_rules('categoryname', 'category name', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', '* marked feilds are requireds');
            redirect_back();

        } else {


            $this->session->set_flashdata('success', 'Category Added');
            $this->load->model('Category_model');
            $this->load->helper('new_helper');
            $category = $this->Category_model->add();
            redirect_back();
            //$category=new Category_model;

        }
    }
    public function addSubCategory()
    {
        $this->load->model('Category_model');
        $category = $this->Category_model->fetch();
        $data['category'] = $category;

        //for details of category and sub category
        $this->load->model('Sub_category_model');
        $sub_category = $this->Sub_category_model->fetch();
        $data['sub_category'] = $sub_category;

        $this->load->view('admin/header');
        $this->load->view('admin/add-sub-category', $data);
        $this->load->view('admin/footer');
    }

    public function submitSubCategory()
    {
        $this->load->helper('new_helper');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('subcategoryname', 'Sub category name', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', '* marked feilds are requireds');
            redirect_back();

        } else {
            $this->session->set_flashdata('success', 'Sub Category Added.');
            $this->load->model('Sub_category_model');
            $category = $this->Sub_category_model->add();
        }
    }

    public function addDistributor()
    {
        $this->load->model('Distributor_model');
        $distributor = $this->Distributor_model->fetch();
        $data['distributor'] = $distributor;


        $this->load->view('admin/header');
        $this->load->view('admin/add-distributor', $data);
        $this->load->view('admin/footer');
    }

    public function submitDistributor()
    {
        $this->load->helper('new_helper');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('distributorname', 'Distributor name', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', '* marked feilds are requireds');
            redirect_back();

        } else {
        $this->session->set_flashdata('success', 'Distributor Added');
        $this->load->model('Distributor_model');
        $add = $this->Distributor_model->add();
    }}

    public function addProduct()
    {

            $this->load->model('Category_model');
            $category = $this->Category_model->fetch();
            $data['category'] = $category;

            $this->load->model('Distributor_model');
            $distributor = $this->Distributor_model->fetch();
            $data['distributor'] = $distributor;

            $this->load->model('Product_model');
            $product_details = $this->Product_model->fetch();
            $data['product_details'] = $product_details;
            //var_dump($product_details);
            /*        header('Content-Type: application/json');
                    echo json_encode($product_details);
                    return;*/
            $this->load->view('admin/header');
            $this->load->view('admin/add-product', $data);
            $this->load->view('admin/ajax-footer');
            //$this->load->view('admin/ajax');


    }
    public function get_sub_category()
    {
        //$id = $this->input->post('category');
        $this->load->model('Sub_category_model');
        $id = $this->uri->segment(2);
        $sub_category = $this->Sub_category_model->fetch_sub($id);
        $data['sub_category'] = $sub_category;
        //var_dump($sub_category);
        header('Content-Type: application/json');
        echo json_encode($sub_category);
        return;
        //echo json_encode($sub_category);


    }

    public function submitProduct()
    {
        $this->load->helper('new_helper');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('productcode', 'productcode', 'required');
        $this->form_validation->set_rules('productname', 'product  name', 'required');
        $this->form_validation->set_rules('productdescription', 'product description', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('error', '* marked feilds are requireds');
            redirect_back();

        } else {


            $this->load->model('Product_model');
            $product_id = $this->Product_model->add();

            $this->load->model('Product_image_model');

            $config['upload_path'] = 'public/upload/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 10000;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;
            $count = count($_FILES['product_img']['size']);
            foreach ($_FILES as $key => $value) {
                for ($s = 0; $s <= $count - 1; $s++) {

                    $_FILES['file']['name'] = time() . $value['name'][$s];
                    $_FILES['file']['type'] = $value['type'][$s];
                    $_FILES['file']['tmp_name'] = $value['tmp_name'][$s];
                    $_FILES['file']['error'] = $value['error'][$s];
                    $_FILES['file']['size'] = $value['size'][$s];
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        $error = array('error' => $this->upload->display_errors());
                        //var_dump($error);
                        echo 'error';
                    } else {
                        $data = $this->upload->data();
                        //var_dump($data);
                        //echo 'success';
                        $result_set = $this->Product_image_model->setSlider('public/upload/' . $data['file_name'], $product_id);
                    }
                }
            }
            redirect('add-product');
        }
    }

    private function set_upload_options()
    {
        //upload an image options
        $config = array();
        $config['upload_path'] = './public/upload/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '0';
        $config['overwrite'] = FALSE;

        return $config;

    }




    public function addProductImages()
    {
        $this->load->model('Product_image_model');
        $this->load->model('Product_model');
        $products= $this->Product_model->fetch_productId();
        $data['products']=$products;
        //var_dump($products);
        $images = $this->Product_image_model->getSlider();
        $data['images'] = $images;
        $this->load->view('admin/header');
        $this->load->view('admin/add-product-images',$data);
        $this->load->view('admin/footer');

    }
    //saving produvt images..
    public function submitImage()
    {

        $this->load->model('Product_image_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Image title', 'required');
        if ($this->form_validation->run() === false) {
            $data = $this->SliderModel->getSlider();
            $data['data'] = $data;

            $this->load->view('admin/header');
            $this->load->view('admin/add-product-image',$data);
            $this->load->view('admin/footer');
        }
        else {
            if (file_exists("public/upload/" . $_FILES["logo"]["name"]))
            {
                echo $_FILES["logo"]["name"] . " already exists. ";
            }
            // uploading logo
            // if logo image is selected
            $logo_url = null;
            if ( ! empty($_FILES['logo']['name'])) {
                $file_name = str_replace(' ', '-', $this->input->post('name'));
                $file_name = time().$file_name;
                $config['upload_path']          = 'public/upload/';
                $config['allowed_types']        = 'jpg|png|jpeg';
                $config['max_size']             = 10024;
                $config['max_width']            = 10024;
                $config['max_height']           = 7068;
                $config['file_name']            = $file_name.'.'.pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);

                $logo_url = 'public/upload/'.$file_name.'.'.pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('logo'))
                {
                    $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                    $data = array('upload_data' => $this->upload->data());
                }
            }


            // insert data into database
            $this->Product_image_model->setSlider($logo_url);
            $this->session->set_flashdata('success', 'New Image has been successfully added.');
            redirect('add-product-images');
        }
    }
    public function deleteImage()
    {
        $this->load->helper('new_helper');
        $image_id = $this->uri->segment(2);
        $product_id = $this->uri->segment(3);
        /*$url='view-details/'.$product_id;
        echo $url;*/
        //echo $id;
        $this->db->delete('product_gallery', array('gallery_id' => $image_id));
        $this->session->set_flashdata('success', 'Image Successfully deleted.');
        //redirect($url);

        redirect_back();

    }
    public function productSampleImage()
    {
        $this->load->model('Product_model');
        $this->load->model('Product_image_model');
        $products= $this->Product_model->fetch_productId();
        $data['products']=$products;

        $this->load->view('admin/header');
        $this->load->view('admin/add-product-sample-image',$data);
        $this->load->view('admin/footer');
    }

    //saving sample image
    public function submitSampleImage()
    {
        $this->load->model('product_sample_image_model');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Image title', 'required');
        if ($this->form_validation->run() === false) {
            redirect('add-product-sample-images');
        }
        else {
            if (file_exists("public/sampleImages/" . $_FILES["logo"]["name"]))
            {
                echo $_FILES["logo"]["name"] . " already exists. ";
            }
            // uploading logo
            // if logo image is selected
            $logo_url = null;
            if ( ! empty($_FILES['logo']['name'])) {
                $file_name = str_replace(' ', '-', $this->input->post('name'));
                $file_name = time().$file_name;
                $config['upload_path']          = 'public/sampleImages/';
                $config['allowed_types']        = 'jpg|png|jpeg';
                $config['max_size']             = 10024;
                $config['max_width']            = 10024;
                $config['max_height']           = 7068;
                $config['file_name']            = $file_name.'.'.pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);

                $logo_url = 'public/sampleImages/'.$file_name.'.'.pathinfo($_FILES['logo']['name'], PATHINFO_EXTENSION);

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('logo'))
                {
                    $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                    $data = array('upload_data' => $this->upload->data());
                }
            }
            // insert data into database
            $this->product_sample_image_model->setSampleImage($logo_url);
            $this->session->set_flashdata('success', 'New Sample Image has been successfully added.');
            redirect('add-product-sample-images');
        }
    }


    public function viewDetails($parameter1)
    {
        $pro_id = $parameter1;
        $this->load->model('Product_model');
        $products = $this->Product_model->fetchProductDetails($pro_id);
        $data['products'] = $products;
//        var_dump($products);

        /*header('content-type:application/json');
        echo json_encode($products);*/


            $this->load->view('admin/header');
            $this->load->view('admin/product-details',$data);
            $this->load->view('admin/footer');

    }
        //edit update and delete functions
    public function editCategory()
    {
        $category_id=$this->uri->segment(2);
        //echo $category_id;
        $this->load->model('Category_model');
        $category_details=$this->Category_model->fetch_by_categoryid($category_id);
        $data['category_details']=$category_details;
        $this->load->view('admin/header');
        $this->load->view('admin/edit-category',$data);
        $this->load->view('admin/footer');
    }
    public function updateCategory()
    {
        $this->load->model('Category_model');
        $this->Category_model->update();
        $this->session->set_flashdata('success', 'category Updated.');
        redirect('add-category');
    }
    public function deletecategory()
    {
        $category_id=$this->uri->segment(2);
        $this->load->model('Sub_category_model');
        //sub_ categories under this category
        $sub=$this->Sub_category_model->fetch_sub($category_id);
        if(count($sub)>0)
        {
            $this->session->set_flashdata('error', 'Unable to delete,this category consist sub categories.');
            redirect('add-category');
        }
        else
            {

                $products=$this->product_model->fetch_by_categoryid($category_id);
                if(count($products))
                {
                    $this->session->set_flashdata('error', 'Unable to delete,this category consist products.');
                    redirect('add-category');
                }
                else
                    {
                        $category_id=$this->uri->segment(2);
                        $this->load->model('category_model');
                        $this->Category_model->delete($category_id);
                        $this->session->set_flashdata('success', 'Category Succesfully deleted.');
                        redirect('add-category');

                    }
            }
    }
    public function editSubcategory()
    {
        $sub_category_id=$this->uri->segment(2);
        //echo $category_id;
        $this->load->model('Sub_category_model');
        $sub_category_details=$this->Sub_category_model->fetch_by_sub_category_id($sub_category_id);
        $data['sub_category_details']=$sub_category_details;
        //var_dump($sub_category_details);
        $this->load->view('admin/header');
        $this->load->view('admin/edit-sub-category',$data);
        $this->load->view('admin/footer');
    }
    public function updateSubcategory()
    {
        $this->load->model('Sub_category_model');
        $this->Sub_category_model->update();
        $this->session->set_flashdata('success', 'sub category Updated.');
        redirect('add-sub-category');
    }
    public function deleteSubcategory()
    {
        $sub_category_id=$this->uri->segment(2);
        $this->load->model('Sub_category_model');
        //sub_ categories under this category
            $this->load->model('product_model');
            $products=$this->product_model->fetch_by_subcategoryid($sub_category_id);
            if(count($products)>0)
            {
                $this->session->set_flashdata('error', 'Unable to delete,this sub category consist products.');
                redirect('add-sub-category');
            }
            else
            {

                $this->Sub_category_model->delete($sub_category_id);
                $this->session->set_flashdata('success', ' Sub Category Succesfully deleted.');
                redirect('add-sub-category');

            }
        }
    public function editDistributor()
    {
        $distributor_id=$this->uri->segment(2);
        //echo $category_id;
        $this->load->model('Distributor_model');
        $distributor_details=$this->Distributor_model->fetch_by_distributorid($distributor_id);
        $data['distributor_details']=$distributor_details;
        //var_dump($sub_category_details);
        $this->load->view('admin/header');
        $this->load->view('admin/edit-distributor',$data);
        $this->load->view('admin/footer');
    }
    public function updateDistributor()
    {
        $this->load->model('Distributor_model');
        $this->Distributor_model->update();
        $this->session->set_flashdata('success', 'Distributor Updated.');
        redirect('add-distributor');
    }
    public function deleteDistributor()
    {
        $Distributor_id=$this->uri->segment(2);
        $this->load->model('Distributor_model');
        $this->Distributor_model->delete($Distributor_id);
        $this->session->set_flashdata('success', ' Distributor Successfully deleted.');
        redirect('add-distributor');

    }
    public function editProduct()
    {
        $this->load->model('Distributor_model');
        $distributor=$this->Distributor_model->fetch();
        $data['distributor'] = $distributor;
        $product_id=$this->uri->segment(2);
        //echo $category_id;
        $this->load->model('Product_model');
        $products = $this->Product_model->fetchProductDetails($product_id);
        $data['products'] = $products;
        /*echo $products[0]['distributor_name'];
        var_dump($products);
        $distributor_id="";
        foreach ($products as $pro)
        {
            echo $pro->distributor_id;
        }
        echo $distributor_id;
        $product_distributor=$this->Distributor_model->fetch_by_distributorid($distributor_id);
        $data['product_distributor']=$product_distributor;
        var_dump($product_distributor);
        die();*/



        $product_details=$this->Product_model->fetch_product_by_productid($product_id);
        $data['product_details']=$product_details;
        //var_dump($product_details);
        $this->load->view('admin/header');
        $this->load->view('admin/edit-product',$data);
        $this->load->view('admin/ajax-footer');
    }


    public function updateProduct()
    {
        $product_id=$this->input->post('productid');
       // echo $product_id;
        $this->load->model('Product_model');
        $product_details=$this->Product_model->update($product_id);
        $data['product_details']=$product_details;
        //var_dump($product_details);
        //die();

        $this->load->model('Product_image_model');

        $config['upload_path']          = 'public/upload/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 10000;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;
        $count = count($_FILES['product_img']['size']);
            //var_dump($_FILES['product_img']['name']);
            //return;
        foreach ($_FILES as $key => $value) {
            for ($s = 0; $s <= $count - 1; $s++) {

                $_FILES['file']['name']=        time().$value['name'][$s];
                $_FILES['file']['type']    =    $value['type'][$s];
                $_FILES['file']['tmp_name'] =   $value['tmp_name'][$s];
                $_FILES['file']['error']    = $value['error'][$s];
                $_FILES['file']['size']    =    $value['size'][$s];
                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload('file'))
                {
                    $error = array('error' => $this->upload->display_errors());
                    //var_dump($error);
                    echo 'error';
                }
                else
                {
                    $data =  $this->upload->data();
                    //var_dump($data);
                    //echo 'success';
                    $result_set = $this->Product_image_model->setSlider('public/upload/'.$data['file_name'], $product_id);
                }
            }
        }
        redirect ('add-product');

    }
    public function deleteProduct()
    {
        $product_id=$this->uri->segment(2);
        $this->load->model('Product_model');
        $this->Product_model->delete($product_id);
        $this->session->set_flashdata('success', ' Product deleted Successfully.');
        redirect('add-product');
    }
    public function editGallery()
    {

        $this->load->model('Product_model');
        $product=$this->Product_model->fetch_productId();
        $data['product']=$product;
        $this->load->view('admin/header');
        $this->load->view('admin/edit-gallery',$data);
        $this->load->view('admin/footer');
    }
//edit update and delete functions end
    public function activateCategory()
    {
        $id=$this->uri->segment(2);
        $this->load->model('Category_model');
        $this->Category_model->activateCategory($id);
        $this->load->model('Product_model');
        $this->Product_model->activateProduct($id);
        $this->session->set_flashdata('success','category Activated');
        redirect('add-category');

    }
    public function deactivateCategory()
    {
        $id=$this->uri->segment(2);
        $this->load->model('Category_model');
        $this->Category_model->deactivateCategory($id);
        $this->load->model('Product_model');
        $this->Product_model->deactivateProduct($id);
        $this->load->model('Sub_category_model');
        $this->Sub_category_model->deactivateSubcategoryByCategoryId($id);
        $this->session->set_flashdata('success','category De-activated');
        redirect('add-category');

    }
    public function activateProduct()
    {
        $this->load->helper('new_helper');
        $product_id = $this->uri->segment(2);
        $this->load->model('Product_model');
        $product_detail = $this->Product_model->fetch_product_by_productid($product_id);
        $category_id = "";
        $sub_category_id = "";
        foreach ($product_detail as $data) {
            $category_id = $data->category_id;
            $sub_category_id = $data->sub_category_id;
        }

        //fetch sub category status
        $this->load->model('Sub_category_model');
        $sub_category_status = $this->Sub_category_model->fetch_by_sub_category_id($sub_category_id);
        foreach ($sub_category_status as $data) {
            if ($data->sub_category_status == 0) {
                $this->session->set_flashdata('error', 'you can not activate product without activating its sub category');
                redirect_back();
                return;
            } else {
                $this->load->model('Category_model');
                $category_status = $this->Category_model->fetch_by_categoryid($category_id);
                foreach ($category_status as $data) {
                    if ($data->status == 0) {
                        $this->session->set_flashdata('error', 'you can not activate product without activating its category');
                        redirect_back();
                    } else {
                        $this->Product_model->activateProductByProductId($product_id);
                        $this->session->set_flashdata('success', 'Product Activated');
                        redirect('add-product');
                    }
                }

            }
        }
    }

    public function deactivateProduct()
    {
        $id=$this->uri->segment(2);
        $this->load->model('Product_model');
        $this->Product_model->deactivateProductByProductId($id);
        $this->session->set_flashdata('success','Product De-activated');
        redirect('add-product');

    }
    public function activateSubcategory()
    {
        $this->load->helper('new_helper');
        $sub_category_id=$this->uri->segment(2);
        $this->load->model('Sub_category_model');
        $this->load->model('Category_model');
        $this->load->model('Product_model');
        $category_status=$this->Category_model->fetch_by_subcategoryid($sub_category_id);
        if($category_status[0]->status==0)
        {
            $this->session->set_flashdata('error','category of this sub category is not activated. first activate the category');
            redirect_back();
        }
        else{
            $this->Sub_category_model->activate($sub_category_id);
            $this->Product_model->activateProductBySubcategoryId($sub_category_id);
            $this->session->set_flashdata('success','Sub Category Activated.');
            redirect_back();
        }
        /*
        $this->Sub_category_model->activate($sub_category_id);
        $this->session->set_flashdata('success','Sub Category De-activated');*/

    }
    public function deactivateSubcategory()
    {
        $this->load->helper('new_helper');
        $sub_category_id=$this->uri->segment(2);
        $this->load->model('Sub_category_model');
        $this->load->model('Product_model');
        $this->Sub_category_model->deactivate($sub_category_id);
        $this->Product_model->deactivateProductBySubcategoryId($sub_category_id);
        $this->session->set_flashdata('success','Sub Category De-activated.');
        redirect_back();

    }

   public function viewOrder()
   {
       $order_detail_id=$this->uri->segment(2);
       $this->load->model('order_details_model');
       $order_detail=$this->order_details_model->fetchByOrderDetailId($order_detail_id);
       $data['order_detail']=$order_detail;
       $this->load->view('admin/header');
        $this->load->view('admin/view-order',$data);
        $this->load->view('admin/footer');
   }
   public function deleteOrder()
   {
       $id=$this->uri->segment(2);
       $this->load->helper('new_helper');
       $this->db->delete('order_details', array('order_detail_id' => $id));
       redirect_back();
   }
    public function viewAllProduct()
    {
        $this->load->model('Category_model');
        $category = $this->Category_model->fetch();
        $data['category'] = $category;
            $this->load->view('admin/header');
            $this->load->view('admin/view-all-product',$data);
            $this->load->view('admin/ajax-footer');
    }

    public function viewProduct()
    {
        $this->load->model('Product_model');
        $sub_category_id=$this->input->post('subcategory');
        if($sub_category_id=="")
        {
            $sub_category_id=0;
        }
        //echo $sub_category_id;
        $products=$this->Product_model->fetch_by_subcategoryid($sub_category_id);
        $this->load->model('Category_model');
        $category = $this->Category_model->fetch();
        $data['category'] = $category;
        $data['products'] = $products;

        $this->load->view('admin/header');
        $this->load->view('admin/view-all-product',$data);
        $this->load->view('admin/ajax-footer');


    }

}

?>