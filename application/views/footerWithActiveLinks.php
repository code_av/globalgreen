
<footer class="main-footer">
    <!--Footer Upper-->
    <div class="footer-upper">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Two 4th column-->
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <div class=" column">
                            <div class="footer-widget about-widget">
                                <h3>About Us</h3>
                                <div class="text margin-bott-30">
                                    <p>Global Green Eco Technologies is a high-tech joint venture which is specialized in researching ,manufacturing and marketing the enviromental friendly WPC materials.</p>
                                </div>

                                <div class="social-links">
                                    <a href="#"><span class="fa fa-facebook-f"></span></a>
                                    <a href="#"><span class="fa fa-twitter"></span></a>
                                    <a href="#"><span class="fa fa-google-plus"></span></a>
                                    <a href="#"><span class="fa fa-skype"></span></a>
                                </div>

                            </div>
                        </div>




                    </div>
                </div><!--Two 4th column End-->

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="column">
                            <div class="footer-widget links-widget">
                                <h3>Global Green Eco Technologies</h3>
                                <div class="row clearfix">

                                    <div class="col-lg-6 col-sm-6 col-xs-12">
                                        <ul>
                                            <li><i class="fa fa-angle-right"></i><a href="index.php">Home</a></li>
                                            <li><i class="fa fa-angle-right"></i><a href="about.php">About Us</a></li>
                                            <li><i class="fa fa-angle-right"></i><a href="product_1.php">Products</a></li>
                                            <li><i class="fa fa-angle-right"></i><a href="services_1.php">Services</a></li>
                                            <li><i class="fa fa-angle-right"></i><a href="contact.php">Contact</a></li>
                                        </ul>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <!--Two 4th column-->
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="row clearfix">



                        <!--Footer Column-->
                        <div class="column">
                            <div class="footer-widget newsletter-widget">
                                <h3>Gallery</h3>

                                <div class="newsletter-form">

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door6.jpg" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                            <img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door7.jpg" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door8.jpg" class="img-responsive">
                                        </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door9.jpg" class="img-responsive">
                                    </div>

                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door10.jpg" class="img-responsive">
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door11.jpg" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--Two 4th column End-->



            </div>

        </div>
    </div>

    <!--Footer Bottom-->
    <div class="footer-bottom">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-6 col-xs-12"><div class="copyright-text">© 2017 All Rights Reserved</div></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <!--Bottom Nav-->
                    <nav class="footer-nav clearfix">
                        <ul class="pull-right clearfix">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about-us-one.php">About</a></li>
                            <li><a href="services_1.php">Services</a></li>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>

</footer>
</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>


<script src="<?php echo base_url() ?>public/assets/js/jquery.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/jQuery.style.switcher.min.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/jquery.appear.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/revolution.min.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/jquery.fancybox-media.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/owl.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/wow.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/appear.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/smoothscroll.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/isotope.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/validation.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/script.js"></script>


<script src="<?php echo base_url()?>public/assets/js/map-script.js"></script>

</body>
</html>