﻿    <section class="page-title" style="background-image:url(<?php echo base_url() ?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Ozone</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li class="active">Ozone</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="about-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="content-column col-md-6 col-sm-6 ">
                    <div class="inner-box">
                        <div class="sec-title">
                            <h2>Welcome to <span>Ozone</span></h2>
                        </div>
                    </div>
                    <div class="text" style="text-align: justify">
                        <p>
                            Ozone Overseas, an integral part of Ozone Enterprise group, is the leading player in
                            Architectural Hardware industry.With striking global presence & recognition, Ozone constantly
                            strives to provide innovative hardware solutions for architects, builders, specifiers and
                            home owners.
                        </p>
                        <p>
                            Ozone offers internationally certified hardware solutions through its product portfolio of more
                            than 3000 products divided into 10 product categories.<br>
                            Ozone has a strong distribution network across India including dealers, distributors, C&F agents &
                            company own display cum training Centre- The Ozone Hub. These display centre showcase
                            all product ranges under one roof & offers technical know-how of product application and usage.
                        </p>
                        <p>
                            mail us your requirement at <a href="mailto: orders@globalgreeneco.com">orders@globalgreeneco.com</a> , you can
                            also <a href="<?php echo base_url() ?>distributors-product/ozone/12">explore our products line</a>
                        </p>
                        <p>
                            <a class="btn btn-success" href="#e-catalogue">download e-catalogue</a>
                        </p>
                    </div>
                </div>
                <div class="content-column col-md-6 col-sm-6 ">
                    <div class="row clearfix">
                        <div class="column">
                            <div class="footer-widget newsletter-widget">
                                <div class="newsletter-form">
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/oz1.png"> <img src="<?php echo base_url()?>public/assets/gallery1/images/home/oz1.png" class="img-responsive" style="width: 200px;height: 180px;"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/oz2.png"><img src="<?php echo base_url()?>public/assets/gallery1/images/home/oz2.png" class="img-responsive" style="width: 200px;height: 180px;"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/oz3.png"><img src="<?php echo base_url()?>public/assets/gallery1/images/home/oz3.png" class="img-responsive" style="width: 200px;height: 180px;"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/oz4.png"><img src="<?php echo base_url()?>public/assets/gallery1/images/home/oz4.png" class="img-responsive" style="width: 200px;height: 180px;"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/oz5.png"><img src="<?php echo base_url()?>public/assets/gallery1/images/home/oz5.png" class="img-responsive" style="width: 200px;height: 180px;"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/oz6.png"><img src="<?php echo base_url()?>public/assets/gallery1/images/home/oz6.png" class="img-responsive" style="width: 200px;height: 180px;" ></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <a name="e-catalogue"></a>
    <section class="why-chooseus sec-padd2">
        <div class="container">
            <div class="sec-title centered">
                <h2>E- <span>Catalogue</span></h2>
            </div>
            <div class="row clearfix">
                <div class="column">
                    <div class="footer-widget newsletter-widget">
                        <div class="newsletter-form">
                            <div class="col-md-12" id="catalogue">
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-3  filter hdpe">
                                    <div class="inner hover-style1  hvr-float-shadow">
                                        <figure class="image-box hover-style1-img">
                                            <a href="<?php echo base_url()?>public/assets/docs/ozone-catalogue.pdf" target="_blank">
                                                <img src="<?php echo base_url()?>public/assets/docs/ozone.png" class="img-responsive" style="height: 250px; width: 300px; "/>
                                            </a>
                                            <div class="hover-style1-view">
                                                <a class="link-box" href="<?php echo base_url()?>public/assets/docs/ozone-catalogue.pdf" target="_blank"><span class="fa fa-link"></span></a>
                                            </div>
                                        </figure>
                                        <div class="lower-box" style="text-align: center">
                                            <h3 style="color:black;">Ozone Fittings</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
