﻿
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Service Details</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="<?php echo base_url()?>services">Services</a></li>
                    <li class="active">WPC Door</li>
                </ul>
            </div>
        </div>
    </section>
    
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container with-left-sidebar">
        <div class="auto-container">
            <div class="row clearfix">
				
                <!--Content Side-->
                <div class="content-side pull-right col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <!--Service Details-->
                    <section class="service-details">
                        
                        <figure class="big-image"><img src="<?php echo base_url()?>public/assets/gallery1/images/service/door1.jpg" alt=""></figure>
                        
                        <!--Text Content-->
                        <div class="content">
                        	<h3>PROFESSIONAL SERVICE </h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur libero enim, lacinia finibus ante et, imperdiet finibus risus. Donec malesuada luctus elit nec hendrerit. Integer ex sap ien, eleifend sit amet tellus ut, porttitor dictum velit. Nulla scelerisque, nisl eu maximus bibendum nibh risus eleifend ex, id dapibus leo dui in nulla. Donec sollicitudin molestie erat sit amet elementum. Donec at lacinia massa, eu hen rit ex. Donec hendrerit, porttitor.</p>
                            <blockquote><div class="txt">People think focus means saying yes to the thing you’ve got to focus on. But that’s not what it means at all. It means saying no to the hundred other good ideas that there are. You have to pick carefully. I’m actually as proud of the things.</div></blockquote>
                            <p>ex orci sollicitudin dolor, et scelerisque sem ex in nisi.Nulla pulvinar vitae arcu at cursus. Class aptent taciti sociosqu ad litora torquent per co Curabitur accumsan mauris vitae leo tempus, ac sceleri sque massa elementum. In blandit fringilla tincidunt. Proin commodo tortor vitae is nisi. Etiam accumsan massa volutpat tellus</p>
                            <h3>OUR EXPERIENCED</h3>
                            <div class="row clearfix">
                            	<div class="col-md-6 col-sm-6 col-xs-12">
                                	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elitazaa. fsdssd Curabitur libero eni lacinia finibus ante et, imaperdiet finibus risus. Donec malesuadaluctus elit naaacec hendrerit. Integer ex sap ien, ds eleifend sit amet tellus ut, porttitor dict um velit. Nulla dssd ads scelerisque, nisl euasaszz maximus bibendum nibh risus eleifen ex, a nulla.</p>
                                    <p> Donec sollicitudin molestie erat sit amet elementum. Donec at lacinia massa.ex orci sollicitudinaasa dolor, e scelerisque sem ex in nisi.Nulla pulvinar vitae ar cu at cursus.</p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                	<!--Graph Chart-->
                                    <div class="chart-outer">
                                        <div id="Chart1" style="height: 270px; width: 100%;"></div>
                                    </div>
                                </div>
                            </div>
                            
                            <!--Faqs-->
                            <div class="faqs">
                                <!--Accordion Box-->
                                <ul class="accordion-box">
                                
                                    <!--Block-->
                                    <li class="accordion block active-block">
                                        <div class="acc-btn active"><div class="icon-outer"><span class="icon fa fa-angle-right"></span></div> Future Generations</div>
                                        <div class="acc-content current">
                                            <div class="content"><p>Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.</p></div>
                                        </div>
                                    </li>
            
                                    <!--Block-->
                                    <li class="accordion block">
                                        <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div> Excellence History</div>
                                        <div class="acc-content">
                                            <div class="content"><p>Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.</p></div>
                                        </div>
                                    </li>
                                    
                                    <!--Block-->
                                    <li class="accordion block">
                                        <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div> Professional Skills  </div>
                                        <div class="acc-content">
                                            <div class="content"><p>Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.</p></div>
                                        </div>
                                    </li>
                                    
                                    <!--Block-->
                                    <li class="accordion block">
                                        <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div>Safety Precautions</div>
                                        <div class="acc-content">
                                            <div class="content"><p>Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.</p></div>
                                        </div>
                                    </li>
            
                                </ul><!--End Accordion Box-->
                            </div>
                        
                        </div>
                    </section>


                </div>
                <!--Content Side-->
                
                <!--Sidebar-->
                <div class="sidebar-side pull-left col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">


                        <!-- Services -->
                        <div class="sidebar-widget services">
                            <ul class="service-list">
                                <li><a href="<?php echo base_url()?>service">View all services</a></li>
                                <li class="active"><a href="<?php echo base_url()?>services-single">Mechanical Enginnering</a></li>
                                <li><a href="<?php echo base_url()?>services-single">Chemical Research</a></li>
                                <li><a href="<?php echo base_url()?>services-single">Material Engineering</a></li>
                                <li><a href="<?php echo base_url()?>services-single">Petroleum and Gas</a></li>
                                <li><a href="<?php echo base_url()?>services-single">Agriculture Process</a></li>
                                <li><a href="<?php echo base_url()?>services-single">Power and Energy</a></li>
                            </ul>
                        </div>
						
                        <!-- Downloads -->
                        <div class="sidebar-widget downloads">
                            <ul class="download-list">
                                <li><a href="#"><span class="fa fa-file-pdf-o"></span> Download via . Pdf</a></li>
                                <li><a href="#"><span class="fa fa-file-word-o"></span> Download via . Doc</a></li>
                            </ul>
                        </div>

                        <!-- Recent Posts -->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title"><h3>Recent news</h3></div>

                            <article class="post">
                            	<figure class="post-thumb"><a href="<?php echo base_url()?>blog-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/post-thumb-1.jpg" alt=""></a></figure>
                                <div class="text"><a href="#">What makes us best<br> in the world?</a></div>
                                <ul class="post-meta clearfix">
                                	<li><a href="#">Jan 30, 2017 </a></li>  <li><a href="#"><span class="fa fa-commenting-o"></span> 2 Comments</a></li>
                                </ul>
                            </article>

                            <article class="post">
                            	<figure class="post-thumb"><a href="<?php echo base_url()?>blog-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/post-thumb-2.jpg" alt=""></a></figure>
                                <div class="text"><a href="#">We help you to Grow your industries</a></div>
                                <ul class="post-meta clearfix">
                                	<li><a href="#">Feb 18, 2017 </a></li>  <li><a href="#"><span class="fa fa-commenting-o"></span> 8 Comments</a></li>
                                </ul>
                            </article>

                        </div>

                        <!-- Popular Tags -->
                        <div class="sidebar-widget popular-tags">
                            <div class="sidebar-title"><h3>Tags</h3></div>
                            <ul>
                                <li><a href="#">Active</a></li>
                                <li><a href="#">Building </a></li>
                                <li><a href="#">Ideas</a></li>
                                <li><a href="#">Energy</a></li>
                                <li><a href="#">Engines</a></li>
                                <li><a href="#">Chemical</a></li>
                                <li><a href="#">Industry</a></li>
                                <li><a href="#">Drilling</a></li>
                            </ul>
                                
                        </div>

                    </aside>


                </div>
                <!--Sidebar-->

            </div>
        </div>
    </div>
