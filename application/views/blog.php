﻿
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url() ?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Blog</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url() ?>">Home</a></li>
                    <li class="active">Blog</li>
                </ul>
            </div>
        </div>
    </section>
    
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container with-right-sidebar gray-bg">
        <div class="auto-container">
            <div class="row clearfix">
				
                <!--Content Side-->
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                            
                    <!--News Section-->
                    <section class="news-section no-padd">
                        
                        <div class="row clearfix">
                            <!--News Style One-->
                            <div class="news-style-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
                                    <div class="image-box"><figure class="image"><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/news-img-2.jpg" alt=""></figure><div class="overlay-box"><a class="link-box" href="<?php echo base_url()?>blog-single"><span class="fa fa-link"></span></a></div></div>
                                    <div class="lower-box">
                                        <div class="post-meta">
                                            <ul class="clearfix">
                                                <li><a href="#"><span class="fa fa-user"></span> by Smith</a></li>
                                                <li><a href="#"><span class="fa fa-calendar"></span> Jan 05, 2017</a></li>
                                                <li><a href="#"><span class="fa fa-commenting-o"></span> 23 Comments</a></li>
                                            </ul>
                                        </div>
                                        <h3><a href="<?php echo base_url() ?>blog-single">We Are help you to Grow your industries Of company.</a></h3>
                                        <div class="text">customer support.Startup equity interaction design scrum project prototype management branding business. We are love to hear from you Angel investor growth</div>

                                    </div>
                                </div>
                            </div>
                            
                            <!--News Style One-->
                            <div class="news-style-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box wow fadeInUp animated" data-wow-delay="300ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 300ms; animation-name: fadeInUp;">
                                    <div class="image-box"><figure class="image"><img src="<?php echo base_url() ?>public/assets/gallery1/images/resource/news-img-3.jpg" alt=""></figure><div class="overlay-box"><a class="link-box" href="<?php echo base_url()?>blog-single"><span class="fa fa-link"></span></a></div></div>
                                    <div class="lower-box">
                                        <div class="post-meta">
                                            <ul class="clearfix">
                                                <li><a href="#"><span class="fa fa-user"></span> by Smith</a></li>
                                                <li><a href="#"><span class="fa fa-calendar"></span> Feb 05, 2017</a></li>
                                                <li><a href="#"><span class="fa fa-commenting-o"></span> 23 Comments</a></li>
                                            </ul>
                                        </div>
                                        <h3><a href="<?php echo base_url() ?>blog-single">Make your teams more productive, deliver lightning-fast, and more</a></h3>
                                        <div class="text">customer support.Startup equity interaction design scrum project prototype management branding business. We are love to hear from you Angel investor growth</div>
                                    </div>
                                </div>
                            </div>
                            
                            <!--News Style One-->
                            <div class="news-style-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box wow fadeInUp animated" data-wow-delay="600ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 600ms; animation-name: fadeInUp;">
                                    <div class="image-box"><figure class="image"><img src="<?php echo base_url() ?>public/assets/gallery1/images/resource/news-img-5.jpg" alt=""></figure><div class="overlay-box"><a class="link-box" href="<?php echo base_url() ?>blog-single"><span class="fa fa-link"></span></a></div></div>
                                    <div class="lower-box">
                                        <div class="post-meta">
                                            <ul class="clearfix">
                                                <li><a href="#"><span class="fa fa-user"></span> by Smith</a></li>
                                                <li><a href="#"><span class="fa fa-calendar"></span> Mar 05, 2017</a></li>
                                                <li><a href="#"><span class="fa fa-commenting-o"></span> 23 Comments</a></li>
                                            </ul>
                                        </div>
                                        <h3><a href="<?php echo base_url() ?>blog-single">Our Expertise are earned through our experiences</a></h3>
                                        <div class="text">customer support.Startup equity interaction design scrum project prototype management branding business. We are love to hear from you Angel investor growth</div>
                                    </div>
                                </div>
                            </div>
                            <!--News Style One-->
                            <div class="news-style-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInUp;">
                                    <div class="image-box"><figure class="image"><img src="<?php echo base_url() ?>public/assets/gallery1/images/resource/news-img-2.jpg" alt=""></figure><div class="overlay-box"><a class="link-box" href="<?php echo base_url()?>blog-single"><span class="fa fa-link"></span></a></div></div>
                                    <div class="lower-box">
                                        <div class="post-meta">
                                            <ul class="clearfix">
                                                <li><a href="#"><span class="fa fa-user"></span> by Smith</a></li>
                                                <li><a href="#"><span class="fa fa-calendar"></span> Jan 05, 2017</a></li>
                                                <li><a href="#"><span class="fa fa-commenting-o"></span> 23 Comments</a></li>
                                            </ul>
                                        </div>
                                        <h3><a href="<?php echo base_url() ?>blog-single">We Are help you to Grow your industries Of company.</a></h3>
                                        <div class="text">customer support.Startup equity interaction design scrum project prototype management branding business. We are love to hear from you Angel investor growth</div>

                                    </div>
                                </div>
                            </div>
                            
                            <!--News Style One-->
                            <div class="news-style-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box wow fadeInUp animated" data-wow-delay="300ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 300ms; animation-name: fadeInUp;">
                                    <div class="image-box"><figure class="image"><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/news-img-3.jpg" alt=""></figure><div class="overlay-box"><a class="link-box" href="<?php echo base_url()?>blog-single"><span class="fa fa-link"></span></a></div></div>
                                    <div class="lower-box">
                                        <div class="post-meta">
                                            <ul class="clearfix">
                                                <li><a href="#"><span class="fa fa-user"></span> by Smith</a></li>
                                                <li><a href="#"><span class="fa fa-calendar"></span> Feb 05, 2017</a></li>
                                                <li><a href="#"><span class="fa fa-commenting-o"></span> 23 Comments</a></li>
                                            </ul>
                                        </div>
                                        <h3><a href="<?php echo base_url() ?>blog-single">Make your teams more productive, deliver lightning-fast, and more</a></h3>
                                        <div class="text">customer support.Startup equity interaction design scrum project prototype management branding business. We are love to hear from you Angel investor growth</div>
                                    </div>
                                </div>
                            </div>
                            
                            <!--News Style One-->
                            <div class="news-style-two col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box wow fadeInUp animated" data-wow-delay="600ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 600ms; animation-name: fadeInUp;">
                                    <div class="image-box"><figure class="image"><img src="<?php echo base_url() ?>public/assets/gallery1/images/resource/news-img-5.jpg" alt=""></figure><div class="overlay-box"><a class="link-box" href="<?php echo base_url() ?>blog-single"><span class="fa fa-link"></span></a></div></div>
                                    <div class="lower-box">
                                        <div class="post-meta">
                                            <ul class="clearfix">
                                                <li><a href="#"><span class="fa fa-user"></span> by Smith</a></li>
                                                <li><a href="#"><span class="fa fa-calendar"></span> Mar 05, 2017</a></li>
                                                <li><a href="#"><span class="fa fa-commenting-o"></span> 23 Comments</a></li>
                                            </ul>
                                        </div>
                                        <h3><a href="<?php echo base_url() ?>blog-single">Our Expertise are earned through our experiences</a></h3>
                                        <div class="text">customer support.Startup equity interaction design scrum project prototype management branding business. We are love to hear from you Angel investor growth</div>
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>
                        
                    </section>


                </div>
                <!--Content Side-->
                
                <!--Sidebar-->
                <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">

                        <!-- Search Form -->
                        <div class="sidebar-widget search-box">
                            <div class="sidebar-title"><h3>SEARCH</h3></div>
                            <form method="post" action="<?php echo base_url() ?>blog">
                                <div class="form-group">
                                    <input type="search" name="search-field" value="" placeholder="Search here.. ">
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>

                        </div>


                        <!-- Categories -->
                        <div class="sidebar-widget categories">
                            <div class="sidebar-title"><h3>Category</h3></div>

                            <ul class="list">
                                <li><a href="#">Architecture Plans</a></li>
                                <li><a href="#">Construction Projects</a></li>
                                <li><a href="#">Paintings</a></li>
                                <li><a href="#">Electrical Works</a></li>
                                <li><a href="#">Plumbing Works</a></li>
                            </ul>

                        </div>


                        <!-- Recent Posts -->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title"><h3>Recent News</h3></div>

                            <article class="post">
                                <figure class="post-thumb"><a href="<?php echo base_url() ?>blog-single"><img src="<?php echo base_url() ?>public/assets/gallery1/images/resource/post-thumb-1.jpg" alt=""></a></figure>
                                <div class="text"><a href="#">Proffesional solutions for your business.</a></div>
                                <ul class="post-meta clearfix">
                                    <li><a href="#"><i class="fa fa-calendar"></i>Dec 30, 2017 </a></li>  
                                </ul>
                            </article>

                            <article class="post">
                                <figure class="post-thumb"><a href="<?php echo base_url() ?>blog-single"><img src="<?php echo base_url() ?>public/assets/gallery1/images/resource/post-thumb-2.jpg" alt=""></a></figure>
                                <div class="text"><a href="#">Money Market Rates Finding in 2017</a></div>
                                <ul class="post-meta clearfix">
                                    <li><a href="#"><i class="fa fa-calendar"></i>Dec 18, 2017 </a></li>  
                                </ul>
                            </article>
                            
                            <article class="post">
                                <figure class="post-thumb"><a href="<?php echo base_url()?>blog-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/post-thumb-3.jpg" alt=""></a></figure>
                                <div class="text"><a href="#">Get a credit online with our team’s help</a></div>
                                <ul class="post-meta clearfix">
                                    <li><a href="#"><i class="fa fa-calendar"></i>Dec 12, 2017 </a></li>  
                                </ul>
                            </article>

                        </div>
                        
                        
                        <!-- Archives -->
                        <div class="sidebar-widget categories">
                            <div class="sidebar-title"><h3>Archive</h3></div>
                            <ul class="list">
                                <li><a href="#">July 2016 &ensp; (12)</a></li>
                                <li><a href="#">Aug 2016 &ensp; (38)</a></li>
                                <li><a href="#">Sep 2016 &ensp; (23)</a></li>
                                <li><a href="#">Oct 2016 &ensp; (17)</a></li>
                                <li><a href="#">Nov 2016  &ensp; (03)</a></li>
                            </ul>

                        </div>

                        <!-- Popular Tags -->
                        <div class="sidebar-widget popular-tags">
                            <div class="sidebar-title"><h3>Tags</h3></div>
                                <li><a href="#">Active</a></li>
                                <li><a href="#">Building </a></li>
                                <li><a href="#">Ideas</a></li>
                                <li><a href="#">Energy</a></li>
                                <li><a href="#">Engines</a></li>
                                <li><a href="#">Chemical</a></li>
                                <li><a href="#">Industry</a></li>
                                <li><a href="#">Drilling</a></li>
                        </div>

                    </aside>


                </div>
                <!--Sidebar-->

            </div>
        </div>
    </div>
