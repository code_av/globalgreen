
<!-- Header Lower -->
<div class="header-lower">
    <div class="main-box">
        <div class="auto-container">
            <div class="outer-container clearfix">

                <!--Nav Outer-->
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="current"><a href="<?php echo base_url()?>">Home</a></li>
                                <li class="dropdown"><a href="<?php echo base_url()?>about-us">About</a>
                                    <ul>
                                        <li><a href="<?php echo base_url()?>about-us-one">About</a></li>
                                        <li><a href="<?php echo base_url()?>team">Our Team</a></li>
                                        <li><a href="<?php echo base_url()?>faq">FAQ's</a></li>
                                        <li><a href="<?php echo base_url()?>404">404 Page</a></li>
                                        <li><a href="<?php echo base_url()?>testimonial">Testimonials</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#">Services</a>
                                    <ul>
                                        <li><a href="<?php echo base_url()?>services">View All Services</a></li>
                                        <li><a href="<?php echo base_url()?>wpcDoor">WPC Doors</a></li>
                                        <li><a href="<?php echo base_url()?>services-single">Agricultural Processing</a></li>
                                        <li><a href="<?php echo base_url()?>services-single">Mechanical Engineering</a></li>
                                        <li><a href="<?php echo base_url()?>services-single">Chemical Research</a></li>
                                        <li><a href="<?php echo base_url()?>services-single">Power And Energy</a></li>
                                        <li><a href="<?php echo base_url()?>services-single">Patroleum & Gas</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url()?>projects">Projects</a></li>
                                <li class="dropdown"><a href="#">Shop</a>
                                    <ul>
                                        <li><a href="<?php echo base_url()?>shop">Our Shop</a></li>
                                        <li><a href="<?php echo base_url()?>shop-single">Product Details</a></li>
                                        <li><a href="<?php echo base_url()?>shopping-cart">Shopping Cart</a></li>
                                        <li><a href="<?php echo base_url()?>checkout">Checkout</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#">News</a>
                                    <ul>
                                        <li><a href="<?php echo base_url()?>blog">News</a></li>
                                        <li><a href="<?php echo base_url()?>blog-single">News Details</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url()?>contact">Contact</a></li>

                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->

                    <!--Search Box-->
                    <div class="outer-box2">
                        <!--Search form-->
                        <div class="search-form">
                            <form method="post" action="<?php echo base_url()?>contact">
                                <div class="form-group">
                                    <input type="text" name="search" value="" placeholder="Search here.." required="">
                                    <button type="submit" class="theme-btn"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div><!--Nav Outer End-->

            </div>
        </div>
    </div>
</div>

<!--Sticky Header-->
<div class="sticky-header">
    <div class="auto-container clearfix">
        <!--Logo-->
        <div class="logo pull-left">
            <a href="<?php echo base_url()?>index" title="Factories"><h2 class="logo-text" ">Global Green Eco</h2></a>
        </div>

        <!--Right Col-->
        <div class="right-col pull-right">
            <!-- Main Menu -->
            <nav class="main-menu">
                <div class="navbar-header">
                    <!-- Toggle Button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="navbar-collapse collapse clearfix">
                    <ul class="navigation clearfix">
                        <li class="current"><a href="<?php echo base_url()?>">Home</a></li>
                        <li class="dropdown"><a href="<?php echo base_url()?>about-us">About</a>
                            <ul>
                                <li><a href="<?php echo base_url()?>about-us-one">About</a></li>
                                <li><a href="<?php echo base_url()?>team">Our Team</a></li>
                                <li><a href="<?php echo base_url()?>faq">FAQ's</a></li>
                                <li><a href="<?php echo base_url()?>404">404 Page</a></li>
                                <li><a href="<?php echo base_url()?>testimonial">Testimonials</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#">Services</a>
                            <ul>
                                <li><a href="<?php echo base_url()?>services">View All Services</a></li>
                                <li><a href="<?php echo base_url()?>wpcDoor">WPC Doors</a></li>
                                <li><a href="<?php echo base_url()?>services-single">Agricultural Processing</a></li>
                                <li><a href="<?php echo base_url()?>services-single">Mechanical Engineering</a></li>
                                <li><a href="<?php echo base_url()?>services-single">Chemical Research</a></li>
                                <li><a href="<?php echo base_url()?>services-single">Power And Energy</a></li>
                                <li><a href="<?php echo base_url()?>services-single">Patroleum & Gas</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url()?>projects">Projects</a></li>
                        <li class="dropdown"><a href="#">Shop</a>
                            <ul>
                                <li><a href="<?php echo base_url()?>shop">Our Shop</a></li>
                                <li><a href="<?php echo base_url()?>shop-single">Product Details</a></li>
                                <li><a href="<?php echo base_url()?>shopping-cart">Shopping Cart</a></li>
                                <li><a href="<?php echo base_url()?>checkout">Checkout</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#">News</a>
                            <ul>
                                <li><a href="<?php echo base_url()?>blog">News</a></li>
                                <li><a href="<?php echo base_url()?>blog-single">News Details</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url()?>contact">Contact</a></li>

                    </ul>
                </div>
            </nav><!-- Main Menu End-->
        </div>

    </div>
</div><!--End Sticky Header-->

</header>