﻿

<!--Page Title-->
<section class="page-title"
         style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);"
         xmlns="http://www.w3.org/1999/html">
    <div class="auto-container">
        <h1>Products</h1>
        <div class="bread-crumb-outer">
            <ul class="bread-crumb clearfix">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li class="active">Products</li>
            </ul>
        </div>
    </div>
</section>


<!--Sidebar Page-->
<div class="sidebar-page-container with-left-sidebar">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Content Side-->
            <div class="content-side pull-right col-lg-9 col-md-8 col-sm-12 col-xs-12">
                <!--Service Details-->
                <section class="service-details">
                    <?php
                        foreach($images as $data) {
                        echo '<a href="'.base_url().$data->image .'">
        <figure class="big-image"><img src="'.base_url().$data->image .'" alt="" style="height:800px; width:800px;"></figure>';
                    }
                    ?>

                    <!--Text Content-->
                <?php
                foreach ($products as $data)
                {
                    echo '<div class="content">
                        <h3>'.$data->name.' </h3>
                        <p>'.$data->description.'</p>
                        <div class="faqs">
                        <!--Accordion Box-->
                        <ul class="accordion-box">

                            <!--Block-->
                            <li class="accordion block active-block">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon fa fa-angle-right"></span></div>Product Dimensions</div>
                                <div class="acc-content current">
                                    <div class="content"><p>
                                    Size Available :'.$data->size.'<br>
                                      Weight :'. $data->weight.'<br>
                                      </div>
                            </li>';
                            }?>
                                <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div> Product Images</div>
                                <div class="acc-content">
                                    <div class="content">
                                            <div class="row">
                                                <?php
                                                foreach($images as $data) {
                                                    echo '<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                              <a href="' . base_url() . $data->image . '"><img src="' . base_url() . $data->image . '" class="img-responsive" style="height: 200px; width: 200px;"></a>
                                            </div>
                                            ';}?>
                                            </div>
                                            <div>
                                            </div>
                                        </div>
                            </li>
                    <li class="accordion block">
                        <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div> Sample Images</div>
                        <div class="acc-content">
                            <div class="content">
                                <div class="row">
                                    <?php
                                    foreach($sample_images as $data) {
                                        echo '<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                                <img src="' . base_url() . $data->sample_image . '" class="img-responsive" style="height: 200px; width: 200px;">
                                                <div>'.$data->color_code.'</div>
                                            </div>
                                            ';}?>
                                </div>
                                <div>
                                </div>
                            </div>
                    </li>

                    </ul><!--End Accordion Box-->
                    </div>
                </section>

        </div>
        </div>
    </div>
</div>





                                        <!--photo gallery-->


            <!--Content Side-->
