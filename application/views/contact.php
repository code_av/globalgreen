﻿
    <!--End Main Header -->



    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url() ?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Contact Us</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li class="active">Contact Us</li>
                </ul>
            </div>
        </div>
    </section>


    <!--Contact Section-->
    <section class="contact-section">
    	<div class="auto-container">
        	
            
        	<div class="row clearfix">
            
                <!--Form Container-->
                <div class="form-column col-md-8 col-sm-12 col-xs-12">
                
                    <h2>Contact Us</h2>
                    
                    <!--Contact Form-->
                    <div class="default-form contact-form">
                        <form method="post" action="<?php echo base_url()?>contactMail" id="contact-form">
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                	<div class="form-group  ">
                                        <input type="text" name="username" value="" placeholder="Name" required="">
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="email" name="email" value="" placeholder="Email" required="">
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="text" name="subject" value="" placeholder="Subject">
                                    </div>
                                </div>
                                
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                	<div class="form-group">
                                        <textarea name="message" placeholder="Message" required=""></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="theme-btn btn-style-one">send Message</button>
                                </div>
                                
                            </div>
                        </form>
                    </div><!--End Contact Form-->
                    
                </div>
                
            	<!--Info Column-->
                <div class="info-column col-md-4 col-sm-12 col-xs-12">
                	<h2>Get in Touch with us</h2>
                    <ul class="contact-info">
                        <a href="tel:+91941502936"><li><div class="icon-box"><span class="fa fa-phone"></span></div>(+91) 9415029036</li></a>
                        <a href="mailto:orders@globalgreeneco.com"><li><div class="icon-box"><span class="fa fa-envelope-o"></span></div>orders@globalgreeneco.com</li></a>
                    	<li><div class="icon-box"><span class="fa fa-map-marker"></span></div>1, Lyall Road, Civil Lines , Allahabad(U.P.), 211003.<a href="https://www.google.com/maps/search/25.4498054,81.8384797/@25.4498054,81.836291,17z?hl=en" target="_blank">Google Map<i class="fa fa-external-link"></i></a></li>
                    </ul>
                    
                </div>
                
            </div>




        </div>
    </section>
