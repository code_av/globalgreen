﻿    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Services</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li class="active">Services</li>
                </ul>
            </div>
        </div>
    </section>
    
    <section class="featured-area">
        <div class="container">

            <div class="row featured">
                <!--Start single featured item-->
                <?php
                foreach($services as $service)
                {
                ?>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="single-featured-item">
                        <div class="icon-holder">
                            <div class="icon-holder">
                            <div class="icon-bg">
                                <img src="<?php echo base_url().$service->service_icon?>" style="border-radius: 50%; height:100px; width:100px;">
                            </div>
                                </div>
                        </div>
                        <div class="text-holder">
                            <h3><?php echo $service->service_title?></h3>
                            <p style="text-align: justify;"><?php echo $service->service_description ; ?></p>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <!--End single featured item-->
            </div><!--End single featured item-->
        </div>
    </section>
    
