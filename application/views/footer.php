<footer class="main-footer">
    <div class="footer-upper">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-md-4 col-sm-12">
                    <div class="row clearfix" style="margin-left: -7px !important; margin-right: -7px !important;">
                        <div class="column">
                            <div class="footer-widget about-widget">
                                <h3>About Us</h3>
                                <div class="text margin-bott-30">
                                    <p style="text-align: justify;">
                                        We expertise in design and installation of UPVC, WPC construction projects including
                                        Designer Kitchens, steps, UPVC doors and windows ,WPC products, hardware fittings,
                                        WPC doors/Windows.
                                    </p>
                                </div>
                                <div class="social-links">
                                    <a target="_blank" href="https://www.facebook.com/globalgreeneco/"><span class="fa fa-facebook-f"></span></a>
                                    <a target="_blank" href="https://twitter.com/globalgreeneco"><span class="fa fa-twitter"></span></a>
                                    <a target="_blank" href="https://www.instagram.com/globalgreeneco/"><span class="fa fa-instagram"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <div class="column">
                            <div class="footer-widget links-widget">
                                <h3 style="color:#FF1356;padding-left: 15px;">Global Green Eco Technologies</h3>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-sm-6 col-xs-12" style="padding-left: 25px;">
                                        <ul>
                                            <li><i class="fa fa-angle-right"></i><a href="<?php echo base_url()?>">Home</a></li>
                                            <li><i class="fa fa-angle-right"></i><a href="<?php echo base_url().'about'?>">About Us</a></li>
                                            <li><i class="fa fa-angle-right"></i><a href="<?php echo base_url().'services'?>">Services</a></li>
                                            <li><i class="fa fa-angle-right"></i><a href="<?php echo base_url().'contact'?>">Contact</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="row clearfix">
                        <div class="column">
                            <div class="footer-widget newsletter-widget">
                                <h3 class="centered">Gallery</h3>
                                <div class="newsletter-form">
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door8.jpg"> <img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door6.jpg" class="img-responsive"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door7.jpg"><img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door7.jpg" class="img-responsive"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door8.jpg"><img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door8.jpg" class="img-responsive"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door9.jpg"><img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door9.jpg" class="img-responsive"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door10.jpg"><img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door10.jpg" class="img-responsive"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door11.jpg"><img src="<?php echo base_url()?>public/assets/gallery1/images/service/doors/door11.jpg" class="img-responsive"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-6 col-xs-12"><div class="copyright-text">© 2018 Developed By<a href="http://advikbot.com" target="_blank"> Advik Bot</a></div></div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <nav class="footer-nav clearfix">
                        <ul class="pull-right clearfix">
                            <li>Global Green Eco Technologies</li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>
<input type="hidden" value="<?php echo $this->session->flashdata('success');?>" id="notify">
<input type="hidden" value="<?php echo $this->session->flashdata('error');?>" id="notify_error">
<script src="<?php echo base_url() ?>public/assets/js/jquery.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script>
    $(document).ready( function() {
        console.log('hey');
        var msg = $('#notify').val();
        console.log(msg);
        if (msg!="") {
            $.confirm({
                title: 'Success',
                content: msg,
                buttons: {

                    ok: function () {
                    }
                }
            });
        }
        var error = $('#notify_error').val();
        console.log(error);
        if (error!="") {
            $.confirm({

                title: 'Error!',
                content: error,
                buttons: {

                    ok: function () {
                    }
                }
            });
        }



    });
</script>

<script src="<?php echo base_url() ?>public/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/jQuery.style.switcher.min.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/jquery.appear.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/revolution.min.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/jquery.fancybox-media.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/owl.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/wow.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/appear.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/smoothscroll.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/isotope.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/validation.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/script.js"></script>
<script src="<?php echo base_url() ?>public/assets/js/sticky.js"></script>


<script src="<?php echo base_url()?>public/assets/js/map-script.js"></script>

</body>
</html>