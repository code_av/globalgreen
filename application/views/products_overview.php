﻿

<!--Page Title-->
<section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <h1>Products</h1>
        <div class="bread-crumb-outer">
            <ul class="bread-crumb clearfix">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="<?php echo base_url()?>services">Products</a></li>
                <!--<li class="active">WPC Door</li>-->
            </ul>
        </div>
    </div>
</section>


<!--Sidebar Page-->
<div class="sidebar-page-container with-left-sidebar">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Content Side-->
            <div>
            <div class="content-side pull-right col-lg-9 col-md-8 col-sm-12 col-xs-12">
                <!--Service Details-->
                <div class="content">
                    <!--photo gallery-->
                    <div class="row">
                        <?php

                        $page_url=current_url();
                        foreach($products as $data) {
                            if (isset($data->product_id)) {
                                echo '<div class="gallery_product col-lg-4 col-md-4 col-sm-4  filter hdpe">
                            <a href="' .$page_url . '/' . $data->product_id . '">
                             <div class="inner hover-style1  hvr-float-shadow">
                                    <figure class="image-box hover-style1-img">
                            <img src="' . $data->image . '" class="img-responsive">
                            <div class="hover-style1-view">
                                        <a class="link-box" href="' .$page_url . '/' . $data->product_id . '" style="margin-left: -40px; height: 50px !important; width: 50px !important;">
                                            <span class="fa fa-shopping-cart" style="font-size: 25px; color: #7FB302;"></span></a>
                                        <a class="link-box" href="' .$page_url . '/' . $data->product_id . '" style="margin-left: 40px; height: 50px !important; width: 50px !important;">
                                            <span class="fa fa-search" style="font-size: 25px; color: #7FB302;"></span></a>
                                    </div>
                                    </figure>
                                    <figcaption style="text-align: center;">
                            <div >' . $product_name . '</div>
                            </figcaption>
                            
                        </div>';
                            }
                        }
                        ?>
                        <div>
                        </div>
                    </div>
                    </li>
                    </ul><!--End Accordion Box-->

                </div>
            </div>

        </div>