﻿<section class="page-title" style="background-image:url(<?php echo base_url() ?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <h1>About Us</h1>
        <div class="bread-crumb-outer">
            <ul class="bread-crumb clearfix">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li class="active">About Us</li>
            </ul>
        </div>
    </div>
</section>
<section class="about-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="content-column col-md-8 col-sm-8">
                    <div class="inner-box">
                        <div class="sec-title">
                            <h2>Welcome to <span>Our Workshop</span></h2>
                        </div>
                    </div>
                    <div class="text dark-text" style="text-align: justify">
                        <p>Global Green Eco Technologies is a high-tech joint venture which is specialized in
                            marketing the environmental friendly WPC products, Upvc Products And Hardware Fittings
                            like Door Controls, Glasses , Hydraulic Patches and many more Products. and Our mission
                            is to Provide products for the modern world, we strive hard to reach the new heights
                            of success accompanied by our privileged customers.
                        </p>
                        <p>Global Green Eco Technologies Pvt. Ltd. foresees to satisfy the increasing demand of
                            decorative ideas in various sectors by providing the best materials to embellish
                            them in to the most innovative and graceful way.
                        </p>
                        <p>Global Green Eco Technology Pvt. Ltd. aims to provide consumers with trendiest and
                            user-friendly innovations that fulfil their various types of requirements ,resulting
                            in gratitude and satisfaction of clients.with a strong adherence to quality and
                            precision. Global Green promises to serve the customer with the best and most diverse
                            portfolio and satisfactory services
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo base_url();?>public/assets/gallery1/images/resource/hb5.jpg" style="width: 400px;width:450px;" class="img-responsive">
                </div>
            </div>
        </div>
</section>
<section class="why-chooseus sec-padd2">
    <div class="container">
        <div class="sec-title centered">
            <h2>why <span>choose Us</span></h2>
        </div>
        <div class="row">
            <div class="item col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="inner-box wow fadeIn  animated animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeIn;">
                    <div class="icon_box">
                        <span class="flaticon-tools"></span>
                    </div>
                    <h4>SUPERIOR QUALITY</h4>
                    <div class="text"><p>Innovatively designed range that enhances the appeal and style of buildings, with best quality products. </p></div>
                </div>
            </div>
            <div class="item col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="inner-box wow fadeIn  animated animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeIn;">
                    <div class="icon_box">
                        <span class="flaticon-arrows"></span>
                    </div>
                    <h4>COMPETITIVE PRICE</h4>
                    <div class="text">We set a price of our products according to your pocket with a best quality.  </p></div>
                </div>
            </div>
            <div class="item col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="inner-box wow fadeIn  animated animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeIn;">
                    <div class="icon_box">
                        <span class="flaticon-stopwatch"></span>
                    </div>
                    <h4>TIME DELIVERY</h4>
                    <div class="text"><p>We know the value of time , so we deliver the products within a commited time</p></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="testimonial">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2>Directors</h2>
        </div>
        <div class="row clearfix">
            <div class="col-md-6">
            <article class="col-md-8 col-sm-6 col-xs-12 pull-right" >
                <div class="testimonial-item">
                    <div class="content">
                        <span class=""><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/user1.png" alt=""  style="height:50px;width:70px;border-radius: 90%; margin-bottom: 10px;"></span>
                        <p><a>Mr. Dhirendra Pratap Singh</a>
                            Contact:<a href="tel:941502936"><i class="flaticon-telephone-1"></i>9415236504 </a><br>
                            Email:<a href="mailto:dpsinghpe@yahoo.in">dpsinghpe@yahoo.in</a><br>
                        </p>
                    </div>
                </div>
            </article>
            </div>
                <div class="col-md-6" style="text-align: center;">
            <article class="col-md-8 col-sm-6 col-xs-12 centered" >
                <div class="testimonial-item">
                    <div class="content">
                        <span class=""><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/user1.png" alt=""  style="height:50px;width:70px;border-radius: 90%; margin-bottom: 10px;"></span>
                        <p><a>Mr. Navneet Agarwal</a>
                        Contact:<a href="tel:9415216113"><i class="flaticon-telephone-1"></i>9415236504 </a><br>
                            Email:<a href="mailto:hcc088    @yahoo.in">hcc088@yahoo.in</a></p>
                    </div>
                </div>
            </article>
                </div>
        </div>
    </div>
</section>
<section class="why-chooseus sec-padd2">
    <a name="certificates"></a>
    <div class="container">
        <div class="sec-title centered">
            <h2>Certified <span>By</span></h2>
        </div>
            <div class="row clearfix">
                <div class="column">
                    <div class="footer-widget newsletter-widget">
                        <div class="newsletter-form">
                            <div class="gallery_product col-lg-2 col-md-2 col-sm-2 col-xs-4 filter hdpe">
                                <div class="inner hover-style1  hvr-float-shadow">
                                    <figure class="image-box hover-style1-img">
                                <a href="<?php echo base_url()?>public/assets/docs/ISO9001.pdf" target="_blank"> <img src="<?php echo base_url()?>public/assets/docs/ISO9001.png" class="img-responsive" style="height: 200px; width: 200px; border: 1px solid black;"></a>
                                        <div class="hover-style1-view">
                                            <a class="link-box" href="<?php echo base_url()?>public/assets/docs/ISO9001.pdf" target="_blank"><span class="fa fa-link"></span></a>
                                        </div>
                                    </figure>
                                    <div class="lower-box" style="text-align: center">
                                        <h4 style="color:black;">ISO 9001:2008</h4>
                                    </div>
                                </div>
                                </div>
                            <div class="gallery_product col-lg-2 col-md-4 col-sm-2 col-xs-4 filter sprinkle">
                                 <div class="inner hover-style1  hvr-float-shadow">
                                    <figure class="image-box hover-style1-img">
                                <a href="<?php echo base_url()?>public/assets/docs/ISO14001.pdf" target="_blank"><img src="<?php echo base_url()?>public/assets/docs/ISO14001.png" class="img-responsive" style="height: 200px; width: 200px; border: 1px solid black;"></a>
                             <div class="hover-style1-view">
                                            <a class="link-box" href="<?php echo base_url()?>public/assets/docs/ISO14001.pdf" target="_blank"><span class="fa fa-link"></span></a>
                                        </div>
                                    </figure>
                                    <div class="lower-box" style="text-align: center">
                                        <h4 style="color:black;">ISO 14001:2004</h4>
                                    </div>
                                 </div>
                            </div>
                            <div class="gallery_product col-lg-2 col-md-2 col-sm-2 col-xs-4 filter hdpe">
                                 <div class="inner hover-style1  hvr-float-shadow">
                                    <figure class="image-box hover-style1-img">
                                <a href="<?php echo base_url()?>public/assets/docs/MPHousing.pdf" target="_blank"><img src="<?php echo base_url()?>public/assets/docs/MPHousing.png" class="img-responsive" style="height: 200px; width: 200px; border: 1px solid black;"></a>
                             <div class="hover-style1-view">
                                            <a class="link-box" href="<?php echo base_url()?>public/assets/docs/MPHousing.pdf" target="_blank"><span class="fa fa-link"></span></a>
                                        </div>
                                    </figure>
                                    <div class="lower-box" style="text-align: center">
                                        <h4 style="color:black;">MP Housing</h4>
                                    </div>
                                    </div>
                                 </div>
                            <div class="gallery_product col-lg-2 col-md-2 col-sm-2 col-xs-4 filter sprinkle">
                                 <div class="inner hover-style1  hvr-float-shadow">
                                    <figure class="image-box hover-style1-img">
                                <a href="<?php echo base_url()?>public/assets/docs/NBCC.pdf" target="_blank"><img src="<?php echo base_url()?>public/assets/docs/NBCC.png" class="img-responsive" style="height: 200px; width: 200px; border: 1px solid black;"></a>
                             <div class="hover-style1-view">
                                            <a class="link-box" href="<?php echo base_url()?>public/assets/docs/NBCC.pdf" target="_blank"><span class="fa fa-link"></span></a>
                                        </div>
                                    </figure>
                                    <div class="lower-box" style="text-align: center">
                                        <h4 style="color:black;">NBCC Approval</h4>
                                    </div>
                                    </div>
                                 </div>
                            <div class="gallery_product col-lg-2 col-md-2 col-sm-2 col-xs-4 filter hdpe">
                                 <div class="inner hover-style1  hvr-float-shadow">
                                    <figure class="image-box hover-style1-img">
                                <a href="<?php echo base_url()?>public/assets/docs/QHSAS18001.pdf" target="_blank"><img src="<?php echo base_url()?>public/assets/docs/QHSAS18001.png" class="img-responsive" style="height: 200px; width: 200px; border: 1px solid black;"></a>
                             <div class="hover-style1-view">
                                            <a class="link-box" href="<?php echo base_url()?>public/assets/docs/QHSAS18001.pdf" target="_blank"><span class="fa fa-link"></span></a>
                                        </div>
                                    </figure>
                                    <div class="lower-box" style="text-align: center">
                                        <h4 style="color:black;">QHSAS 18001:2007</h4>
                                    </div>
                                    </div>
                                 </div>
                            <div class="gallery_product col-lg-2 col-md-2 col-sm-2 col-xs-4 filter sprinkle">
                                 <div class="inner hover-style1  hvr-float-shadow">
                                    <figure class="image-box hover-style1-img">
                                <a href="<?php echo base_url()?>public/assets/docs/winwall.pdf" target="_blank"><img src="<?php echo base_url()?>public/assets/docs/winwall.png" class="img-responsive" style="height: 200px; width: 200px; border: 1px solid black;"></a>
                             <div class="hover-style1-view">
                                            <a class="link-box" href="<?php echo base_url()?>public/assets/docs/winwall.pdf" target="_blank"><span class="fa fa-link"></span></a>
                                        </div>
                                    </figure>
                                    <div class="lower-box" style="text-align: center">
                                        <h4 style="color:black;">Winwall Test Certificate  </h4>
                                    </div>
                                    </div>
                                 </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>
<section class="why-chooseus sec-padd2" style="background-color:#EAEAEA;">
    <div class="container">
        <div class="sec-title centered">
            <h2>Our<span> Brands</span></h2>
        </div>
        <div class="row" >
            <div class="container" style="">
                 <div class="col-lg-4 col-md-4 col-sm-2 col-xs-4">
                     <a href="<?php echo base_url()?>axilam">
                         <img src="<?php echo base_url()?>public/assets/gallery1/images/home/axilam.png" alt="<?php echo base_url()?>axilam">
                     </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-2 col-xs-4">
                    <a href="<?php echo base_url()?>alstone">
                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/alstone.png" alt="<?php echo base_url()?>alstone"  >
                    </a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-2 col-xs-4">
                    <a href="<?php echo base_url()?>ozone">
                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/ozone.png" alt="<?php echo base_url()?>ozone">
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
