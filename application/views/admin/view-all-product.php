<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>>
            <a href="#" title="Go to Home" class="tip-bottom"> products</a>
        </div>
        <h1> Products</h1>
    </div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
								<span class="icon">
									<i class="icon-info-sign"></i>
								</span>
                    <h5>Select Products</h5>
                </div>
                <div class="widget-content nopadding">

                    <form class="form-horizontal" method="post" action="<?php echo base_url().'view'?>"  name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                        <div class="control-group">
                            <label class="control-label " >Choose Category</label>
                            <div class="controls">
                                <select class="span5 category" name="category" id="category">
                                    <option>Select</option>;
                                    <?php
                                    //var_dump($data);
                                    foreach($category as $options)
                                    {

                                        echo "<option value=".$options->category_id.">".$options->category_name."</option>";
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Choose Sub Category:</label>
                            <div class="controls " >
                                <select class="span5 subcategory" name="subcategory" id="subcategory">
                                    <option ></option>
                                </select>

                            </div>
                        </div>
                        <div class="form-actions">
                                <input type="submit" value="View Products" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


<?php
if(isset($products))
{
?>
    <div class="row-fluid">
        <div class="span12" >
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="icon-th"></i></span>
                    <h5>Available Products</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Size</th>
                            <th>Weight</th>
                            <th>Material</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="gradeA">
                            <?php

                            foreach($products as $data) {
                                echo '
                                    <tr>';
                                if (isset($data->name)) {
                                    echo '<td>' . $data->name . '</td>';
                                } else {
                                    echo '<td></td>';
                                }
                                if (isset($data->size)) {
                                    echo '<td>' . $data->size . '</td>';
                                } else {
                                    echo '<td></td>';
                                }
                                if (isset($data->weight)) {
                                    echo '<td>' . $data->weight . '</td>';
                                } else {
                                    echo '<td></td>';
                                }
                                if (isset($data->material)) {
                                    echo '<td>' . $data->material. '</td>';
                                } else {
                                    echo '<td></td>';
                                }
                                echo '<td><a href="' . base_url() . 'edit-product/' . $data->product_id . '">EDIT</a>|<a href="' . base_url() . 'delete-product/' . $data->product_id . '">DELETE</a>|<a href="' . base_url() . 'view-details/' . $data->product_id . '">VIEW</a>|'; ?>
                                <?php if ($data->product_status == 0) {
                                    echo '<a href="' . base_url() . 'activate-product/' . $data->product_id . '" onclick="return confirmChangeProductStatus();">ACTIVATE</a></td>
                                    </tr>';
                                }
                                if ($data->product_status == 1) {
                                    echo '<a href="' . base_url() . 'de-activate-product/' . $data->product_id . '" onclick="return confirmChangeProductStatus();">DE-ACTIVATE</a></td>
                                    </tr>';
                                }
                            }
                            } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>





</div>