<!DOCTYPE html>
<html lang="en">

<head>
    <!--Favicons-->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <title>Global Green Admin</title><meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="../jquery.simplePagination.js"></script>
    <link href="../simplePagination.css" rel="stylesheet" type="text/css" />


    <style type="text/css">
        table {
            width: 40em;
            margin: 2em auto;
        }

        thead {
            background: #000;
            color: #fff;
        }

        td {
            width: 10em;
            padding: 0.3em;
        }

        tbody {
            background: #ccc;
        }

    </style>

    <script>

        function test(pageNumber)
        {

            var page="#page-id-"+pageNumber;
            $('.select').hide()
            $(page).show()

        }

    </script>




    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/colorpicker.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/datepicker.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/fullcalendar.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/maruti-style.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/maruti-media.css" class="skin-color" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/add.css" class="skin-color" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/uniform.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/select2.css"/>
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/jquery.gritter.css"  />


    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet" />

    <style type="text/css" rel="stylesheet">
        #files
        {
            display: block;
        }
    </style>
    <!--<link rel="stylesheet" href="<?php /*echo base_url()*/?>public/assets/adminCss/maruti-login.css"  />-->
    <script
            src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
            crossorigin="anonymous"></script>

</head>
<body>

<?php
if (!$this->ion_auth->logged_in())
{
    redirect('login');
}

$this->load->view('admin/alert');
?>



<!--Header-part-->
<div id="header">
    <h1><a href="dashboard.html"></a></h1>
</div>
<!--close-Header-part-->

<!--top-Header-messaages-->
<div class="btn-group rightzero"> <a class="top_message tip-left" title="Manage Files"><i class="icon-file"></i></a> <a class="top_message tip-bottom" title="Manage Users"><i class="icon-user"></i></a> <a class="top_message tip-bottom" title="Manage Comments"><i class="icon-comment"></i><span class="label label-important">5</span></a> <a class="top_message tip-bottom" title="Manage Orders"><i class="icon-shopping-cart"></i></a> </div>
<!--close-top-Header-messaages-->

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
    <ul class="nav">
<!--        <li class="" ><a title="" href="#"><i class="icon icon-user"></i> <span class="text">Profile</span></a></li>-->
        <li class=""><a title="" href="<?php echo base_url()?>logout"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
    </ul>
</div>

<!--close-top-Header-menu-->

<div id="sidebar">
        <a href="#" class="visible-phone"><i class="icon icon-th-list"></i> Menu</a>
        <ul>
        <li class="active"><a href="<?php echo base_url()?>admin-dashboard"><i class="icon icon-home" style="color:white;"></i> <span>Dashboard</span></a> </li>
        <li> <a href="<?php echo base_url()?>add-category"><i class="icon icon-pencil" style="color:white;"></i><span>Category</span></a> </li>
        <li> <a href="<?php echo base_url()?>add-sub-category"><i class="icon icon-pencil" style="color:white;"></i><span>Sub Category</span></a> </li>
        <li><a href="<?php echo base_url()?>add-distributor"><i class="icon icon-pencil" style="color:white;"></i><span>Distributor</span></a></li>
        <li><a href="<?php echo base_url()?>add-product"><i class="icon icon-pencil" style="color:white;"></i><span>Products</span></a></li>


    </ul>
</div>




