<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>>
            <a href="#" class="current">view Order</a>
        </div>
        <h1>View Order</h1>
    </div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" >
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="icon-th"></i></span>
                    <h5>order</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                            <th>Order id</th>
                            <th>Client Name</th>
                            <th>Product</th>
                            <th>Phone No.</th>
                            <th>Address</th>
                            <th>Message</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach($order_detail as $data)
                        {
                            echo '<tr>';
                                if(isset($data->order_detail_id)) {
                                    echo '<td>' . $data->order_detail_id . '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                                 if(isset($data->username)) {
                                    echo '<td>' . $data->username . '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                            if(isset($data->name)) {
                                echo '<td>' . $data->name . '</td>';
                            }
                            else{
                                echo '<td></td>';
                            }
                            if(isset($data->phone)) {
                                echo '<td>' . $data->phone . '</td>';
                            }
                            else{
                                echo '<td></td>';
                            }
                            if(isset($data->address)) {
                                echo '<td>' . $data->address . '</td>';
                            }
                            else{
                                echo '<td></td>';
                            }
                            if(isset($data->message)) {
                                echo '<td>' . $data->message . '</td>';
                            }
                            else{
                                echo '<td></td>';
                            }
                            echo '<td><a href="'.base_url().'delete-order/'.$data->order_detail_id.'">DELETE</a></td>';
                        }

                        ?>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>








