<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>>
            <a href="<?php echo base_url()?>/add-product" title="Go to Home" class="tip-bottom">Products</a>>
            <a href="#" title="Go to Home" class="tip-bottom"> product Details</a>
        </div>
        <h1> Product Details</h1>
    </div>
    <div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
								<span class="icon">
									<i class="icon-eye-open"></i>
								</span>
                    <h5>product Details</h5>
                    <div class="span10">
                        <?php foreach($products as $data)
                        {
                        echo '<a href="'.base_url().'edit-product/'.$data['product_id'].'">';
                        }?>
                            <input type="button" value="Edit" class="btn btn-success pull-right">
                        </a>
                    </div>

                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>details</th>
                        </tr>
                        </thead>
                        <tbody> <?php
                        foreach($products as $data)
                        {

                            echo '<tr>
                            <td>Product Code:</td>
                            <td>'.$data['product_code'].'</td>
                            </tr>
                           <tr><td>Product Name:</td><td>'.$data['name'].'</td></tr>
                        
                        
                           <tr><td>Product Size:</td><td>'.$data['size'].'</td></tr>
                        
                        
                           <tr><td>Product Weight:</td><td>'.$data['weight'].'</td></tr>
                        
                        
                           <tr><td>Product Price:</td><td>'.$data['price'].'</td></tr>
                        
                        
                           <tr><td>Product Description:</td><td>'.$data['description'].'</td></tr>
                        
                        
                           <tr><td>Product material:</td><td>'.$data['material'].'</td></tr>
                           <tr colspan="2"><td colspan="2">
                        
                        ';
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row-fluid">
                            <div class="span12">
                                <div class="widget-box">
                                    <div class="widget-title">
								<span class="icon">
									<i class="icon-picture"></i>
								</span>
                                        <h5>Gallery</h5>
                                    </div>
                                    <div class="widget-content">
                                        <ul class="thumbnails">

                                <?php

                                foreach($products as $data)
                                {

                                foreach($data['images'] as $img) {
                                    echo '
                
                                    <li class="span2">
                                            <img src="' . base_url() . $img['image'] .'" alt="" style="width: 120px; height: 100px;">
                                        <div class="actions">
                                            <a title="" href="' . base_url().'delete-image/'. $img['gallery_id'] .'/'.$data['product_id'].'"><i class="icon-remove icon-white"></i></a>
                                        </div>
                                    </li>
                                    ';
                                }
                                }
                                ?>
                                </ul>
                            </div>
                        </div>
            </div>
        </div>
        </div>
            </div>
        </div>
    </div>







