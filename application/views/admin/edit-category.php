<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>>
            <a href="#" title="Go to Home" class="tip-bottom">edit-ategory</a>
        </div>
        <h1>Edit Category</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">

            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5> Edit Category Form</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <?php
                        foreach ($category_details as $data) {
                            echo '<form action="' . base_url() . 'update-category" method="post" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">Category Code :</label>
                                <div class="controls">
                                    <input type="text" class="span5" placeholder="Enter Category Code" name="categorycode" id="categorycode" value="' . $data->category_code . '"/><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                    <input type="hidden" name="category_id" value="'.$data->category_id.'">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Category Name :</label>
                                <div class="controls">
                                    <input type="text" class="span5" placeholder="Enter Category Name" name="categoryname" id="categoryname" value="' . $data->category_name . '"/><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--table-->

        </div>


        </div>
    </div>';}
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



