<div id="content">
    <div id="content-header">
        <div id="breadcrumb">
            <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>>
            <a href="#" class="current">Edit Product</a>
        </div>
        <h1>Edit Product</h1>
    </div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
								<span class="icon">
									<i class="icon-info-sign"></i>
								</span>
                    <h5>Edit Products</h5>
                </div>
                <div class="widget-content nopadding">
                    <?php
                        foreach ($product_details as $data) {
                       echo
                    '<form class="form-horizontal" method="post" action="'. base_url().'update-product"  name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                        <div class="control-group">
                            <label class="control-label">Product Code</label>
                            <div class="controls">
                                <input type="text" required="" class="span5" placeholder="Enter Product Code" name="productcode" id="productcode" value="'.$data->product_code.'" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product Name</label>
                            <div class="controls">
                                <input type="text" required="" class="span5" placeholder="Enter Product Name" name="productname" id="productname" value="'.$data->name.'"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product Size</label>
                            <div class="controls">
                                <input type="text" class="span5" placeholder="Enter Product Size" name="productsize" id="productsize" value="'.$data->size.'"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product Weight</label>
                            <div class="controls">
                                <input type="text" class="span5" placeholder="Enter Product Weight" name="productweight" id="productweight" value="'.$data->weight.'"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product Price</label>
                            <div class="controls">
                                <input type="number" required="" class="span5" placeholder="Enter Product Price" name="productprice" id="productprice" value="'.$data->price.'"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product Description</label>
                            <div class="controls">
                                <textarea class="span5" required="" placeholder="Enter Product Description" name="productdescription" id="productdescription"  />'.$data->description.'</textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product material</label>
                            <div class="controls">
                                <input type="text" class="span5" placeholder="Enter Product Material" name="productmaterial" id="productmaterial" value="'.$data->material.'"/>
                                <input type="hidden" name="productid" id="productid" value="'.$data->product_id.'"/>
                            </div>
                        </div>
                             <div class="control-group">
                            <label class="control-label">Distributor:</label>
                            <div class="controls">
                            <select name="distributor">
                            <option value="'.$products[0]['distributor_id'].'">'.$products[0]['distributor_name'].'</option>';?>
                     <?php foreach ($distributor as $dist){
                         if($dist->distributor_id!=$data->distributor_id){
                      echo '<option value="'.$dist->distributor_id.'">'.$dist->distributor_name.'</option>';
    }
    }
}
?>
                            </select>
                            </div>
                        </div>                        
                        
                        <div class="control-group">
                            <h3>
                                <button type="button" id="reset" class="btn btn-primary pull-right">Reset Selection</button>
                            </h3>
                            <label class="control-label">Select Images</label>
                            <div class="controls">
                                <input type="file" id="files" name="product_img[]" multiple />
                            </div>
                        </div>


                        <div class="form-actions">
                            <a href="<?php echo base_url()?>add-product"><input type="button" value="Cancel" class="btn btn-warning" >
                            <input type="submit" value="Update" class="btn btn-success">
                        </div>
                    </form>
                </div>
                 </div>
            </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="icon-picture"></i>
                    </span>
                    <h5>Gallery</h5>
                </div>
                <div class="widget-content nopadding">
                    <ul class="thumbnails">
                        <?php
                        foreach($products as $data)
                        {
                            foreach($data['images'] as $img) {
                                echo '<li class="span2">
                                       <img src="' . base_url() . $img['image'] .'" alt="" >
                                        <div class="actions">
                                            <a title="" href="' . base_url().'delete-image/'. $img['gallery_id'] .'/'.$data['product_id'].'" style="color: #f00;"><i class="icon-remove icon-white"></i></a>
                                        </div>
                                    </li>
                                    ';
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>
</div>








