<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>>
            <a href="#" title="Go to Home" class="tip-bottom"> Distributor</a>
        </div>
        <h1> Distributor</h1>
    </div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5> Distributor Form</h5>
                </div>
                <div class="widget-content nopadding">
                    <form action="<?php echo base_url()?>submit-distributor" method="post" class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">Distributor Name :</label>
                            <div class="controls">
                                <input type="text" required="" class="span5" placeholder="Enter Distributor Name" name="distributorname" id="distributorname" /><span style="color: red;font-size:25px; margin: 3px;">*</span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">


        <div class="span12" >
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="icon-th"></i></span>
                    <h5>Available Distributors</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                            <th>Distributor Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="gradeA">
                            <?php
                            //var_dump($data);
                            foreach($distributor as $vendors)
                            {   echo '<tr>';
                                if(isset($vendors->distributor_name)) {
                                    echo '<td>' . $vendors->distributor_name . '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                                echo '<td><a href="'.base_url().'edit-distributor/'.$vendors->distributor_id.'">EDIT</a>|<a href="'.base_url().'delete-distributor/'.$vendors->distributor_id.'">DELETE</a></td>';
                                echo '</tr>';
                            } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</div>








