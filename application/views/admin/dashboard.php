<?php
if (!$this->ion_auth->logged_in())
{
    redirect('auth/login');
}

?>
<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
        <h1>Dashboard</h1>
    </div>
    <div class="container-fluid">
        <div class="quick-actions_homepage">
            <ul class="quick-actions">

                <li> <a href="view-all-products"> <i class="icon-dashboard" style="font-size: 30px;"></i><i style="color:blue; font-size: 20px;"><?php echo $count;?> </i><br>Products</a> </li>
                <li> <a href="#"> <i class="icon-dashboard" style="font-size: 30px;"></i><i style="color:blue; font-size: 20px;"><?php echo $count_orders;?></i><br> Orders</a> </li>


            </ul>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>Requested Orders</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                            <tr>

                                <th>Order Id</th>
                                <th>Client Name</th>
                                <th>phone no.</th>
                                <th>Product</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($order_details as $order)
                            {echo '
                            
                            <tr class="gradeA">
                                <td>'.$order->order_detail_id.'</td>
                                <td>'.$order->username.'</td>
                                <td>'.$order->phone.'</td>
                                <td>'.$order->name.'</td>
                                <td class="center">
                                <a href="'.base_url().'view-order/'.$order->order_detail_id.'"> VIEW</a>|<a href="'.base_url().'delete-order/'.$order->order_detail_id.'">DELETE</a></td>
                            </tr>';}?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>