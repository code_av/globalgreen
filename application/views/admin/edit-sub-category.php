<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>>
            <a href="#" title="Go to Home" class="tip-bottom"> edit sub-category</a>
        </div>
        <h1>Edit Sub Category</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">

            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5> Edit Sub Category Form</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <?php
                        foreach ($sub_category_details as $data) {
                            echo '<form action="' . base_url() . 'update-sub-category" method="post" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">Sub Category Name :</label>
                                <div class="controls">
                                    <input type="text" class="span5" placeholder="Enter Category Code" name="subcategoryname" id="subcategoryname" value="' . $data->sub_category_name . '"/>
                                    <input type="hidden" name="subcategoryid" value="'.$data->sub_category_id.'">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--table-->

        </div>


        </div>
    </div>';}
                        ?>



