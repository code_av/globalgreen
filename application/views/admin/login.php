<!DOCTYPE html>
<html lang="en">

<head>
    <title>Global Green Eco</title><meta charset="UTF-8" />
   <!-- favicons-->

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <!-- stylesheets-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?php echo base_url()?>public/assets/adminCss/maruti-login.css" />
</head>
<body>
<div id="loginbox">
    <form action="<?php echo base_url();?>loginUser" method='post' name='process' enctype="multipart/form-data">
    <?php if(! is_null($msg))
    "<p style="color: red;">".$msg.'</p>'?>
        <form id="loginform" class="form-vertical" action="<?php echo base_url()?>loginUser">
        <div class="control-group normal_text"> <h3><img src="<?php echo base_url()?>public/assets/gallery1/images/home/logo1.png" alt="Login" /></h3></div>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on"><i class="icon-user"></i></span>
                    <input type="text" placeholder="Username" name="username"/>
                </div>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <div class="main_input_box">
                    <span class="add-on"><i class="icon-lock"></i></span><input type="password" placeholder="Password" name="password" />
                </div>
            </div>
        </div>
            <br>
            <div>
                 <?php if(isset($data)) echo $data ?>
            </div>
        <div class="form-actions">
            <span class="pull-right"><input type="submit" class="btn btn-success" value="Login" /></span>
        </div>
    </form>
</div>
<script src="<?php echo base_url()?>public/assets/adminJs/jquery.min.js"></script>
<script src="<?php echo base_url()?>public/assets/adminJs/maruti.login.js"></script>
</body>
</html>