﻿
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>FAQ's</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="<?php echo base_url()?>about-us-one">About Us</a></li>
                    <li class="active">FAQ's</li>
                </ul>
            </div>
        </div>
    </section>
    
    
    <!--Team Section / Default-->
    <section class="faq-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <div class="faqs">
                        <!--Accordion Box-->
                        <ul class="accordion-box">
                        
                            <!--Block-->
                            <li class="accordion block active-block">
                                <div class="acc-btn active"><div class="icon-outer"><span class="icon fa fa-angle-right"></span></div> Why Choose Us Today?</div>
                                <div class="acc-content current">
                                    <div class="content"><p>Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.</p></div>
                                </div>
                            </li>
    
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div>Waht makes your financial projects special? </div>
                                <div class="acc-content">
                                    <div class="content"><p>Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.</p></div>
                                </div>
                            </li>
                            
                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div> Do you give any offer for premium customer?  </div>
                                <div class="acc-content">
                                    <div class="content"><p>Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.</p></div>
                                </div>
                            </li>

                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div> What makes you special from others? </div>
                                <div class="acc-content">
                                    <div class="content"><p>Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.</p></div>
                                </div>
                            </li>

                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div>How long will take us to raise capital?  </div>
                                <div class="acc-content">
                                    <div class="content"><p>Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.</p></div>
                                </div>
                            </li>

                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div>Hosting and Domain Servies  </div>
                                <div class="acc-content">
                                    <div class="content"><p>Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.</p></div>
                                </div>
                            </li>

                            <!--Block-->
                            <li class="accordion block">
                                <div class="acc-btn"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div>We Are The Best Consultant  </div>
                                <div class="acc-content">
                                    <div class="content"><p>Cum sociis natoque penatibus et magnis dis parturient ntesmus. Proin vel nibh et elit mollis commodo et nec augue tristique sed Quisque velit nisi, pretium.nibh.</p></div>
                                </div>
                            </li>
                            
    
                        </ul><!--End Accordion Box-->
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <!--Contact Form-->
                    <div class="default-form contact-form">
                        <form method="post" action="<?php echo base_url()?>sendemail" id="contact-form">
                            <div class="row clearfix">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="username" value="" placeholder="Name" required="">
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="email" name="email" value="" placeholder="Email" required="">
                                    </div>
                                    
                                    <div class="form-group">
                                        <input type="text" name="Phone" value="" placeholder="Phone">
                                    </div>

                                    <div class="form-group">
                                        <input type="text" name="subject" value="" placeholder="Subject">
                                    </div>

                                    <div class="form-group">
                                        <textarea name="message" placeholder="Message" required=""></textarea>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="theme-btn btn-style-one">send Message</button>
                                </div>
                                
                            </div>
                        </form>
                    </div><!--End Contact Form-->
                </div>
            </div>
        </div>
    </section>
    
