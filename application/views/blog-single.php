﻿

    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Blog Details</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>index">Home</a></li>
                    <li><a href="<?php echo base_url()?>blog">Blog</a></li>
                    <li class="active">Blog Details</li>
                </ul>
            </div>
        </div>
    </section>
    
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container with-right-sidebar gray-bg">
        <div class="auto-container">
            <div class="row clearfix">
				
                <!--Content Side-->
                <div class="content-side col-lg-9 col-md-8 col-sm-12 col-xs-12">
                    <section class="blog-details">
						
                        <div class="news-style-two">
                            <div class="inner-box">
                                <div class="image-box"><figure class="image"><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/single-news.jpg" alt=""></figure></div>
                                <div class="lower-box">
                                    <h3>Make your teams more productive, deliver lightning-fast, and more</h3>
                                    <div class="post-meta">
                                        <ul class="clearfix">
                                            <li><a href="#"><span class="fa fa-calendar"></span> Jan 05, 2017</a></li>
                                            <li><a href="#"><span class="fa fa-commenting-o"></span> 23 Comments</a></li>
                                        </ul>
                                    </div>
                                    <div class="text">
                                    	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci....commodapienporta Etiam eu molestie eros, Etiam eu molestie eros, commodapien.Maecenas tempus leo ac nisi iaculis porta..commodapien...</p>
                                        <p>Fusce non ante sed lorem rutrum feugiat. Vestibulum pellentesque, purus ut dignissim consectetur, nulla erat ultrices purus, ut consequat sem elit non sem. Morbi lacus massa, euismod ut turpis molestie, tristique sodales est. Integer sit amet mi id sapien tempor molestie in nec massa. Fusce non ante sed lorem rutrum feugiat. Vestibulum pellentesque, purus ut dignisim consectetur, nulla erat ultrices purus, ut consequat sem elit non sem. Fusce non ante sed lorem rutrum feugiat. Vestilum pell ent esque, purus ut dignissim consectetur, nulla erat ultrices purus, ut consequat sem elit non sem.</p>
                                        <blockquote><div class="txt">People think focus means saying yes to the thing you’ve got to focus on. But that’s not what it means at all. It means saying no to the hundred other good ideas that there are. You have to pick carefully. I’m actually as proud of the things.</div><div class="info">- David Warner</div></blockquote>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non laoreet dui. Morbi lacus massa, euismod ut turpis molestie, tristique sodales est. Integer sit amet mi id sapien tempor molestie in nec massa. Vestibulum pellentesque, purus ut dignissim consectetur, nulla erat ultrices purus, ut consequat sem elit non sem.</p>
                                        
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <ul class="list-style-one">
                                                    <li>Vestibulum pellentes, purus ut dignissim conssfecteftu.</li>
                                                    <li>Vestibulum pellentes, purus ut dignissim.</li>
                                                    <li>Vestibulum pellentes, purus ut dignissim moris.</li>
                                                    <li>Vestibulum pellentes, purus ut digniss.</li>
                                                </ul>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <ul class="list-style-one">
                                                    <li>Vestibulum pellentes, purus ut dignissim conssfecteftu.</li>
                                                    <li>Vestibulum pellentes, purus ut dignissim.</li>
                                                    <li>Vestibulum pellentes, purus ut dignissim moris.</li>
                                                    <li>Vestibulum pellentes, purus ut digniss.</li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                        <div class="row clearfix">
                                            <div class="image-column col-md-6 col-sm-6 col-xs-12">
                                                <figure><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/featured-image-8.jpg" alt=""></figure>
                                            </div>
                                            <div class="content-column col-md-6 col-sm-6 col-xs-12">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscin. Mauris non laoreet dui. Morbi lacus massa, euismod ut turpislestie, tristique sodales est. Integer sit amet mi id sapien temporte molestie in nec massa.</p>
                                                <p>Vestibulum pellentesque, purus ut dignissim consectetur, ul la erat ultrices purus, ut consequat sem elit non sem. Morbi lacus massa, euismod ut turpis molestie, tristique sodales. Lorem ipsum dolor sit ame.</p>
                                            </div>
                                        </div>
                                        
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris non laoreet dui. Morbi lacus massa, euismod ut turpis molestie, tristique sodales est. Integer sit amet mi id sapien tempor molestie in nec massa. Vestibulum pellentesque, purus ut dignissim consectetur, nulla erat ultrices purus, ut consequat sem elit non sem.</p>
                                    </div>
                                    
                                </div>
                                
                                <div class="post-options">
                                	<div class="tags"><strong>TAGS :</strong> &ensp; <a href="#">Business</a> , <a href="#">Consulting</a></div>
                                    <div class="categories"><strong>CATEGORIES :</strong> &ensp;  <a href="#">Accounts</a> , <a href="#">Portfolio</a></div>
                                    <div class="share-options">
                                    	<!--Social Links-->
                                        <ul class="social-links">
                                            <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                            <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                            <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        
                        <!--Comments Area-->
                        <div class="comments-area">
                        	<div class="group-title"><h2>03 Comments</h2></div>
                            
                            <div class="comment-box">
                            	<!--Comment-->
                                <div class="comment">
                                    <div class="comment-inner">
                                        <div class="author-thumb img-circle"><img class="img-circle" src="<?php echo base_url()?>public/assets/gallery1/images/resource/author-thumb-1.jpg" alt=""></div>
                                        <!--comment-content-->
                                        <div class="comment-content">
                                        	<div class="comment-info"><h4>Eric Rowan</h4><div class="date">January 7, 2017</div></div>
                                            <div class="text">Podcasting operational change management inside of workflows to establish a framework. Taking of seamless key performance indicators offline to maximise the long tail. </div>
                                            
                                            <a href="#" class="reply"><span class="fa fa-reply-all"></span> Reply</a>
                                        </div> 
                                    </div>
                                </div>
                                
                                <!--Comment / Reply-->
                                <div class="comment reply-comment">
                                    <div class="comment-inner">
                                        <div class="author-thumb img-circle"><img class="img-circle" src="<?php echo base_url()?>public/assets/gallery1/images/resource/author-thumb-3.jpg" alt=""></div>
                                        <!--comment-content-->
                                        <div class="comment-content">
                                            <div class="comment-info"><h4>Game smith</h4> <div class="date">January 10, 2017</div></div>
                                            <div class="text">Podcasting operational change management inside of workflows to establish a framework. Taking of seamless key performaximise the long tail. </div>
                                            
                                            <a href="#" class="reply"><span class="fa fa-reply-all"></span> Reply</a>
                                        </div> 
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <!-- Comment Form -->
                        <div class="comment-form">
    
                            <div class="group-title"><h2>Leave your Comment</h2></div>
    
                            <!--Comment Form-->
                            <form method="post" action="<?php echo base_url()?>blog">
                                <div class="row clearfix">
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <input type="text" name="username" placeholder="First Name" required="">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <input type="text" name="username" placeholder="Last Name" required="">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <input type="email" name="email" placeholder="Email" required="">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                        <input type="text" name="username" placeholder="Phone" required="">
                                    </div>
    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <textarea name="message" placeholder="Message"></textarea>
                                    </div>
    
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                        <button class="theme-btn btn-style-one" type="submit" name="submit-form">Send Message</button>
                                    </div>
    
                                </div>
                            </form>
    
                        </div><!--End Comment Form -->
                        
                    </section>


                </div>
                <!--Content Side-->
                
                <!--Sidebar-->
                <div class="sidebar-side col-lg-3 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">

                        <!-- Search Form -->
                        <div class="sidebar-widget search-box">
							<div class="sidebar-title"><h3>SEARCH</h3></div>
                            <form method="post" action="<?php echo base_url()?>blog">
                                <div class="form-group">
                                    <input type="search" name="search-field" value="" placeholder="Search here.. ">
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>

                        </div>


                        <!-- Categories -->
                        <div class="sidebar-widget categories">
                            <div class="sidebar-title"><h3>Category</h3></div>

                            <ul class="list">
                                <li><a href="#">Architecture Plans</a></li>
                                <li><a href="#">Construction Projects</a></li>
                                <li><a href="#">Paintings</a></li>
                                <li><a href="#">Electrical Works</a></li>
                                <li><a href="#">Plumbing Works</a></li>
                            </ul>

                        </div>


                        <!-- Recent Posts -->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title"><h3>Recent News</h3></div>

                            <article class="post">
                            	<figure class="post-thumb"><a href="<?php echo base_url()?>blog-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/post-thumb-1.jpg" alt=""></a></figure>
                                <div class="text"><a href="#">Proffesional solutions for your business.</a></div>
                                <ul class="post-meta clearfix">
                                	<li><a href="#"><i class="fa fa-calendar"></i>Dec 30, 2017 </a></li>  
                                </ul>
                            </article>

                            <article class="post">
                            	<figure class="post-thumb"><a href="<?php echo base_url()?>blog-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/post-thumb-2.jpg" alt=""></a></figure>
                                <div class="text"><a href="#">Money Market Rates Finding in 2017</a></div>
                                <ul class="post-meta clearfix">
                                	<li><a href="#"><i class="fa fa-calendar"></i>Dec 18, 2017 </a></li>  
                                </ul>
                            </article>
                            
                            <article class="post">
                            	<figure class="post-thumb"><a href="<?php echo base_url() ?>blog-single"><img src="<?php echo base_url() ?>public/assets/gallery1/images/resource/post-thumb-3.jpg" alt=""></a></figure>
                                <div class="text"><a href="#">Get a credit online with our team’s help</a></div>
                                <ul class="post-meta clearfix">
                                	<li><a href="#"><i class="fa fa-calendar"></i>Dec 12, 2017 </a></li>  
                                </ul>
                            </article>

                        </div>
						
                        
                        <!-- Archives -->
                        <div class="sidebar-widget categories">
                            <div class="sidebar-title"><h3>Archive</h3></div>
                            <ul class="list">
                                <li><a href="#">July 2016 &ensp; (12)</a></li>
                                <li><a href="#">Aug 2016 &ensp; (38)</a></li>
                                <li><a href="#">Sep 2016 &ensp; (23)</a></li>
                                <li><a href="#">Oct 2016 &ensp; (17)</a></li>
                                <li><a href="#">Nov 2016  &ensp; (03)</a></li>
                            </ul>

                        </div>

                        <!-- Popular Tags -->
                        <div class="sidebar-widget popular-tags">
                            <div class="sidebar-title"><h3>Tags</h3></div>
                                <li><a href="#">Active</a></li>
                                <li><a href="#">Building </a></li>
                                <li><a href="#">Ideas</a></li>
                                <li><a href="#">Energy</a></li>
                                <li><a href="#">Engines</a></li>
                                <li><a href="#">Chemical</a></li>
                                <li><a href="#">Industry</a></li>
                                <li><a href="#">Drilling</a></li>
                        </div>

                    </aside>


                </div>
                <!--Sidebar-->

            </div>
        </div>
    </div>
