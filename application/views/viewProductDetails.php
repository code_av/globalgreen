<!--Page Title-->
<style>
    @media (min-width: 1100px) {

        #img{
            width: 51%!important;
            margin-left: 205px;
    }
    }
</style>
<section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <h1>Products</h1>
        <div class="bread-crumb-outer">
            <ul class="bread-crumb clearfix">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="<?php echo base_url()?>services">Products</a></li>

            </ul>
        </div>
    </div>
</section>


<!--Sidebar Page-->
<div class="sidebar-page-container with-left-sidebar">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Content Side-->
            <div class="content-side pull-right col-lg-9 col-md-8 col-sm-12 ">
                <!--Service Details-->
                <section class="service-details">
                    <div class="row">
                        <?php
                        foreach($products as $pro)
                        {
                            ?>
                            <div class="col-md-12" id="img" style="text-align: center;">
                                <div class="inner hover-style1  hvr-float-shadow">
                                    <figure class="image-box hover-style1-img">
                                    <?php if(isset($pro['images'][0]['image'])){?>
                                    <img src="<?php echo base_url($pro['images'][0]['image']);?>" style="width:400px;height:400px;"/>
                                    <?php }else{?>
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/resource/no-image-available.jpg" style="width:400px;height:400px;"/>
                                    <?php }?>
                                        <div class="hover-style1-view">
                                            <a class="link-box" href="<?php echo base_url();?>add-to-cart/<?php echo $products[0]['product_id']; ?>" style="margin-left: -60px; height: 60px !important; width: 60px !important;">
                                                <span class="fa fa-shopping-cart" style="font-size: 25px; color: #7FB302;"></span></a>
                                            <a class="link-box" href="<?php echo base_url($pro['images'][0]['image']);?>" style="margin-left: 60px; height: 60px !important; width: 60px !important;">
                                                <span class="fa fa-search" style="font-size: 25px; color: #7FB302;"></span></a>
                                        </div>
                                </figure>
                                </div>
                                </div>
                            <?php
                        }
                        ?>
                    </div>


                    <!--Text Content-->
                    <div class="content">
                        <h3><?php echo $products[0]['name'] ?></h3>
                        <b>Product Code:</b><?php echo $products[0]['product_code'];?><br>
                        <p><b>Description:</b><?php echo ucfirst($products[0]['description']) ?></p>

                        <!--Faqs-->
                        <div class="faqs">
                            <!--Accordion Box-->
                            <ul class="accordion-box">

                                <!--Block-->
                                <li class="accordion block ">
                                    <div class="acc-btn "><div class="icon-outer"><span class="icon fa fa-angle-right"></span></div> Product Dimensions</div>
                                    <div class="acc-content ">
                                        <div class="content"><p>Size Available :  <?php echo $products[0]['size'] ?><br>
                                                Weight :	<?php echo $products[0]['weight'] ?><br>
                                                Material :  <?php echo $products[0]['material'] ?></p></div>
                                    </div>
                                </li>

                                <!--Block-->
                                <li class="accordion block active-block">
                                    <div class="acc-btn active"><div class="icon-outer"><span class="icon fa fa-angle-right"></span> </div> Photo Gallery</div>
                                    <div class="acc-content current">
                                        <div class="content">
                                            <!--photo gallery-->
                                            <div class="row">
                                                <?php
                                                foreach($products as $pro) {

                                                    foreach($pro['images'] as $img) {
                                                        ?>
                                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                            <a href="<?php echo base_url($img['image']); ?>"><img src="<?php echo base_url($img['image']); ?>" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>


                                                <div>
                                                </div>
                                            </div>
                                </li>
                            </ul><!--End Accordion Box-->
                        </div>
                    </div>
                </section>
            </div>
            <!--Content Side-->
