﻿<section class="page-title" style="background-image:url(<?php echo base_url() ?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <h1>Axilam</h1>
        <div class="bread-crumb-outer">
            <ul class="bread-crumb clearfix">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li class="active">Axilam</li>
            </ul>
        </div>
    </div>
</section>
    <section class="about-section">
        <div class="auto-container">
            <div class="row clearfix">
                <div class="content-column col-md-6 col-sm-6 ">
                    <div class="inner-box">
                        <div class="sec-title">
                            <h2>Welcome to <span>Axilam</span></h2>
                        </div>
                    </div>
                    <div class="text" style="text-align: justify">
                        <p>
                            Axi Lam laminates aims to provide consumers with trendiest and user-friendly innovations
                            that fulfil their various types of requirement, resulting in the gratitude and satisfaction
                            of client and manufacturers as well as workers. With a strong adherence to quality and precision,
                            Axi Lam laminates promises to serve the customers with the best and the most diverse portfolio
                            and satisfactory services.
                        </p>
                        <p>
                            A strong investment in infrastructure is the most essential aspect, when it comes to achieving new benchmarks
                            in excellence. A generous investment in manpower and capital machinery has
                            led to quality products, manufactured with latest technotlogy and fine quality controls. The entire
                            production process is designed to carry out smooth functioning right from manufacturing, quality
                            control, and packaging till distribution. By optimizing on both technical and intellectual resources.
                        </p>
                        <p>
                            mail us your requirement at <a href="mailto: orders@globalgreeneco.com">orders@globalgreeneco.com</a> , you can
                            also <a href="<?php echo base_url() ?>product-category/wpc-boards/24">explore our products line</a>
                        </p>
                        <p>
                            <a class="btn btn-success" href="#e-catalogue">download e-catalogue</a>
                        </p>
                    </div>
                </div>
                <div class="content-column col-md-6 col-sm-6 ">
                    <div class="row clearfix">
                        <div class="column">
                            <div class="footer-widget newsletter-widget">
                                <div class="newsletter-form">
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/ax1.png"> <img src="<?php echo base_url()?>public/assets/gallery1/images/home/ax1.png" class="img-responsive" style="width: 200px;height: 180px;"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/ax2.png"><img src="<?php echo base_url()?>public/assets/gallery1/images/home/ax2.png" class="img-responsive" style="width: 200px;height: 180px;"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/ax3.png"><img src="<?php echo base_url()?>public/assets/gallery1/images/home/ax3.png" class="img-responsive" style="width: 200px;height: 180px;"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/ax4.png"><img src="<?php echo base_url()?>public/assets/gallery1/images/home/ax4.png" class="img-responsive" style="width: 200px;height: 180px;"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/ax5.png"><img src="<?php echo base_url()?>public/assets/gallery1/images/home/ax5.png" class="img-responsive" style="width: 200px;height: 180px;"></a>
                                    </div>
                                    <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/images/home/ax6.png"><img src="<?php echo base_url()?>public/assets/gallery1/images/home/ax6.png" class="img-responsive" style="width: 200px;height: 180px;" ></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <a name="e-catalogue"></a>
    <section class="why-chooseus sec-padd2">
        <div class="container">
            <div class="sec-title centered">
                <h2>E- <span>Catalogue</span></h2>
            </div>
            <div class="row clearfix">
                <div class="column">
                    <div class="footer-widget newsletter-widget">
                        <div class="newsletter-form">
                        <div class="col-md-12" id="catalogue">
                            <div class="gallery_product col-lg-3 col-md-3 col-sm-3  filter hdpe">
                                <div class="inner hover-style1  hvr-float-shadow">
                                    <figure class="image-box hover-style1-img">
                                        <a href="<?php echo base_url()?>public/assets/docs/axilam-catalogue.pdf" target="_blank">
                                            <img src="<?php echo base_url()?>public/assets/docs/axilam.png" class="img-responsive" style="height: 250px; width: 300px; "/>
                                        </a>
                                        <div class="hover-style1-view">
                                            <a class="link-box" href="<?php echo base_url()?>public/assets/docs/axilam-catalogue.pdf" target="_blank"><span class="fa fa-link"></span></a>
                                        </div>
                                    </figure>
                                    <div class="lower-box" style="text-align: center">
                                        <h3 style="color:black;">Axilam Laminates</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
