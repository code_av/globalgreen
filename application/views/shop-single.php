﻿
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Product Details</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="<?php echo base_url()?>shop">Shop</a></li>
                    <li class="active">Product Details</li>
                </ul>
            </div>
        </div>
    </section>
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container shop-single-page">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side--> 
                <div class="content-side col-lg-9 col-md-12 col-sm-12 col-xs-12">
                    
                    <!--Shop Single-->
                    <div class="shop-single">
                        <div class="auto-container">

                            <!--Product Details Section-->
                            <section class="product-details">
                                <!--Basic Details-->
                                <div class="basic-details">
                                    <div class="row clearfix">
                                        <div class="image-column col-md-6 col-sm-6 col-xs-12">
                                            <figure class="image-box"><a href="<?php echo base_url()?>public/assets/gallery1/images/shop/large-image.jpg" class="lightbox-image"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/large-image.jpg" alt=""></a></figure>
                                        </div>
                                        <div class="info-column col-md-6 col-sm-6 col-xs-12">
                                            <div class="details-header">
                                                <h4>Helment</h4>
                                                <div class="item-price">$25.00</div>
                                            </div>
                                            <div class="text">Capitalise on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.User generated multiple touchpoints for offshoring.</div>
                                            <div class="stock">In Stock</div>
                                            <div class="other-options clearfix">
                                                <div class="item-quantity"><input class="quantity-spinner" type="text" value="2" name="quantity"></div>
                                                <button type="button" class="theme-btn btn-style-one add-to-cart">Add To Cart</button>
                                            </div>

                                            <!--Item Meta-->
                                            <ul class="item-meta">
                                                <li>Categories: <a href="#">Books</a> , <a href="#">Magazine</a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <!--Basic Details-->

                                <!--Product Info Tabs-->
                                <div class="product-info-tabs">

                                    <!--Product Tabs-->
                                    <div class="prod-tabs" id="product-tabs">

                                        <!--Tab Btns-->
                                        <div class="tab-btns clearfix">
                                            <a href="#prod-description" class="tab-btn active-btn">description</a>
                                            <a href="#prod-reviews" class="tab-btn">Reviews</a>
                                        </div>

                                        <!--Tabs Container-->
                                        <div class="tabs-container">

                                            <!--Tab / Active Tab-->
                                            <div class="tab active-tab" id="prod-description">
                                                <h3>Product Description</h3>
                                                <div class="content">
                                                    <p>Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.</p>
                                                    <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.Capitalise on low hanging fruit to identify a ballpark value added activity to beta test.</p>
                                                </div>
                                            </div>

                                            <!--Tab-->
                                            <div class="tab" id="prod-reviews">
                                                <h3>3 Reviews Found</h3>

                                                <!--Reviews Container-->
                                                <div class="reviews-container">

                                                    <!--Reviews-->
                                                    <article class="review-box clearfix">
                                                        <figure class="rev-thumb"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/author-1.png" alt=""></figure>
                                                        <div class="rev-content">
                                                            <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star-o"></span></div>
                                                            <div class="rev-info">Admin – April 03, 2016: </div>
                                                            <div class="rev-text"><p>Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis</p></div>
                                                        </div>
                                                    </article>

                                                    <article class="review-box clearfix">
                                                        <figure class="rev-thumb"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/author-3.jpg" alt=""></figure>
                                                        <div class="rev-content">
                                                            <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star-o"></span> <span class="fa fa-star-o"></span></div>
                                                            <div class="rev-info">Ahsan – April 01, 2016: </div>
                                                            <div class="rev-text"><p>Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis</p></div>
                                                        </div>
                                                    </article>

                                                    <article class="review-box clearfix">
                                                        <figure class="rev-thumb"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/author-4.jpg" alt=""></figure>
                                                        <div class="rev-content">
                                                            <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span></div>
                                                            <div class="rev-info">Sara – March 31, 2016: </div>
                                                            <div class="rev-text"><p>Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis</p></div>
                                                        </div>
                                                    </article>

                                                </div>

                                                <!--Add Review-->
                                                <div class="add-review">
                                                    <h3>Add a Review</h3>

                                                    <form method="post" action="<?php echo base_url()?>shop-single">
                                                        <div class="row clearfix">
                                                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                                                <label>Name *</label>
                                                                <input type="text" name="name" value="" placeholder="" required="">
                                                            </div>
                                                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                                                <label>Email *</label>
                                                                <input type="email" name="email" value="" placeholder="" required="">
                                                            </div>
                                                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                                                <label>Website *</label>
                                                                <input type="text" name="website" value="" placeholder="" required="">
                                                            </div>
                                                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                                                <label>Rating </label>
                                                                <div class="rating">
                                                                    <a href="#" class="rate-box" title="1 Out of 5"><span class="fa fa-star"></span></a>
                                                                    <a href="#" class="rate-box" title="2 Out of 5"><span class="fa fa-star"></span> <span class="fa fa-star"></span></a>
                                                                    <a href="#" class="rate-box" title="3 Out of 5"><span class="fa fa-star"></span> <span class="fa fa-star"> </span> <span class="fa fa-star"></span></a>
                                                                    <a href="#" class="rate-box" title="4 Out of 5"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span></a>
                                                                    <a href="#" class="rate-box" title="5 Out of 5"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span></a>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                <label>Your Review</label>
                                                                <textarea name="review-message"></textarea>
                                                            </div>
                                                            <div class="form-group text-right col-md-12 col-sm-12 col-xs-12">
                                                                <button type="button" class="theme-btn btn-style-one">Add Review</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>


                                            </div>

                                        </div>
                                    </div>

                                </div>

                                <!--Related Products-->
                                <div class="related-products">
                                    <div class="normal-title"><h3>Related Products</h3></div>

                                    <div class="row clearfix">

                                       <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <!--inner-box-->
                                            <div class="inner-box">
                                                <!--image-box-->
                                                <figure class="image-box">
                                                    <a href="<?php echo base_url()?>shop-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/1.png" alt=""></a>
                                                    <div class="overlay-box">
                                                        <a class="cart-btn" href="<?php echo base_url()?>shop-single">Add to cart</a>
                                                    </div>
                                                </figure>

                                                <!--lower-content-->
                                                <div class="lower-content">
                                                    <h3><a href="<?php echo base_url()?>shop-single">Paint Brush</a></h3>

                                                    <div class="rating">
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    </div>

                                                    <div class="price">$ 14.00</div>

                                                </div>

                                            </div>
                                        </div>
                            
                                        <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <!--inner-box-->
                                            <div class="inner-box">
                                                <!--image-box-->
                                                <figure class="image-box">
                                                    <a href="<?php echo base_url()?>shop-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/2.png" alt=""></a>
                                                    <div class="overlay-box">
                                                        <a class="cart-btn" href="<?php echo base_url()?>shop-single">Add to cart</a>
                                                    </div>
                                                    <div class="item-sale-tag">Sale</div>
                                                </figure>

                                                <!--lower-content-->
                                                <div class="lower-content">
                                                    <h3><a href="<?php echo base_url()?>shop-single">Steel Hammer</a></h3>

                                                    <div class="rating">
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    </div>

                                                    <div class="price">$ 29.00</div>

                                                </div>

                                            </div>
                                        </div>
                            
                                        <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <!--inner-box-->
                                            <div class="inner-box">
                                                <!--image-box-->
                                                <figure class="image-box">
                                                    <a href="<?php echo base_url()?>shop-single.html"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/3.png" alt=""></a>
                                                    <div class="overlay-box">
                                                        <a class="cart-btn" href="<?php echo base_url()?>shop-single.html">Add to cart</a>
                                                    </div>
                                                    <div class="item-sale-tag">Sale</div>
                                                </figure>

                                                <!--lower-content-->
                                                <div class="lower-content">
                                                    <h3><a href="<?php echo base_url()?>shop-single.html">Lawn Makers</a></h3>

                                                    <div class="rating">
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star-o"></span>
                                                    </div>

                                                    <div class="price">$ 35.00</div>

                                                </div>

                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>

                            </section>

                        </div>
                    </div>
                
                </div>
                <!--Content Side-->
                
                <!--Sidebar-->  
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <aside class="sidebar blog-sidebar">

                        <!-- Search Form -->
                        <div class="sidebar-widget search-box">
                            <div class="sidebar-title"><h3>Search</h3></div>
                            <form method="post" action="<?php echo base_url()?>blog">
                                <div class="form-group">
                                    <input type="search" name="search-field" value="" placeholder="Search here..">
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>
                        </div>
                        
                        <!-- Categories -->
                        <div class="sidebar-widget recent-articles wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="sidebar-title"><h3>PRODUCT CATEGORIES</h3></div>
                            <ul class="list">
                                <li><a href="#">Architecture Plans</a></li>
                                <li><a href="#">Construction Projects</a></li>
                                <li><a href="#">Paintings</a></li>
                                <li><a href="#">Electrical Works</a></li>
                                <li><a href="#">Plumbing Works</a></li>
                            </ul>
                        </div>
                        
                        <!-- Price Filter -->
                        <div class="sidebar-widget rangeslider-widget">
                            <div class="sidebar-title"><h3>FILTER BY PRICE</h3></div>
                                
                            <div class="range-slider-price" id="range-slider-price"></div>
                            
                            <br>
                            <div class="form-group">
                                <input type="text" class="val-box" id="min-value-rangeslider">
                                <input type="text" class="val-box" id="max-value-rangeslider">
                                <button type="button">FILTER</button>
                            </div>
                        </div>
                        
                        <!-- Best Sellers -->
                        <div class="sidebar-widget best-sellers">
                            <div class="sidebar-title"><h3>LATEST PRODUCTS</h3></div>
                            
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/thumb-1.jpg" alt=""></a></div>
                                <h4><a href="#">Paint Brush</a></h4>
                                <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span></div>
                                <div class="item-price">$ 35.00</div>
                            </div>
                            
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/thumb-2.jpg" alt=""></a></div>
                                <h4><a href="#">Steel Hammer</a></h4>
                                <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star-half-empty"></span></div>
                                <div class="item-price">$ 29.00</div>
                            </div>
                            
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/thumb-3.jpg" alt=""></a></div>
                                <h4><a href="#">Lawn Makers</a></h4>
                                <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star-o"></span></div>
                                <div class="item-price">$ 29.00</div>
                            </div>
                            
                        </div>

                    </aside>
                
                </div>
                <!--Sidebar-->  
                
            </div>
        </div>
    </div>
