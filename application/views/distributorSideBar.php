<!--Sidebar-->
<div class="sidebar-side pull-left col-lg-3 col-md-4 col-sm-12 col-xs-12">
    <aside class="sidebar">

        <div class="sidebar-widget services">
            <div>Distributors</div>
            <ul class="service-list">
                <?php
                foreach($distributor as $cat)
                {
                    $dist_name =$cat->distributor_name ;
                    // remove all the brackets
                    $dist_name = str_replace(array( '(',')' ), '', $dist_name);
                    // remove all the special charters
                    $dist_name = preg_replace('/[^A-Za-z0-9 \']/', '', $dist_name);
                    // remove all the spaces
                    $string =strtolower(preg_replace("/[\s]/", "-", $dist_name));
                    echo '<li><a href="'.base_url().'distributors-product/'.$string.'/'.$cat->distributor_id.'">'.$cat->distributor_name.'</a></li>';

                }
                ?>
            </ul>
        </div>
        <!-- Services -->

        <div class="sidebar-widget services">
            <div>Categories</div>
            <ul class="service-list">
                <?php
                foreach($category as $cat)
                {
                    $cat_name =$cat->category_name ;
                    // remove all the brackets
                    $cat_name = str_replace(array( '(',')' ), '', $cat_name);
                    // remove all the special charters
                    $cat_name = preg_replace('/[^A-Za-z0-9 \']/', '', $cat_name);
                    // remove all the spaces
                    $string =strtolower(preg_replace("/[\s]/", "-", $cat_name));
                    if($cat->status==1) {
                        echo '<li><a href="' . base_url() . 'product-category/' . $string . '/' . $cat->category_id . '">' . $cat->category_name . '</a></li>';
                    }
                }
                ?>
            </ul>
        </div>




        <!-- Downloads -->

    </aside>


</div>
<!--Sidebar-->

</div>
</div>
</div>
