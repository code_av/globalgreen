﻿ <div class="content" style="padding-top:0px !important;">
    <section class="main-slider" data-start-height="600" data-slide-overlay="yes">
        <div class="tp-banner-container" >
            <div class="tp-banner">
                <ul>
                    <?php
                    foreach($main_slider as $slider)
                    {
                    ?>
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/1.jpg" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="<?php echo base_url().$slider->slider_image;?>" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                    <div class="slide-overlay"></div>

                    <div class="tp-caption sft sfb tp-resizeme" data-x="left" data-hoffset="500" data-y="center" data-voffset="-70" data-speed="1500" data-start="0" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="text-left"><h2><?php echo $slider->slider_text;?></h2></div></div>
                    <div class="tp-caption sft sfb tp-resizeme" data-x="left" data-hoffset="500" data-y="center" data-voffset="40" data-speed="1500" data-start="500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="text text-left"></div></div>
                    </li>

                    <?php } ?>
                </ul>
            </div>
        </div>
    </section>
    <section class="welcome-section">
        <div class="auto-container">
            <div class="sec-title centered">
                <h2>Welcome to <span>Global Green Eco Technologies Pvt. Ltd.</span></h2>
                <div class="separator"></div>
                <div class="text" style="font-size: 18px; color: black; text-align: justify">
                    We at Global Green Eco Technologies Pvt. Ltd. specializes in design, installation and service
                    of precision-engineered, <strong>100% customised UPVC windows and doors, 100% customised WPC windows and
                        doors, extensive range of hardware fittings and high quality plywood mica</strong>. With our technical
                    capabilities, professionalism, and hard work we installed over 100k windows and doors in short period
                    of time that can withstand India's extreme climatic conditions. Today, we firmly established
                    ourselves as most trusted windows and doors dealer/retailer, with presence in more than 20
                    cities across the country. <a href="<?php echo base_url() ?>/about#certificates">View our certificates</a>
                    that we earned by hard work and love of our customers.
                </div>
            </div>
            <div class="row clearfix">
                <div class="services-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner hover-style1  hvr-float-shadow">
                        <figure class="image-box hover-style1-img">
                            <img src="<?php echo base_url() ?>public/assets/gallery1/images/resource/upvc-doors.jpg" alt="">
                            <div class="hover-style1-view">
                                <a class="link-box" href="<?php echo base_url()?>upvc-doors-windows"><span class="fa fa-link"></span></a>
                            </div>
                        </figure>
                        <div class="lower-box" style="padding-left: 10px;padding-right: 10px;">
                            <h3><a href="<?php echo base_url()?>upvc-doors-windows">UPVC Doors/Windows</a></h3>
                            <div class="text" style="text-align: justify">
                                UPVC doors and windows are long lasting and deliver the same look and
                                glaze for years without being affected even by worse physical factors...
                                <a href="<?php echo base_url()?>upvc-doors-windows">learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="services-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner hover-style1  hvr-float-shadow">
                        <figure class="image-box hover-style1-img">
                            <img src="<?php echo base_url()?>public/assets/gallery1/images/resource/wpc-boards.jpg" alt="">
                            <div class="hover-style1-view">
                                <a class="link-box" href="<?php echo base_url()?>wpc-chaukhat-boards">
                                    <span class="fa fa-link"></span></a>
                            </div>
                        </figure>
                        <div class="lower-box" style="padding-left: 10px;padding-right: 10px;">
                            <h3><a href="<?php echo base_url()?>wpc-chaukhat-boards">WPC Chaukhat/Boards</a></h3>
                            <div class="text" style="text-align: justify">
                                WPC is a highly durable and secure material used for construction purposes. It is a
                                blend of wood fibre/ wood flour and thermoplastics...
                                <a href="<?php echo base_url()?>wpc-chaukhat-boards">learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="services-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner hover-style1  hvr-float-shadow">
                        <figure class="image-box hover-style1-img">
                            <img src="<?php echo base_url() ?>public/assets/gallery1/images/resource/modular-kitchen.jpg" alt="">
                            <div class="hover-style1-view">
                                <a class="link-box" href="<?php echo base_url()?>modular-kitchen-overview"><span class="fa fa-link"></span></a>
                            </div>
                        </figure>
                        <div class="lower-box" style="padding-left: 10px;padding-right: 10px;">
                            <h3><a href="<?php echo base_url()?>modular-kitchen-overview">Modular Kitchen</a></h3>
                            <div class="text" style="text-align: justify">
                                Modular kitchen designs - Explore our gallery to find thousands of trendy kitchen
                                designs for every kitchen shape & theme along with instant estimates...
                                <a href="<?php echo base_url()?>modular-kitchen-overview">learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="services-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner hover-style1  hvr-float-shadow">
                        <figure class="image-box hover-style1-img">
                            <img src="<?php echo base_url()?>public/assets/gallery1/images/resource/hardware-fittings.jpg" alt="">
                            <div class="hover-style1-view">
                                <a class="link-box" href="<?php echo base_url()?>distributors-product/ozone/12"><span class="fa fa-link"></span></a>
                            </div>
                        </figure>
                        <div class="lower-box" style="padding-left: 10px;padding-right: 10px;">
                            <h3><a href="<?php echo base_url()?>distributors-product/ozone/12">Hardware Fittings</a></h3>
                            <div class="text" style="text-align: justify">We offer an extensive range of hardware fittings which is of
                                supreme quality.  All the range is made up of high quality raw material...
                                <a href="<?php echo base_url()?>distributors-product/ozone/12">learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
 </div>
     <div class="tow-column">
         <div class="auto-container">
             <div class="sec-title centered light">
                 <h2>THE WORLD IS MOVING TO UPVC & WPC, IT'S <span>TIME YOU SHOULD TOO...</span></h2>
             </div>
             <div class="row clearfix">
                 <div class="text" style="font-size: 18px; text-align: justify; padding: 15px;">
                     UPVC & WPC are today a popular choice of material, thanks to it's inherent characteristics of natural wood
                     aesthetics combined with the tenacity to endure harsh outdoor conditions. This results in a
                     lifespan unusual for natural wood, even with regular maintenance. Global Green Eco Technologies Pvt. Ltd.
                     is a pioneer in UPVC & WPC with a vision to lead the industry and continually set benchmarks with
                     uncompromising quality and unmatched product features. 'No compromises', is our ethos, and that's how
                     we offer superlative products to stay well ahead of competition. Global Green Eco Technologies Pvt. Ltd.
                     has grown based on a philosophy of product innovation and the ability to offer a range of profiles
                     that complement design development for multiple applications. We take great pride in helping you choose
                     genuine products, from the usual choices to create lasting beautiful indoor & outdoor spaces.
                 </div>
                 <br>
                 <div class="col-lg-12 col-md-12 ">
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>Durable and long lasting</h3>
                                 <div class="text" style="text-align: justify;">
                                     uPVC windows and doors are long lasting and deliver the same look and glaze for
                                     years without being affected even by worse physical factors.
                                 </div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>Perfect insulation</h3>
                                 <div class="text" style="text-align: justify;">
                                     Keeps the cold out and the warmth in during winter vice versa in summer. Safe on heating and cooling bills.
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-12 col-md-12 ">
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>Highly hygienic</h3>
                                 <div class="text" style="text-align: justify;">
                                     Keeps dust, polling, wind and bacteria out while closed
                                 </div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>Virtually no maintenance required</h3>
                                 <div class="text" style="text-align: justify;">
                                     No painting. No sanding. No warping
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-12 col-md-12 ">
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>Noise Insulator</h3>
                                 <div class="text" style="text-align: justify;">
                                     Blocks out the noise from outside and retains privacy by keeping indoor noise blocked
                                 </div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>Eco friendly</h3>
                                 <div class="text" style="text-align: justify;">
                                     The products are fully recyclable
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-12 col-md-12 ">
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>Noise Insulator</h3>
                                 <div class="text" style="text-align: justify;">
                                     Blocks out the noise from outside and retains privacy by keeping indoor noise blocked
                                 </div>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>Eco friendly</h3>
                                 <div class="text" style="text-align: justify;">
                                     The products are fully recyclable
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-12 col-md-12 ">
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>Various colour options</h3>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>Wooden lookalike with real feeling grain</h3>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-lg-12 col-md-12 ">
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>not effected by wind, dust particles, sun and other natural elements</h3>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-6 col-sm-6 ">
                         <div class="service-block">
                             <div class="inner-box">
                                 <div class="icon-box"><span class="fa fa-check" style="color: green"></span></div>
                                 <h3>Easy to clean, Easy to operate </h3>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 <div class="content" style="padding-top:0px !important;">
    <section class="welcome-section">
        <div class="auto-container">
            <div class="sec-title centered">
                <h2>Our<span>Brands</span></h2>
                <div class="separator"></div>
            </div>
            <div class="row clearfix">
                <div class="services-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner hover-style1  hvr-float-shadow">
                        <figure class="image-box hover-style1-img">
                            <img src="<?php echo base_url() ?>public/assets/gallery1/images/home/alstone_master.png" alt="">
                            <div class="hover-style1-view">
                                <a class="link-box" href="<?php echo base_url()?>alstone"><span class="fa fa-link"></span></a>
                            </div>
                        </figure>
                        <div class="lower-box" style="padding-left: 10px;padding-right: 10px;">
                            <h3><a href="<?php echo base_url()?>alstone">Alstone Doors & Chaukhat</a></h3>
                            <div class="text" style="text-align: justify">
                                Frame your door with authentic beauty and strength of ALSTONE CHAUKHAT, the revolutionary
                                door frame manufactured with wood polymer composite material. ALSTONE door frames are
                                durable, low maintenance & ready to use...<a href="<?php echo base_url()?>alstone">learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="services-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner hover-style1  hvr-float-shadow">
                        <figure class="image-box hover-style1-img">
                            <img src="<?php echo base_url()?>public/assets/gallery1/images/home/axilam_master.png" alt="">
                            <div class="hover-style1-view">
                                <a class="link-box" href="<?php echo base_url()?>axilam">
                                    <span class="fa fa-link"></span></a>
                            </div>
                        </figure>
                        <div class="lower-box" style="padding-left: 10px;padding-right: 10px;">
                            <h3><a href="<?php echo base_url()?>axilam">Axi Lam Laminates</a></h3>
                            <div class="text" style="text-align: justify">
                                Axi Lam's laminates sheets are the execellent products created from 100% imported design
                                paper. With the intelligent use of A' Grade Kraft paper with best quality resin, that
                                has superior back surface sanding for best adhesion, Axi Lam laminates offers...
                                <a href="<?php echo base_url()?>axilam">learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="services-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner hover-style1  hvr-float-shadow">
                        <figure class="image-box hover-style1-img">
                            <img src="<?php echo base_url() ?>public/assets/gallery1/images/home/ozone_master.png" alt="">
                            <div class="hover-style1-view">
                                <a class="link-box" href="<?php echo base_url()?>ozone"><span class="fa fa-link"></span></a>
                            </div>
                        </figure>
                        <div class="lower-box" style="padding-left: 10px;padding-right: 10px;">
                            <h3><a href="<?php echo base_url()?>ozone">Ozone Fittings</a></h3>
                            <div class="text" style="text-align: justify">
                                Ozone provide international standard solutions in Stainless Steel. Ozone has extensive
                                range of hardware fittings, which are easy to install, smooth finishing, premium design,
                                smooth movement, durable, long lasting. Whatever you need in hardware...
                                <a href="<?php echo base_url()?>ozone">learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="why-chooseus sec-padd2">
         <div class="container">
             <div class="sec-title centered">
                 <h2>why <span>choose Us</span></h2>
             </div>
             <div class="row">
                 <div class="item col-lg-4 col-md-6 col-sm-12 col-xs-12">
                     <div class="inner-box wow fadeIn  animated animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeIn;">
                         <div class="icon_box">
                             <span class="flaticon-tools"></span>
                         </div>
                         <h4>SUPERIOR QUALITY</h4>
                         <div class="text"><p>Innovative designed range that enhances the appeal and style of
                                 indoors and outdoors, with best quality products. </p></div>
                     </div>
                 </div>
                 <div class="item col-lg-4 col-md-6 col-sm-12 col-xs-12">
                     <div class="inner-box wow fadeIn  animated animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeIn;">
                         <div class="icon_box">
                             <span class="flaticon-arrows"></span>
                         </div>
                         <h4>COMPETITIVE PRICE</h4>
                         <div class="text">We set a price of our products according to your pocket with a best quality.  </p></div>
                     </div>
                 </div>
                 <div class="item col-lg-4 col-md-6 col-sm-12 col-xs-12">
                     <div class="inner-box wow fadeIn  animated animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeIn;">
                         <div class="icon_box">
                             <span class="flaticon-stopwatch"></span>
                         </div>
                         <h4>TIME DELIVERY</h4>
                         <div class="text"><p>We know the value of time , so we deliver the products within a commited time</p></div>
                     </div>
                 </div>
             </div>
         </div>
     </section>
</div>
<section class="testimonial">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2>Our <span>Testimonials</span></h2>
        </div>
        <div class="row clearfix">
            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="testimonial-item">
                    <div class="content">
                        <span class="fa fa-quote-left"></span>
                        <p>the quality of the product is awesome . <br>look of the products are <br>very attractive.</p>
                        <ul class="rating">
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                    </div>
                    <div class="author">
                        <ul class="list-inline">
                            <li>
                                <img src="<?php echo base_url()?>public/assets/gallery1/images/resource/user1.jpg" alt="">
                            </li>
                            <li>
                                <h5>abhi banerjee</h5>
                                <p>allahabad</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </article>
            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="testimonial-item">
                    <div class="content">
                        <span class="fa fa-quote-left"></span>
                        <p>The  best seller of wpc products,<br>and the price of product is<br>under budget</p>
                        <ul class="rating">
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                    </div>
                    <div class="author">
                        <ul class="list-inline">
                            <li>
                                <img src="<?php echo base_url()?>public/assets/gallery1/images/resource/user2.jpg" alt="">
                            </li>
                            <li>
                                <h5>jain malik</h5>
                                <p>kanpur</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </article>
            <article class="col-md-4 col-sm-6 col-xs-12">
                <div class="testimonial-item">
                    <div class="content">
                        <span class="fa fa-quote-left"></span>
                        <p>high quality products with<br>less price, these products <br>are very awesome</p>
                        <ul class="rating">
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                    </div>
                    <div class="author">
                        <ul class="list-inline">
                            <li>
                                <img src="<?php echo base_url()?>public/assets/gallery1/images/resource/user3.jpg" alt="">
                            </li>
                            <li>
                                <h5>Manish Malhotra</h5>
                                <p>Varanasi</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </article>
        </div>
    </div>
</section>
<section class="parallax" style="background-color:grey;">
    <div class="auto-container">
        <h2>Our goal is to have customer service that is not  <br> just the best, but its Legendary</h2>
    </div>
</section>
<section class="fluid-section-one" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/parallax2.jpg);">
    <div class="outer-container clearfix">
        <div class="left-column">
            <div class="clearfix">
                <div class="inner">
                    <div class="title"><h3>Request A Call Back</h3></div>
                    <div class="text">
                        <p>We expertise in design and installation of UPVC, WPC construction projects including
                            Designer Kitchens, steps, UPVC doors and windows ,WPC products, hardware fittings,
                            WPC doors/Windows.</p>
                        <p><span>Phone &amp; Email:</span> For any information contact with us through our <a href="mailto:orders@globalgreeneco.com"> Email:orders@globalgreeneco.com</a> and you can also contact with directe by call us in this number <span><a href="tel:9415029036">9415029036</a></span></p>
                        <p><span>Office Hours:</span> We are always open except Sunday from <span>10:00am</span> to <span> 7:00pm</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="right-column">
            <div class="clearfix">
                <div class="inner">
                    <div class="title"><h3>Contact Us</h3></div>
                    <div class="text">Tell us about your requirements.</div>
                    <div class="default-form quote-form-two">
                        <form method="post" action="<?php echo base_url()?>mail">
                            <div class="row clearfix">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="fname" value="" placeholder="Name " required="">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" name="email" value="" placeholder="Email" required="">
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <textarea name="message" placeholder="Message" required=""></textarea>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="theme-btn btn-style-one">Send Message </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
