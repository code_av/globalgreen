﻿<style>
    .fancybox-overlay
    {
        background-color:#000c;
    }
    .fancybox-overlay-fixed
    {
        background-color:#000c;
    }
    @media (min-width:1100px) {
        .fancybox-inner {
            width: 570px !important;
            height: 470px !important;
        }

        .fancybox-skin {
            width: 570px !important;
        }

        .fancybox-wrap {
            left: 374px !important;
        }
    }
</style>
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url() ?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Projects</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url() ?>">Home</a></li>
                    <li class="active">Projects</li>
                </ul>
            </div>
        </div>
    </section>
    
    
    <!--Gallery Section-->
    <section class="gallery-section">
    	<div class="auto-container">
            <div class="row-15 grid clearfix masonary-layout filter-layout">
                <!--Default Portfolio Item-->
                <?php
                foreach($projects as $project)
                {
                ?>
                <div class="default-portfolio-item Growth col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <a href="<?php echo base_url().$project->project_image ;?>" class="lightbox-image option-btn" title="<?php echo $project->project_title; ?>" data-fancybox-group="example-gallery">
                            <figure class="effect-bubba">
                                <img src="<?php echo base_url().$project->project_image ;?>" alt="image">
                                <figcaption>
                                    <div class="content">
                                        <h2><?php echo $project->project_title ;?></h2>
                                        <p><?php echo $project->project_subtitle ;?></p>
                                    </div>
                                </figcaption>
                            </figure>
                        </a>
                    </div>
                </div>
                <?php }?>
                </div>
            
            <br>
            
        </div>
    </section>
