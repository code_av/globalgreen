﻿
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Shop</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li class="active">Shop</li>
                </ul>
            </div>
        </div>
    </section>
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container right-side-bar">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side--> 
                <div class="content-side col-lg-9 col-md-12 col-sm-12 col-xs-12">
                    
                    <!--News Section-->
                    <section class="products-section">
                        
                        <div class="shop-upper-box">
                            <div class="clearfix">
                                <div class="pull-left items-label">Showing 1-9 of 40 items</div>
                                <div class="pull-right sort-by">
                                    <select class="sort-select">
                                        <option>Price: Lowest First
                                        <option>Price: Highest First
                                        <option>Ascending
                                        <option>Descending
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            
                            <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <!--inner-box-->
                                <div class="inner-box">
                                    <!--image-box-->
                                    <figure class="image-box">
                                        <a href="<?php echo base_url()?>shop-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/1.png" alt=""></a>
                                        <div class="overlay-box">
                                            <a class="cart-btn" href="<?php echo base_url()?>shop-single">Add to cart</a>
                                        </div>
                                    </figure>
                                    
                                    <!--lower-content--> 
                                    <div class="lower-content">
                                        <h3><a href="shop-single.html">Paint Brush</a></h3>
                                        
                                        <div class="rating">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                        
                                        <div class="price">$ 14.00</div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <!--inner-box-->
                                <div class="inner-box">
                                    <!--image-box-->
                                    <figure class="image-box">
                                        <a href="<?php echo base_url()?>shop-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/2.png" alt=""></a>
                                        <div class="overlay-box">
                                            <a class="cart-btn" href="<?php echo base_url()?>shop-single">Add to cart</a>
                                        </div>
                                        <div class="item-sale-tag">Sale</div>
                                    </figure>
                                    
                                    <!--lower-content-->
                                    <div class="lower-content">
                                        <h3><a href="<?php echo base_url()?>shop-single">Steel Hammer</a></h3>
                                        
                                        <div class="rating">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                        
                                        <div class="price">$ 29.00</div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <!--inner-box-->
                                <div class="inner-box">
                                    <!--image-box-->
                                    <figure class="image-box">
                                        <a href="<?php echo base_url()?>shop-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/3.png" alt=""></a>
                                        <div class="overlay-box">
                                            <a class="cart-btn" href="<?php echo base_url()?>shop-single">Add to cart</a>
                                        </div>
                                        <div class="item-sale-tag">Sale</div>
                                    </figure>
                                    
                                    <!--lower-content-->
                                    <div class="lower-content">
                                        <h3><a href="<?php echo base_url()?>shop-single">Lawn Makers</a></h3>
                                        
                                        <div class="rating">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                        
                                        <div class="price">$ 35.00</div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <!--inner-box-->
                                <div class="inner-box">
                                    <!--image-box-->
                                    <figure class="image-box">
                                        <a href="<?php echo base_url()?>shop-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/4.png" alt=""></a>
                                        <div class="overlay-box">
                                            <a class="cart-btn" href="<?php echo base_url()?>shop-single">Add to cart</a>
                                        </div>
                                        <div class="item-sale-tag">Sale</div>
                                    </figure>
                                    
                                    <!--lower-content-->
                                    <div class="lower-content">
                                        <h3><a href="<?php echo base_url()?>shop-single">Helment</a></h3>
                                        
                                        <div class="rating">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                        
                                        <div class="price">$ 35.00</div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <!--inner-box-->
                                <div class="inner-box">
                                    <!--image-box-->
                                    <figure class="image-box">
                                        <a href="<?php echo base_url()?>shop-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/5.png" alt=""></a>
                                        <div class="overlay-box">
                                            <a class="cart-btn" href="<?php echo base_url()?>shop-single">Add to cart</a>
                                        </div>
                                    </figure>
                                    
                                    <!--lower-content-->
                                    <div class="lower-content">
                                        <h3><a href="<?php echo base_url()?>shop-single">Lawn Movers</a></h3>
                                        
                                        <div class="rating">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                        
                                        <div class="price">$ 14.00</div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <!--inner-box-->
                                <div class="inner-box">
                                    <!--image-box-->
                                    <figure class="image-box">
                                        <a href="<?php echo base_url()?>shop-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/6.png" alt=""></a>
                                        <div class="overlay-box">
                                            <a class="cart-btn" href="<?php echo base_url()?>shop-single">Add to cart</a>
                                        </div>
                                        <div class="item-sale-tag">Sale</div>
                                    </figure>
                                    
                                    <!--lower-content-->
                                    <div class="lower-content">
                                        <h3><a href="<?php echo base_url()?>shop-single">Drainage Cleaner</a></h3>
                                        
                                        <div class="rating">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                        
                                        <div class="price">$ 29.00</div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <!--inner-box-->
                                <div class="inner-box">
                                    <!--image-box-->
                                    <figure class="image-box">
                                        <a href="<?php echo base_url()?>shop-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/7.png" alt=""></a>
                                        <div class="overlay-box">
                                            <a class="cart-btn" href="<?php echo base_url()?>shop-single">Add to cart</a>
                                        </div>
                                    </figure>
                                    
                                    <!--lower-content-->
                                    <div class="lower-content">
                                        <h3><a href="<?php echo base_url()?>shop-single">Hand Drill Machine</a></h3>
                                        
                                        <div class="rating">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                        
                                        <div class="price">$ 29.00</div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <!--inner-box-->
                                <div class="inner-box">
                                    <!--image-box-->
                                    <figure class="image-box">
                                        <a href="<?php echo base_url()?>public/assets/gallery1/shop-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/8.png" alt=""></a>
                                        <div class="overlay-box">
                                            <a class="cart-btn" href="<?php echo base_url()?>public/assets/gallery1/shop-single">Add to cart</a>
                                        </div>
                                        <div class="item-sale-tag">Sale</div>
                                    </figure>
                                    
                                    <!--lower-content-->
                                    <div class="lower-content">
                                        <h3><a href="<?php echo base_url()?>public/assets/gallery1/shop-single">Drainage Cleaner</a></h3>
                                        
                                        <div class="rating">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                        
                                        <div class="price">$ 14.00</div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="default-shop-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <!--inner-box-->
                                <div class="inner-box">
                                    <!--image-box-->
                                    <figure class="image-box">
                                        <a href="<?php echo base_url()?>shop-single"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/9.png" alt=""></a>
                                        <div class="overlay-box">
                                            <a class="cart-btn" href="<?php echo base_url()?>shop-single">Add to cart</a>
                                        </div>
                                    </figure>
                                    
                                    <!--lower-content-->
                                    <div class="lower-content">
                                        <h3><a href="<?php echo base_url()?>shop-single">Speed Bench Drill Press</a></h3>
                                        
                                        <div class="rating">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                        
                                        <div class="price">$ 35.00</div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                        </div>
                        
                        <!--Styled Pagination-->
                        <div class="styled-pagination">
                            <ul>
                                <li><a href="#">1</a></li>
                                <li><a href="#" class="active">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#"><span class="fa fa-angle-double-right"></span></a></li>
                            </ul>
                        </div>
                        
                    </section>
                
                    
                </div>
                <!--Content Side-->
                
                <!--Sidebar-->  
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <aside class="sidebar blog-sidebar">

                        <!-- Search Form -->
                        <div class="sidebar-widget search-box">
                            <div class="sidebar-title"><h3>Search</h3></div>
                            <form method="post" action="<?php echo base_url()?>blog">
                                <div class="form-group">
                                    <input type="search" name="search-field" value="" placeholder="Search here..">
                                    <button type="submit"><span class="icon fa fa-search"></span></button>
                                </div>
                            </form>
                        </div>
                        
                        <!-- Categories -->
                        <div class="sidebar-widget recent-articles wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="sidebar-title"><h3>PRODUCT CATEGORIES</h3></div>
                            <ul class="list">
                                <li><a href="#">Architecture Plans</a></li>
                                <li><a href="#">Construction Projects</a></li>
                                <li><a href="#">Paintings</a></li>
                                <li><a href="#">Electrical Works</a></li>
                                <li><a href="#">Plumbing Works</a></li>
                            </ul>
                        </div>
                        
                        <!-- Price Filter -->
                        <div class="sidebar-widget rangeslider-widget">
                            <div class="sidebar-title"><h3>FILTER BY PRICE</h3></div>
                                
                            <div class="range-slider-price" id="range-slider-price"></div>
                            
                            <br>
                            <div class="form-group">
                                <input type="text" class="val-box" id="min-value-rangeslider">
                                <input type="text" class="val-box" id="max-value-rangeslider">
                                <button type="button">FILTER</button>
                            </div>
                        </div>
                        
                        <!-- Best Sellers -->
                        <div class="sidebar-widget best-sellers">
                            <div class="sidebar-title"><h3>LATEST PRODUCTS</h3></div>
                            
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/thumb-1.jpg" alt=""></a></div>
                                <h4><a href="#">Paint Brush</a></h4>
                                <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span></div>
                                <div class="item-price">$ 35.00</div>
                            </div>
                            
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/thumb-2.jpg" alt=""></a></div>
                                <h4><a href="#">Steel Hammer</a></h4>
                                <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star-half-empty"></span></div>
                                <div class="item-price">$ 29.00</div>
                            </div>
                            
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="<?php echo base_url()?>public/assets/gallery1/images/shop/thumb-3.jpg" alt=""></a></div>
                                <h4><a href="#">Lawn Makers</a></h4>
                                <div class="rating"><span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star"></span> <span class="fa fa-star-o"></span></div>
                                <div class="item-price">$ 29.00</div>
                            </div>
                            
                        </div>

                    </aside>
                
                </div>
                <!--Sidebar-->  
                
            </div>
        </div>
    </div>
