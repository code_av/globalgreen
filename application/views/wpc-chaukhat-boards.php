<section class="page-title" style="background-image:url(<?php echo base_url() ?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <h1>WPC Chaukhat & Boards</h1>
    </div>
</section>
<section class="welcome-section">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2>WPC CHAUKHAT <span>FRAME IT</span>, <span>FIX IT</span>, <span>FORGET IT</span> FOR LIFETIME</h2>
            <div class="separator"></div>
            <div class="text" style="font-size: 18px; color: black; text-align: center">
                <i>Na deemak ka darr, na silan ki fikar, WPC chaukhat chale jeevan bhar</i>
            </div>
            <div class="text">
                <p>
                    Frame your door with the authentic beauty and strength of ALSTONE CHAUKHAT, the revolutionary
                    door frame manufactures with wood polymer composite material. ALSTONE door frames are durable,
                    low maintenance & ready to use. Unlike other wooden or other kind of door frame it is 100% water
                    and termite proof panel and it is perfect choice for outdoor as well as indoor door framing.
                </p>
                <p>
                    ALSTONE CHAUKHAT designs blend beautifully with your home interior and exterior finishes. The
                    exclusive innovations provide exceptional protection from drafts and leaks making your door stay
                     looking new for years.
                </p>
                <p>
                    <strong>ADVANTAGES:</strong>
                </p>
                <ul>
                    <li>Ready to paint in any color you want</li>
                    <li>101% water proof</li>
                    <li>101% termite proof</li>
                    <li>Fire retardant</li>
                    <li>Helps keep door operating and looking great for years</li>
                    <li>0% shrinkage</li>
                    <li>100% solid</li>
                </ul>
                <br>
                <p>
                    order us now at <a href="mailto: orders@globalgreeneco.com">orders@globalgreeneco.com</a> you can
                    also <a href="<?php echo base_url() ?>product-category/wpc-boards/24">explore our products line</a>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="about-section">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="content-column col-md-12 col-sm-12">
                <div class="row clearfix">
                    <div class="column">
                        <div class="footer-widget newsletter-widget">
                            <div class="newsletter-form">
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter hdpe">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-1.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-1.jpg" class="img-responsive" ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-2.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-2.jpg" class="img-responsive" >
                                    </a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter hdpe">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-3.png">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-3.png" class="img-responsive" ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-4.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-4.jpg" class="img-responsive" ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter hdpe">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-5.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-5.jpg" class="img-responsive" ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-6.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/wpc-6.jpg" class="img-responsive"  ></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>