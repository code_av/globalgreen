<section class="page-title" style="background-image:url(<?php echo base_url() ?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <h1>Modular Kitchen</h1>
    </div>
</section>
<section class="welcome-section">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2>Design Your <span>Dream Kitchen</span> With <span>Experts</span></h2>
            <div class="separator"></div>
            <div class="text">
                <p>
                    The kitchen is debatably the most important room in our homes. It is the place where the magic
                    happens; where we prepare our meals! Yum! Furthermore, kitchens are a place where we gather with
                    our friends and family to sit around the dining table with a coffee or a glass of wine and chat.
                    Out of all the rooms in a house, the kitchen is usually the most frequented.
                </p>
                <p>
                    We believe that cooking is an art and it deserves to be done well. Our goal
                    is to elevate cooking to an art form, by fusing functionality and aesthetics, while
                    still emphasizing on ergonomics and utility. Equipped with high quality raw materials
                    and German hardware. We sport a very international sensibility and are available in
                    multiple colours and finishes.
                </p>
                <p>
                    order us now at <a href="mailto: orders@globalgreeneco.com">orders@globalgreeneco.com</a> you can
                    also <a href="<?php echo base_url() ?>product-category/modular-kitchens/25">explore our products line</a>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="about-section">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="content-column col-md-12 col-sm-12">
                <div class="row clearfix">
                    <div class="column">
                        <div class="footer-widget newsletter-widget">
                            <div class="newsletter-form">
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter hdpe">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-1.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-1.jpg" class="img-responsive" ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-2.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-2.jpg" class="img-responsive" >
                                    </a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter hdpe">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-3.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-3.jpg" class="img-responsive" ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-4.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-4.jpg" class="img-responsive" ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter hdpe">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-5.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-5.jpg" class="img-responsive" ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-6.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-6.jpg" class="img-responsive"  ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-7.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-7.jpg" class="img-responsive"  ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-9.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-9.jpg" class="img-responsive"  ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-10.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-10.jpg" class="img-responsive"  ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-11.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-11.jpg" class="img-responsive"  ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-12.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-12.jpg" class="img-responsive"  ></a>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-8.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/kitchen-8.jpg" class="img-responsive"  ></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>