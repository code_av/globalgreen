﻿
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Checkout</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="<?php echo base_url()?>shop">Shop</a></li>
                    <li class="active">Checkout</li>
                </ul>
            </div>
        </div>
    </section>
    

    <!--Checkout Page-->
    <div class="checkout-page">
        <div class="auto-container">
                
            <!--Default Links-->
            <ul class="default-links">
                <li>Exisitng Customer? <a href="#">Click here to login</a></li>
                <li>Have a coupon? <a href="#">Click here to enter your code</a></li>
            </ul>
                
            <!--Billing Details-->
            <div class="billing-details">
                <div class="shop-form">
                    <form method="post" action="checkout.html">
                        <div class="row clearfix">
                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                
                                <div class="default-title"><h2>Billing Details</h2></div>
                        
                                <div class="row clearfix">
                                    
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">First name <sup>*</sup></div>
                                        <input type="text" name="field-name" value="" placeholder="">
                                    </div>
                                    
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">Last name <sup>*</sup></div>
                                        <input type="text" name="field-name" value="" placeholder="">
                                    </div>
                                    
                                    <!--Form Group-->
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <div class="field-label">Company name </div>
                                        <input type="text" name="field-name" value="" placeholder="">
                                    </div>
                                    
                                    <!--Form Group-->
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <div class="field-label">Address </div>
                                        <input type="email" name="field-name" value="" placeholder="">
                                    </div>
                                    
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">Town / City <sup>*</sup></div>
                                        <input type="text" name="field-name" value="" placeholder="">
                                    </div>
                                    
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">Postcode / Zip <sup>*</sup></div>
                                        <input type="text" name="field-name" value="" placeholder="">
                                    </div>
                                    
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">Email <sup>*</sup></div>
                                        <input type="email" name="field-name" value="" placeholder="">
                                    </div>
                                    
                                    <!--Form Group-->
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <div class="field-label">Phone <sup>*</sup></div>
                                        <input type="text" name="field-name" value="" placeholder="">
                                    </div>
                                    
                                    <!--Form Group-->
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <div class="field-label">Country <sup>*</sup> </div>
                                        <select name="country">
                                            <option>United Kingdom (UK)
                                            <option>Pakistan
                                            <option>USA
                                            <option>CANADA
                                            <option>INDIA
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="check-box"><input type="checkbox" name="shipping-option" id="account-option"> &ensp; <label for="account-option">Creat an account?</label></div>
                                    </div>
                                    
                                </div>
                            </div>
                            
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <div class="default-title"><h2>Additional Information</h2></div>
                                
                                <div class="form-group">
                                    <div class="field-label">Order note </div>
                                    <textarea placeholder="Notes about your order, e.g. special notes for delivery"></textarea>
                                </div>
                            </div>
                        </div>                             
                    </form>
                    
                </div>
                
            </div><!--End Billing Details-->
            
            
            <div class="default-title"><h2>Your Order</h2></div>
            
            <!--Cart Outer-->
            <div class="cart-outer">
                <div class="table-outer">
                    <table class="cart-table">
                        <thead class="cart-header">
                            <tr>
                                <th><span class="fa fa-trash-o"></span></th>
                                <th class="prod-column">Product</th>
                                <th class="price">Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td class="remove"><a href="#" class="remove-btn"><span class="fa fa-remove"></span></a><figure class="prod-thumb"><a href="#"><img src="images/shop/product-10.jpg" alt=""></a></figure></td>
                                <td class="prod-column">
                                    <div class="column-box">
                                        <!--<figure class="prod-thumb"><a href="#"><img src="images/resource/products/product-10.jpg" alt=""></a></figure>-->
                                        <h4 class="prod-title">Chair</h4>
                                    </div>
                                </td>
                                <td class="price">$15.00</td>
                                <td class="qty"><input class="quantity-spinner" type="text" value="2" name="quantity"></td>
                                <td class="sub-total">$25.00</td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                
                <div class="cart-options clearfix">
                    <div class="pull-left">
                        <div class="apply-coupon clearfix">
                            <div class="form-group clearfix">
                                <input type="text" name="coupon-code" value="">
                            </div>
                            <div class="form-group clearfix">
                                <button type="button" class="theme-btn btn-style-one">Apply Coupon</button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="pull-right">
                        <button type="button" class="theme-btn btn-style-one">PROCEED TO CHECKOUT</button>
                        <button type="button" class="theme-btn btn-style-five">UPDATE CART</button>
                    </div>
                    
                </div>
                
                
            </div>
            
            <!--Place Order-->
            <div class="place-order">
                <div class="default-title"><h2>Payment Method</h2></div>
                
                
                <!--Payment Options-->
                <div class="payment-options">
                    <ul>
                        <li>
                            <div class="radio-option">
                                <input type="radio" name="payment-group" id="payment-1" checked="">
                                <label for="payment-1"><strong>Cheque Payment</strong><span class="small-text">Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</span></label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-option">
                                <input type="radio" name="payment-group" id="payment-3">
                                <label for="payment-3"><strong>Paypal</strong></label>
                            </div>
                        </li>
                        <li>
                            <div class="radio-option">
                                <input type="radio" name="payment-group" id="payment-2">
                                <label for="payment-2"><strong>Direct Bank Transfer</strong></label>
                            </div>
                        </li>
                    </ul>
                </div>
                
                <button type="button" class="theme-btn btn-style-one place-order">Place Order</button>
                
            </div>
            <!--End Place Order-->
            
        </div>
    </div>
