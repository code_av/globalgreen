<!--Sidebar-->
<div class="sidebar-side pull-left col-lg-3 col-md-4 col-sm-12 col-xs-12">
    <aside class="sidebar">


        <!-- Services -->

        <div class="sidebar-widget services">
            <div>Sub Categories</div>
            <ul class="service-list">
                <?php
                foreach($sub_category as $cat)
                {
                    if($cat->sub_category_status==1) {
                        $sub_cat_name =$cat->sub_category_name ;
                        // remove all the brackets
                        $sub_cat_name = str_replace(array( '(', ')' ), '', $sub_cat_name);
                        // remove all the special charters
                        $sub_cat_name = preg_replace('/[^A-Za-z0-9 \']/', '', $sub_cat_name);
                        // remove all the spaces
                        $string =strtolower(preg_replace("/[\s]/", "-", $sub_cat_name));

                        echo '<li><a href="' . base_url() . 'product-subcategory/' . $string .'/'. $cat->sub_category_id . '/' . $cat->category_id . '">' . $cat->sub_category_name . '</a></li>';
                    }
                }
                ?>
            </ul>
        </div>

        <div class="sidebar-widget services">
            <div>Distributors</div>
            <ul class="service-list">
                <?php
                foreach($distributor as $cat)
                {
                    $dist_name =$cat->distributor_name ;
                    // remove all the brackets
                    $dist_name = str_replace(array( '(', ')' ), '', $dist_name);
                    // remove all the special charters
                    $dist_name = preg_replace('/[^A-Za-z0-9 \']/', '', $dist_name);
                    // remove all the spaces
                    $string =strtolower(preg_replace("/[\s]/", "-", $dist_name));

                    echo '<li><a href="'.base_url().'distributors-product/'.$string.'/'.$cat->distributor_id.'">'.$cat->distributor_name.'</a></li>';

                }
                ?>
            </ul>
        </div>


        <!-- Downloads -->

    </aside>


</div>
<!--Sidebar-->

</div>
</div>
</div>
