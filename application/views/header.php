<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 , user-scalable=no">
    <title>Global Green Eco Technologies: UPVC doors, WPC doors, Hardware fittings, Plywood & mica</title>
    <meta name="author" content="global green eco technology pvt ltd">
    <meta name="subject" content="business, upvc door dealer">
    <meta name="Description" content="we are top dealer / retailer in premium quality upvc doors, wpc doors, hardware fittings, plywood & mica.">
    <meta name="Keywords" content="upvc door, wpc door, upvc window, wpc window ,hardware fittings, plywood and mica, upvc door in allahabad, wpc door in allahabad, hardware fittings in allahabad, plywood in allahabad, mica in allahabad, dealer in upvc, dealer in wpc, retailer in upvc, retailer in wpc, dealer in hardware fittings, retailer in hardware fittings">
    <meta name="Geography" content="1, Lyall Road, Civil Lines , Allahabad(U.P.), 211003">
    <meta name="Language" content="English">
    <meta name="Copyright" content="global green eco technology pvt ltd">
    <meta name="Designer" content="Advikbot">
    <meta name="Publisher" content="Advikbot">
    <meta name="distribution" content="Local">
    <meta name="Robots" content="INDEX,FOLLOW">
    <meta name="zipcode" content="211003">
    <meta name="city" content="Allahabad">
    <meta name="country" content="India">
    <meta name="theme-color" content="#202020">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <style>
        span.deleteicon {
            position: relative;
        }
        span.deleteicon span {
            position: absolute;
            display: block;
            top: 5px;
            right: 0px;
            width: 16px;
            height: 16px;
            background: url('http://cdn.sstatic.net/stackoverflow/img/sprites.png?v=4') 0 -690px;
            cursor: pointer;
        }
        span.deleteicon input {
            padding-right: 16px;
            box-sizing: border-box;
        }
    </style>
    <link href="<?php echo base_url()?>public/assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url()?>public/assets/css/revolution-slider.css" rel="stylesheet">
    <link href="<?php echo base_url()?>public/assets/css/slider-setting.css" rel="stylesheet">
    <link href="<?php echo base_url()?>public/assets/css/style.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" id="jssDefault" href="<?php echo base_url()?>public/assets/css/theme-2.css">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="<?php echo base_url()?>public/assets/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--<script src="<?php /*echo base_url()*/?>public/assets/js/respond.js"></script>-->
</head>
<body class="home layout_changer">
<div class="page-wrapper body_wrapper">
    <!--<div class="preloader"></div>-->
    <header class="main-header header-style-three">
        <div class="header-top-one ">
            <div class="auto-container">
                <div class="clearfix top-bar-bg">
                    <div class="top-left top-links">
                        <ul class="clearfix">
                            <li><a href="tel:+91941502936"><i class="fa fa-phone"></i>(+91) 9415029036</a></li>
                            <li><a href="mailto:orders@globalgreeneco.com"><i class="fa fa-envelope"></i>orders@globalgreeneco.com</a></li>
                        </ul>
                    </div>
                    <div class="top-right">
                        <ul class="social-links clearfix">
                            <li><a target="_blank" href="https://www.facebook.com/globalgreeneco/"><span class="fa fa-facebook-f"></span></a></li>
                            <li><a  target="_blank" href="https://twitter.com/globalgreeneco"><span class="fa fa-twitter"></span></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/globalgreeneco/"><span class="fa fa-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-upper ">
            <div class="auto-container">
                <div class="clearfix">
                    <div class="pull-left logo-outer">
                        <div class="logo-box">
                            <div class="logo"><a href="<?php echo base_url() ?>" title="Factories">
                                    <h3 class=" logo-text" style="color: black; padding-top:18px;"><b>Global Green Eco<br>Technologies Pvt. Ltd.</b></h3>
                                </a></div>
                        </div>
                    </div>
                    <div class="pull-right upper-right clearfix">
                        <div class="upper-column info-box">
                            <a href="https://www.google.com/maps/search/25.4498054,81.8384797/@25.4498054,81.836291,17z?hl=en"  target="_blank"><div class="icon-box"><span class="fa fa-map-marker"></span></div>
                            <ul>
                                <li><strong>Our Address</strong></li>
                                <li>1, Lyall Road, Civil Lines, Allahabad</li>
                            </ul>
                            </a>
                        </div>
                        <div class="upper-column info-box">
                            <div class="icon-box"><span class="fa fa-clock-o"></span></div>
                            <ul>
                                <li><strong>Office Hour</strong></li>
                                <li>Mon- Sat : 10 am - 7 pm</li>
                            </ul>
                        </div>
                        <div class="upper-column info-box">
                       </div>
                    </div>
                </div>
            </div>
        </div>
