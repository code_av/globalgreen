﻿<style>
    @media (max-width: 760px) {
        .productrow {
            margin-left: 7px !important;

        }
    }
</style>
<section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <h1>Products</h1>
        <div class="bread-crumb-outer">
            <ul class="bread-crumb clearfix">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li class="active">Products</li>
            </ul>
        </div>
    </div>
</section>
<!--Sidebar Page-->
<div class="sidebar-page-container with-left-sidebar">
    <div class="auto-container">
        <div class="row clearfix productrow">

            <!--Content Side-->
            <div class="content-side pull-right col-lg-9 col-md-8 col-sm-12 col-xs-12">
                <!--Service Details-->
                <section class="service-details">
                   <div class="row productrow">
                       <?php
                       foreach($products as $pro)
                       {
                           if($pro['product_status']==1){
                       ?>
                       <div class="col-md-3" style="height: 290px;margin: 15px;border: solid 1px; border-color: dimgrey;">
                           <?php
                           $pro_name =$pro['name'] ;
                           // remove all the brackets
                           $pro_name = str_replace(array( '(', ')' ), '', $pro_name);
                           // remove all the special charters
                           $pro_name = preg_replace('/[^A-Za-z0-9 \']/', '', $pro_name);
                           // remove all the spaces
                           $string =strtolower(preg_replace("/[\s]/", "-", $pro_name));

                           ?>
                           <div class="inner hover-style1  hvr-float-shadow">
                               <figure class="image-box hover-style1-img" style="width: 100%; height: 200px;">
                               <?php if(isset($pro['images'][0]['image'])){ ?>
                                    <img src="<?php echo base_url($pro['images'][0]['image']); ?>"
                                     style="height:200px;"/>
                                    <?php    }?>
                               <div class="hover-style1-view">
                                   <a class="link-box" href="<?php echo base_url();?>add-to-cart/<?php echo $pro['product_id']; ?>" style="margin-left: -40px; height: 50px !important; width: 50px !important;">
                                       <span class="fa fa-shopping-cart" style="font-size: 25px; color: #7FB302;"></span></a>
                                   <a class="link-box" href="<?php echo base_url().'products/'.$string.'/'.$pro['product_id'].'/'.$pro['category_id']?>" style="margin-left: 40px; height: 50px !important; width: 50px !important;">
                                       <span class="fa fa-search" style="font-size: 25px; color: #7FB302;"></span></a>
                               </div>

                           </div>
                                    <figcaption style="text-align: center;font-size: 16px;color: black;margin-top: 10px;">
                                         <?php
                                         echo $pro['name'] . '<br/>';

                                        ?>
                                    </figcaption>
                           </figure>
                       </div>
                       <?php
                          }}
                       ?>
                   </div>
                </section>
            </div>

                                           <!--Content Side-->
