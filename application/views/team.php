﻿
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Team</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>index">Home</a></li>
                    <li><a href="<?php echo base_url()?>about-us-one">About Us</a></li>
                    <li class="active">Team</li>
                </ul>
            </div>
        </div>
    </section>
    
    
    <!--Team Section / Default-->
    <section class="team-section default">
    	<div class="auto-container">
            <div class="row clearfix">
            
                <!--Member Style Two-->
                <div class="member-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">	
                            <figure class="image"><img src="<?php echo base_url()?>public/assets/gallery1/images/team/1.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <ul class="social-links">
                                    <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h4><a href="<?php echo base_url()?>team-single">David Warner</a></h4>
                            <div class="designation">Founder & CEO</div>
                        </div>
                    </div>
                </div>
                
                <!--Team Member-->
                <div class="member-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">	
                            <figure class="image"><img src="<?php echo base_url()?>public/assets/gallery1/images/team/2.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <ul class="social-links">
                                    <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h4><a href="<?php echo base_url()?>team-single">Michael King</a></h4>
                            <div class="designation">Engineer</div>
                        </div>
                    </div>
                </div>
                
                <!--Team Member-->
                <div class="member-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">	
                            <figure class="image"><img src="<?php echo base_url()?>public/assets/gallery1/images/team/3.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <ul class="social-links">
                                    <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h4><a href="<?php echo base_url()?>team-single">Game Smith</a></h4>
                            <div class="designation">Founder & CEO</div>
                        </div>
                    </div>
                </div> 
                
                <!--Member Style Two-->
                <div class="member-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">	
                            <figure class="image"><img src="<?php echo base_url()?>public/assets/gallery1/images/team/4.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <ul class="social-links">
                                    <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h4><a href="<?php echo base_url()?>team-single">Eric Rowen</a></h4>
                            <div class="designation">Engineer</div>
                        </div>
                    </div>
                </div>
                
                <!--Team Member-->
                <div class="member-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">	
                            <figure class="image"><img src="<?php echo base_url()?>public/assets/gallery1/images/team/5.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <ul class="social-links">
                                    <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h4><a href="<?php echo base_url()?>team-single">Dave John </a></h4>
                            <div class="designation">President</div>
                        </div>
                    </div>
                </div>
                
                <!--Team Member-->
                <div class="member-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box"> 
                            <figure class="image"><img src="<?php echo base_url()?>public/assets/gallery1/images/team/6.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <ul class="social-links">
                                    <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h4><a href="<?php echo base_url()?>team-single">Seth Rollins</a></h4>
                            <div class="designation">Engineer</div>
                        </div>
                    </div>
                </div>  
                <!--Team Member-->
                <div class="member-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box"> 
                            <figure class="image"><img src="<?php echo base_url()?>public/assets/gallery1/images/team/7.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <ul class="social-links">
                                    <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h4><a href="<?php echo base_url()?>team-single">Seth Rollins</a></h4>
                            <div class="designation">Founder & CEO</div>
                        </div>
                    </div>
                </div>  
                <!--Team Member-->
                <div class="member-style-two col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">	
                            <figure class="image"><img src="<?php echo base_url()?>public/assets/gallery1/images/team/8.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <ul class="social-links">
                                    <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                    <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="lower-box">
                            <h4><a href="<?php echo base_url()?>team-single">Seth Rollins</a></h4>
                            <div class="designation">Team Leader</div>
                        </div>
                    </div>
                </div>  
                 
            </div>
            
        </div>
    </section>
    
