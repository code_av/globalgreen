﻿    <!--Main Slider-->
    <section class="main-slider" data-start-height="600" data-slide-overlay="yes">

        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/1.jpg" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="<?php echo base_url()?>public/assets/gallery1/images/main-slider/1.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                    <!--Slide Overlay-->
                    <div class="slide-overlay"></div>

                    <div class="tp-caption sft sfb tp-resizeme" data-x="left" data-hoffset="500" data-y="center" data-voffset="-70" data-speed="1500" data-start="0" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="text-left"><h2>Bring home durability<br> and Style</h2></div></div>
                    <div class="tp-caption sft sfb tp-resizeme" data-x="left" data-hoffset="500" data-y="center" data-voffset="40" data-speed="1500" data-start="500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="text text-left"></div></div>
<!--
                    <div class="tp-caption sfb sfb tp-resizeme" data-x="left" data-hoffset="500" data-y="center" data-voffset="120" data-speed="1500" data-start="1000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><a href="<?php /*echo base_url()*/?>about-us" class="theme-btn btn-style-one">Read More</a> &ensp;&ensp; <a href="<?php /*echo base_url()*/?>public/assets/js/services" class="theme-btn btn-style-two">OUR SERVICES</a></div>
-->
                    </li>


                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/3.jpg" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="<?php echo base_url()?>public/assets/gallery1/images/main-slider/3.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">


                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="15" data-y="center" data-voffset="-70" data-speed="1500" data-start="0" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><h2>Everything Suited to<br> your taste</h2></div>

                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="15" data-y="center" data-voffset="30" data-speed="1500" data-start="500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="text"></div></div>
<!--
                    <div class="tp-caption sfl sfb tp-resizeme" data-x="left" data-hoffset="15" data-y="center" data-voffset="110" data-speed="1500" data-start="1000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><a href="<?php /*echo base_url()*/?>contact" class="theme-btn btn-style-one">CONTACT US</a> &ensp;&ensp; <a href="<?php /*echo base_url()*/?>services" class="theme-btn btn-style-two">OUR SERVICES</a></div>
-->
                    </li>

                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/2.jpg" data-saveperformance="off" data-title="Awesome Title Here">
                    <img src="<?php echo base_url()?>public/assets/gallery1/images/main-slider/2.jpg" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                    <!--Slide Overlay-->
                    <div class="slide-overlay"></div>

                    <div class="tp-caption sft sfb tp-resizeme" data-x="left" data-hoffset="500" data-y="center" data-voffset="-70" data-speed="1500" data-start="0" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="text-left"><h2>A Complete Replacement <br>of Plywood</h2>.</h2></div></div>

                    <div class="tp-caption sft sfb tp-resizeme" data-x="left" data-hoffset="500" data-y="center" data-voffset="40" data-speed="1500" data-start="500" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><div class="text text-left"></div></div>
<!--
                    <div class="tp-caption sfb sfb tp-resizeme" data-x="left" data-hoffset="500" data-y="center" data-voffset="120" data-speed="1500" data-start="1000" data-easing="easeOutExpo" data-splitin="none" data-splitout="none" data-elementdelay="0.01" data-endelementdelay="0.3" data-endspeed="1200" data-endeasing="Power4.easeIn"><a href="<?php /*echo base_url()*/?>about-us" class="theme-btn btn-style-one">Read More</a> &ensp;&ensp; <a href="<?php /*echo base_url()*/?>services" class="theme-btn btn-style-two">OUR SERVICES</a></div>
-->
                    </li>

                </ul>
            </div>
        </div>
    </section>

    <!--Services Section-->
    <section class="welcome-section">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <h2>Welcome to <span>Global Green Eco Technologies</span></h2>
                <div class="separator"></div>
                <div class="text">Welcome and thank you for visiting the web site.we are the oldest and most <br>experienced aftermarket performance company in the industry. When you deal with<br> wood plastic composite product you benefit from our strength, experience.</div>
            </div>
            <div class="row clearfix">
                <!--Services Style Two-->
                <div class="services-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner hover-style1  hvr-float-shadow">
                        <figure class="image-box hover-style1-img">
                            <img src="<?php echo base_url() ?>public/assets/gallery1/images/resource/featured-image-1 .jpg" alt="">
                            <div class="hover-style1-view">
                                <a class="link-box" href="<?php echo base_url()?>service-single"><span class="fa fa-link"></span></a>
                            </div>
                        </figure>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url()?>service-single">Plywood and Mica</a></h3>
                            <div class="text">Being the reputed organization incorporated with experienced professionals, we serve with optimum quality Mica Plywood .
                            </div>
                        </div>
                    </div>
                </div>

                <!--Services Style Two-->
                <div class="services-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner hover-style1  hvr-float-shadow">
                        <figure class="image-box hover-style1-img">
                            <img src="<?php echo base_url()?>public/assets/gallery1/images/resource/featured-image-2.jpg" alt="">
                            <div class="hover-style1-view">
                                <a class="link-box" href="<?php echo base_url()?>service-single"><span class="fa fa-link"></span></a>
                            </div>
                        </figure>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url()?>service-single">Hardware Fittings</a></h3>
                            <div class="text">We offer an extensive range of brass hardware fittings which is of supreme quality.  All the range is made up of high quality raw material.</div>
                        </div>
                    </div>
                </div>

                <!--Services Style Two-->
                <div class="services-style-two col-md-4 col-sm-6 col-xs-12">
                    <div class="inner hover-style1  hvr-float-shadow">
                        <figure class="image-box hover-style1-img">
                            <img src="<?php echo base_url()?>public/assets/gallery1/images/resource/featured-image-3.jpg" alt="">
                            <div class="hover-style1-view">
                                <a class="link-box" href="<?php echo base_url()?>service-single"><span class="fa fa-link"></span></a>
                            </div>
                        </figure>
                        <div class="lower-box">
                            <h3><a href="<?php echo base_url()?>service-single">WPC Doors</a></h3>
                            <div class="text">WPC Doors are made by mechanized manufacturing process, <br>which offers uniform thickness without undulation.</div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!--End Services Section-->

    <!--Two Default Section-->
    <section class="default-section-two">
        <div class="auto-container">

            <div class="row clearfix">

                <div class="about-column column col-sm-12 col-xs-12">
                    <div class="inner">
                        <!--Title-->
                        <div class="sec-title">
                            <h2>About <span>Us</span></h2>
                        </div>

                        <div class="image-box"><img src="<?php echo base_url()?>public/assets/gallery1/images/resource/about.jpg" alt=""></div>
                        <div class="row clearfix">
                            <!--Text Column-->
                            <div class="text-column col-md-6 col-sm-6 col-xs-12">
                                <h3>Who We Are?</h3>
                                <div class="text">Global Green Eco Technologies is a high-tech joint venture which is specialized in researching ,manufacturing and marketing the enviromental friendly WPC materials.</div>
                                <div class="btn-box"><a href="<?php echo base_url()?>about" class="theme-btn btn-style-one">Learn More</a></div>
                            </div>
                            <!--Text Column-->
                            <div class="text-column col-md-6 col-sm-6 col-xs-12">
                                <h3>Our Mission</h3>
                                <div class="text"> Our mission is to produce products for the modern world, we strive hard to reach the new heights of success accompanied by our privileged customers.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--End Two Default Section-->

    <div class="tow-column">
        <div class="auto-container">
            <!--Title-->
            <div class="sec-title centered light">
                <h2>Who <span>We Are</span></h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                            <div class="service-block">
                                <div class="inner-box">
                                    <!--icon-box-->
                                    <div class="icon-box">
                                        <span class="flaticon-tools"></span>
                                    </div>
                                    <h3>We are Professional</h3>
                                    <div class="text">We are one of the well-distinguished companies in the market that is into presenting a quality approved range of Wood Plastic Composite Sheet to our customers.</div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                            <div class="service-block">
                                <div class="inner-box">
                                    <!--icon-box-->
                                    <div class="icon-box">
                                        <span class="flaticon-people-1"></span>
                                    </div>
                                    <h3>We are Trusted</h3>
                                    <div class="text">We are trusted because of our way of being, not because of our polished exteriors or our expertly crafted communications. </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <section class="testimonial">
        <div class="auto-container">
            <div class="sec-title centered">
                <h2>Our <span>Testimonials</span></h2>
            </div>

            <div class="row clearfix">
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="testimonial-item">
                        <div class="content">
                            <span class="fa fa-quote-left"></span>
                            <p>the quality of the product is awesome . <br>look of the products are <br>very attractive.</p>

                            <ul class="rating">
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                            </ul>
                        </div>
                        <div class="author">
                            <ul class="list-inline">
                                <li>
                                    <img src="<?php echo base_url()?>public/assets/gallery1/images/resource/testi-image-1.jpg" alt="">
                                </li>
                                <li>
                                    <h5>abhi banerjee</h5>
                                    <p>allahabad</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="testimonial-item">
                        <div class="content">
                            <span class="fa fa-quote-left"></span>
                            <p>The  best seller of wpc products,<br>and the price of product is<br>under budget</p>

                            <ul class="rating">
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                            </ul>
                        </div>
                        <div class="author">
                            <ul class="list-inline">
                                <li>
                                    <img src="<?php echo base_url()?>public/assets/gallery1/images/resource/testi-image-2.jpg" alt="">
                                </li>
                                <li>
                                    <h5>jain malik</h5>
                                    <p>kanpur</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="testimonial-item">
                        <div class="content">
                            <span class="fa fa-quote-left"></span>
                            <p>high quality products with<br>less price, these products <br>are very awesome</p>

                            <ul class="rating">
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                            </ul>
                        </div>
                        <div class="author">
                            <ul class="list-inline">
                                <li>
                                    <img src="<?php echo base_url()?>public/assets/gallery1/images/resource/testi-image-3.jpg" alt="">
                                </li>
                                <li>
                                    <h5>Manish Malhotra</h5>
                                    <p>Varanasi</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <section class="parallax" style="background-color:grey;">
        <div class="auto-container">
            <h2>Our goal is to have customer service that is not  <br> just the best, but its Legendary</h2>

        </div>
    </section>

    <!--Fluid Section One-->
    <section class="fluid-section-one" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/parallax2.jpg);">
    	<div class="outer-container clearfix">
            <!--Left Column-->
            <div class="left-column">
            	<div class="clearfix">
                	<div class="inner">
                    	<div class="title"><h3>Request A Call Back</h3></div>
                        <div class="text">
                            <p>We also provide the design and installation of WPC construction projects including Designer Kitchens, steps, WPC products, hardware fittings, WPC doors/Windows, Plywood and Mica Products.</p>
                            <p><span>Phone &amp; Email:</span> For any information contact with us through our <a href="#"> Email:orders@globalgreeneco.com</a> and you can also contact with directe by call us in this number <span>941502936</span></p>
                            <p><span>Office Hours:</span> We are alwyes open excpet saturday &amp; Sunday from <span>10:00am</span> to <span> 8:00pm</span></p>
                        </div>

                    </div>
                </div>
            </div>

            <!--Right Column-->
            <div class="right-column">
            	<div class="clearfix">
                	<div class="inner">
                    	<div class="title"><h3>Sign up for our service</h3></div>
                        <div class="text">Get a weekly email of our pros' current thinking about financial investing egiea and personal finance..</div>
                        <!--Quote Form-->
                        <div class="default-form quote-form-two">

                            <form method="post" action="<?php echo base_url()?>mail">
                                <div class="row clearfix">

                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="fname" value="" placeholder="Name " required="">
                                    </div>

                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="email" name="email" value="" placeholder="Email" required="">
                                    </div>

                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <textarea name="message" placeholder="Message" required=""></textarea>
                                    </div>

                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <button type="submit" class="theme-btn btn-style-one">Send Message </button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
