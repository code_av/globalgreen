<style>@media (max-width: 760px) {  .productrow {margin-left:7px !important;}</style>
<section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <h1>Products</h1>
        <div class="bread-crumb-outer">
            <ul class="bread-crumb clearfix">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li class="active">Products</li>

            </ul>
        </div>
    </div>
</section>
<div class="sidebar-page-container with-left-sidebar">
    <div class="auto-container">
        <div class="row clearfix productrow">
            <div class="content-side pull-right col-lg-9 col-md-8 col-sm-12 col-xs-12">
                <section class="service-details">
                    <div class="row">
                        <style>
                            .sec-title h2:before{
                                content:'';
                                position:absolute;
                                left:14px;
                                bottom:0px;
                                width: 80px;
                                border-bottom: 2px solid #FF1356;
                            }
                        </style>
                        <?php
                        // if alstone
                        if ($distributorId == 10) { ?>
                            <div class="inner-box">
                                <div class="sec-title">
                                    <h2 style="padding-left: 12px;">Welcome to <span>Alstone</span></h2>
                                    <br>
                                    <p class="col-md-10">
                                        Alstone are crafted out from the finest quality two layers of aluminium skins.
                                        Pre-coated and thermally bonded with non-toxic polyethylene core, these
                                        panels are known for their durability. Besides this, the offered ACPs are
                                        resistant to delamination, a result of strong adhesion attained by combining
                                        chemical and mechanical actions.
                                    </p>
                                    <p class="col-md-10">
                                        mail us your requirement at <a href="mailto:orders@globalgreeneco.com" style="color: #ff1356">
                                            orders@globalgreeneco.com</a>
                                    </p>
                                    <p class="col-md-10">
                                        <a class="btn btn-success" href="<?php echo base_url()?>alstone#e-catalogue">download e-catalogue</a>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                        <?php
                        if ($distributorId == 11) { ?>
                        <div class="inner-box">
                            <div class="sec-title">
                                <h2 style="padding-left: 12px;">Welcome to <span>Axilam</span></h2>
                                <br>
                                <p class="col-md-10">
                                    Axi Lam laminates aims to provide consumers with trendiest and user-friendly
                                    innovations that fulfil their various types of requirement, resulting in the
                                    gratitude and satisfaction of client and manufacturers as well as workers.
                                    With a strong adherence to quality and precision, Axi Lam laminates promises
                                    to serve the customers with the best and the most diverse portfolio and
                                    satisfactory services.
                                </p>
                                <p class="col-md-10">
                                    mail us your requirement at <a href="mailto: orders@globalgreeneco.com">orders@globalgreeneco.com</a>
                                </p>
                                <p class="col-md-10">
                                    <a class="btn btn-success" href="<?php echo base_url() ?>axilam/#e-catalogue">download e-catalogue</a>
                                </p>
                            </div>
                        </div>
                        <?php } ?>
                        <?php
                        if ($distributorId == 12) { ?>
                            <div class="inner-box">
                                <div class="sec-title">
                                    <h2 style="padding-left: 12px;">Welcome to <span>Ozone</span></h2>
                                    <br>
                                    <p class="col-md-10">
                                        Ozone offers internationally certified hardware solutions through its product
                                        portfolio of more than 3000 products divided into 10 product categories.
                                        Ozone has a strong distribution network across India including dealers,
                                        distributors, C&F agents & company own display cum training Centre- The
                                        Ozone Hub. These display centre showcase all product ranges under one roof
                                        & offers technical know-how of product application and usage.
                                    </p>
                                    <p class="col-md-10">
                                        mail us your requirement at <a href="mailto: orders@globalgreeneco.com">orders@globalgreeneco.com</a>
                                    </p>
                                    <p class="col-md-10">
                                        <a class="btn btn-success" href="<?php echo base_url() ?>ozone/#e-catalogue">download e-catalogue</a>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </section>
                <section class="service-details">
                    <div class="row">
                        <?php
                        if (count($products) == 0) {
                            echo '<br>';
                            echo '<h4 style="text-align: center">There are no products to show at the moment.</h4>';
                        }
                        foreach($products as $pro)
                        {
                            if($pro['product_status']==1){
                            ?>
                            <div class="col-md-3" style="height: 290px;margin: 15px;border: solid 1px; border-color: dimgrey">
                                <?php
                                $pro_name =$pro['name'] ;
                                // remove all the brackets
                                $pro_name = str_replace(array( '(', ')' ), '', $pro_name);
                                // remove all the special charters
                                $pro_name = preg_replace('/[^A-Za-z0-9 \']/', '', $pro_name);
                                // remove all the spaces
                                $string =strtolower(preg_replace("/[\s]/", "-", $pro_name));
                                ?>
                                <div class="inner hover-style1  hvr-float-shadow" style="">
                                    <figure class="image-box hover-style1-img" style="width: 100%; height: 200px;">
                                        <?php if(isset($pro['images'][0]['image'])){ ?>
                                            <img src="<?php echo base_url($pro['images'][0]['image']); ?>"
                                                 style="height:200px;"/>
                                        <?php    }?>
                                        <div class="hover-style1-view">
                                            <a class="link-box" href="<?php echo base_url();?>add-to-cart/<?php echo $pro['product_id']; ?>" style="margin-left: -40px; height: 50px !important; width: 50px !important;">
                                                <span class="fa fa-shopping-cart" style="font-size: 25px; color: #7FB302;"></span></a>
                                            <a class="link-box" href="<?php echo base_url().'products/'.$string.'/'.$pro['product_id'].'/'.$pro['category_id']?>" style="margin-left: 40px; height: 50px !important; width: 50px !important;">
                                                <span class="fa fa-search" style="font-size: 25px; color: #7FB302;"></span></a>
                                        </div>

                                </div>
                                <figcaption style="text-align: center;font-size: 16px;color: black;margin-top: 10px;">
                                    <?php echo $pro['name'] . '<br/>'; ?>
                                </figcaption>
                                </figure>

                                </a>
                            </div>
                            <?php
                        }}
                        ?>
                    </div>
                </section>
            </div>

