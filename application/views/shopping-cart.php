﻿
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Shopping Cart</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>index">Home</a></li>
                    <li><a href="<?php echo base_url()?>shop">Shop</a></li>
                    <li class="active">Shopping Cart</li>
                </ul>
            </div>
        </div>
    </section>
    
    <!--Cart Section-->
    <section class="cart-section">
        <div class="auto-container">
            
            <!--Cart Outer-->
            <div class="cart-outer">
                <div class="table-outer">
                    <table class="cart-table">
                        <thead class="cart-header">
                            <tr>
                                <th><span class="fa fa-trash-o"></span></th>
                                <th class="prod-column">Product</th>
                                <th class="price">Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td class="remove"><a href="#" class="remove-btn"><span class="fa fa-remove"></span></a><figure class="prod-thumb"><a href="#">
                                <img src="<?php echo base_url()?>public/assets/gallery1/images/shop/1.png" alt=""></a></figure></td>
                                <td class="prod-column">
                                    <div class="column-box">
                                        <!--<figure class="prod-thumb"><a href="#"><img src="images/resource/products/product-10.jpg" alt=""></a></figure>-->
                                        <h4 class="prod-title">Paint Brush</h4>
                                    </div>
                                </td>
                                <td class="price">$15.00</td>
                                <td class="qty"><input class="quantity-spinner" type="text" value="2" name="quantity"></td>
                                <td class="sub-total">$25.00</td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                
                <div class="cart-options clearfix">
                    <div class="pull-left">
                        <div class="apply-coupon clearfix">
                            <div class="form-group clearfix">
                                <input type="text" name="coupon-code" value="">
                            </div>
                            <div class="form-group clearfix">
                                <button type="button" class="theme-btn btn-style-one">Apply Coupon</button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="pull-right">
                        <button type="button" class="theme-btn btn-style-one">PROCEED TO CHECKOUT</button>
                        <button type="button" class="theme-btn btn-style-five">UPDATE CART</button>
                    </div>
                    
                </div>
                
                <div class="row clearfix">
                    
                    <div class="column pull-right col-md-4 col-sm-8 col-xs-12">
                        <h3>Cart Totals</h3>
                        <!--Totals Table-->
                        <ul class="totals-table">
                            <li class="clearfix title"><span class="col">Sub Total</span><span class="col">$30.00</span></li>
                            <li class="clearfix"><span class="col">Order</span><span class="col total">$30.00</span></li>
                        </ul>
                        
                    </div>
                    
                </div>
                
                
            </div>
            
        </div>
    </section>
