﻿
    
    <!--Page Title-->
    <section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
        <div class="auto-container">
            <h1>Testimonials</h1>
        	<div class="bread-crumb-outer">
                <ul class="bread-crumb clearfix">
                    <li><a href="<?php echo base_url()?>index">Home</a></li>
                    <li><a href="<?php echo base_url()?>about-us-one">About Us</a></li>
                    <li class="active">Testimonials</li>
                </ul>
            </div>
        </div>
    </section>
    
    
    <section class="testimonial">
        <div class="auto-container">

            <div class="row clearfix">
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="testimonial-item">
                        <div class="content">
                            <span class="fa fa-quote-left"></span>
                            <p>Lorem ipsum dolor sit amet ment tionim sea. Ei explicari evertitur uptatum vi core zupitor inciderint reforin alienum</p>

                            <ul class="rating">
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                            </ul>
                        </div>
                        <div class="author">
                            <ul class="list-inline">
                                <li>
                                    <img src="<?php echo base_url()?>public/assets/gallery1/images/team/1.jpg" alt="">
                                </li>
                                <li>
                                    <h5>Don Flethcer</h5>
                                    <p>Los Angeles</p>
                                </li>
                            </ul>
                        </div>
                    </div>     
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="testimonial-item">
                        <div class="content">
                            <span class="fa fa-quote-left"></span>
                            <p>Lorem ipsum dolor sit amet ment tionim sea. Ei explicari evertitur uptatum vi core zupitor inciderint reforin alienum</p>

                            <ul class="rating">
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                            </ul>
                        </div>
                        <div class="author">
                            <ul class="list-inline">
                                <li>
                                    <img src="<?php echo base_url()?>public/assets/gallery1/images/team/2.jpg" alt="">
                                </li>
                                <li>
                                    <h5>mack raider</h5>
                                    <p>California</p>
                                </li>
                            </ul>
                        </div>
                    </div>     
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="testimonial-item">
                        <div class="content">
                            <span class="fa fa-quote-left"></span>
                            <p>Lorem ipsum dolor sit amet ment tionim sea. Ei explicari evertitur uptatum vi core zupitor inciderint reforin alienum</p>

                            <ul class="rating">
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                            </ul>
                        </div>
                        <div class="author">
                            <ul class="list-inline">
                                <li>
                                    <img src="<?php echo base_url()?>public/assets/gallery1/images/team/3.jpg" alt="">
                                </li>
                                <li>
                                    <h5>Don Flethcer</h5>
                                    <p>Los Angeles</p>
                                </li>
                            </ul>
                        </div>
                    </div>     
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="testimonial-item">
                        <div class="content">
                            <span class="fa fa-quote-left"></span>
                            <p>Lorem ipsum dolor sit amet ment tionim sea. Ei explicari evertitur uptatum vi core zupitor inciderint reforin alienum</p>

                            <ul class="rating">
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                            </ul>
                        </div>
                        <div class="author">
                            <ul class="list-inline">
                                <li>
                                    <img src="<?php echo base_url()?>public/assets/gallery1/images/team/4.jpg" alt="">
                                </li>
                                <li>
                                    <h5>Don Flethcer</h5>
                                    <p>Los Angeles</p>
                                </li>
                            </ul>
                        </div>
                    </div>     
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="testimonial-item">
                        <div class="content">
                            <span class="fa fa-quote-left"></span>
                            <p>Lorem ipsum dolor sit amet ment tionim sea. Ei explicari evertitur uptatum vi core zupitor inciderint reforin alienum</p>

                            <ul class="rating">
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                            </ul>
                        </div>
                        <div class="author">
                            <ul class="list-inline">
                                <li>
                                    <img src="<?php echo base_url()?>public/assets/gallery1/images/team/5.jpg" alt="">
                                </li>
                                <li>
                                    <h5>mack raider</h5>
                                    <p>California</p>
                                </li>
                            </ul>
                        </div>
                    </div>     
                </article>
                <article class="col-md-4 col-sm-6 col-xs-12">
                    <div class="testimonial-item">
                        <div class="content">
                            <span class="fa fa-quote-left"></span>
                            <p>Lorem ipsum dolor sit amet ment tionim sea. Ei explicari evertitur uptatum vi core zupitor inciderint reforin alienum</p>

                            <ul class="rating">
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                                <li class="fa fa-star"></li>
                            </ul>
                        </div>
                        <div class="author">
                            <ul class="list-inline">
                                <li>
                                    <img src="<?php echo base_url()?>public/assets/gallery1/images/team/6.jpg" alt="">
                                </li>
                                <li>
                                    <h5>Don Flethcer</h5>
                                    <p>Los Angeles</p>
                                </li>
                            </ul>
                        </div>
                    </div>     
                </article>
            </div>
        </div>
    </section>
    
