<section class="page-title" style="background-image:url(<?php echo base_url() ?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <h1>UPVC Doors & Windows</h1>
    </div>
</section>
<section class="welcome-section">
    <div class="auto-container">
        <div class="sec-title centered">
            <h2>PROVIDING <span>STYLE</span>, <span>SUBSTANCE</span> & <span>SECURITY</span></h2>
            <div class="separator"></div>
            <div class="text" style="font-size: 18px; color: black; text-align: center">
                <i>uPVC Windows can transform your home with a number of performance and maintenance benefits.
                    Discover the potential with Global Green Eco uPVC today!</i>
            </div>
            <div class="text">
                <p>
                    Windows and doors are essential in any building, yet we often install inefficient windows and doors
                    which makes the home uncomfortable.
                </p>
                <p>
                    The solution to this is to install windows and doors that allow light into the home, but keep
                    you comfortable, irrespective of the weather conditions outside. You would have read about
                    many benefits of upVC Windows and Doors as its been in the country for more than 2
                    decades but have become more popular in the past 5 – 6 years.
                </p>
                <p>
                    There are uPVC Doors to match all kinds of interiors. You can choose the colour and its type
                    like the French door, Sliding door, Folding door, Tilt and Slide, Lift and slide or Low Threshold
                    door based on your preference. The Unique offering of Global Green Eco uPVC systems are that
                    they also come with dual colour options where you can have your choice of colour of wood
                    finish frame matching your interiors or flooring and solid colour for your exteriors.
                </p>
                <p>
                    order us now at <a href="mailto: orders@globalgreeneco.com">orders@globalgreeneco.com</a> you can
                    also <a href="<?php echo base_url() ?>product-category/upvc-doors-and-windows/23">explore our products line</a>
                </p>
            </div>
        </div>
    </div>
</section>
<section class="about-section">
    <div class="auto-container">
        <div class="row clearfix">
            <div class="content-column col-md-12 col-sm-12">
                <div class="row clearfix">
                    <div class="column">
                        <div class="footer-widget newsletter-widget">
                            <div class="newsletter-form">
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter hdpe">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/casement-window.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/casement-window.jpg" class="img-responsive" ></a>
                                    <p style="text-align: center">Casement Window</p>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/slider-door-with-multiple-sashes.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/slider-door-with-multiple-sashes.jpg" class="img-responsive" >
                                    </a>
                                    <p style="text-align: center">Slider door with multiple sashes</p>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter hdpe">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/sliding-door.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/sliding-door.jpg" class="img-responsive" ></a>
                                    <p style="text-align: center">Sliding door</p>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/sliding-window.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/sliding-window.jpg" class="img-responsive" ></a>
                                    <p style="text-align: center">Sliding window</p>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter hdpe">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/sliding-window-with-fixed-top.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/sliding-window-with-fixed-top.jpg" class="img-responsive" ></a>
                                    <p style="text-align: center">Sliding window fixed top</p>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/top-hung-window.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/top-hung-window.jpg" class="img-responsive"  ></a>
                                    <p style="text-align: center">Top hung window</p>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/villa-casement-window.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/villa-casement-window.jpg" class="img-responsive"  ></a>
                                    <p style="text-align: center">Villa casement window</p>
                                </div>
                                <div class="gallery_product col-lg-3 col-md-3 col-sm-4 col-xs-6 filter sprinkle">
                                    <a href="<?php echo base_url()?>public/assets/gallery1/images/home/villb-bay-window.jpg">
                                        <img src="<?php echo base_url()?>public/assets/gallery1/images/home/villb-bay-window.jpg" class="img-responsive"  ></a>
                                    <p style="text-align: center">Villa bay window</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>