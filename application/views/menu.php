<?php
$url=$this->uri->segment(1);
$url2=$this->uri->segment(2);
?>
<div class="header-lower header" id="myHeader">
    <div class="main-box">
        <div class="container">
            <div class="outer-container clearfix">
                <div class="nav-outer clearfix">
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li <?php if($url=='') { echo 'class="current"';}?>><a href="<?php echo base_url()?>">Home</a></li>
                                <li <?php if($url=='about') { echo 'class="current"';}?>><a href="<?php echo base_url()?>about">About</a></li>
                                <li <?php if($url=='services') { echo 'class="current"';}?>><a href="<?php echo base_url()?>services">Services</a></li>
                                <li class="dropdown" ><a href="#" <?php if($url2!='') { echo 'style="color:#FF1356!important"';}?> >Products</a>
                                    <ul>
                                        <?php
                                        echo '<li><a href="'.base_url().'/distributors-product/alstone/10">Alstone Products</a></li>';
                                        echo '<li><a href="'.base_url().'/distributors-product/axilam/11">Axilam Products</a></li>';
                                        echo '<li><a href="'.base_url().'/distributors-product/ozone/12">Ozone Products</a></li>';

                                        foreach($category as $cat)
                                        {
                                            if ($cat->status==1) {
                                                $cat_name =$cat->category_name ;
                                                // remove all the brackets
                                                $cat_name = str_replace(array( '(', ')' ), '', $cat_name);
                                                // remove all the special charters
                                                $cat_name = preg_replace('/[^A-Za-z0-9\-\']/', " ", $cat_name);
                                                // remove all the spaces
                                                $string =strtolower(preg_replace("/[\s]/", "-", $cat_name));
                                                echo '<li><a href="' . base_url() .'product-category/'. $string .'/' . $cat->category_id . '">' . $cat->category_name . '</a></li>';
                                            }
                                        }
                                        ?>

                                    </ul>
                                </li>
                                <li <?php if($url=='projects') { echo 'class="current"';}?>><a href="<?php echo base_url()?>projects">Projects</a></li>
                                <li <?php if($url=='contact') { echo 'class="current"';}?>><a href="<?php echo base_url()?>contact">Contact</a></li>
                                <!--<li style="  border-radius: 12px;\ ><a href="<?php /*echo base_url()*/?>cart" class="img-rounded"  style="border:1px solid white; padding: 5px;">Orders<span style="color: red;margin-left: 5px;"><?php /*echo $count;*/?></span></a></li>-->
                                <li id="visible-to-desktop" >
                                    <a href="<?php echo base_url()?>cart"><button class="btn btn-rounded" style="border-radius: 15px; width: 100px; background-color: #202020; border-color: white; <?php if($url=='cart') { echo 'color:#FF1356!important;';}else{echo 'color:white;';}?>"><b>ORDERS</b></button></a>
                                </li>
                                <li id="hide-on-desktop">
                                    <a href="<?php echo base_url()?>cart" class="hide-on-desktop">ORDERS</span></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="outer-box2" id="hide-on-desktop">
                        <div class="form-group">
                            <a href="<?php echo base_url()?>">
                                <img src="<?php echo base_url()?>public/assets/gallery1/images/home/logo1.png">
                                <h4 style="display: inline-block;color: #1e6dae !important;"> Global Green Eco</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="sticky-header">
    <div class="auto-container clearfix">
        <div class="logo pull-left">
            <a href="<?php echo base_url()?>index" title="Factories"><h2 class="logo-text">Global Green</h2></a>
        </div>
        <div class="right-col pull-right">
            <nav class="main-menu">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse clearfix">
                    <ul class="navigation clearfix">
                        <li  ><a href="<?php echo base_url()?>" <?php if ($url==''){?> style="color:#FF1356!important;"<?php }?>>Home</a></li>
                        <li <?php if($url=='about') { echo 'class="current"';}?>><a href="<?php echo base_url()?>about" <?php if ($url=='about'){?> style="color:#FF1356!important;"<?php }?>>About</a></li>
                        <li <?php if($url=='services') { echo 'class="current"';}?>><a href="<?php echo base_url()?>services" <?php if ($url=='services'){?> style="color:#FF1356!important;"<?php }?>>Services</a></li>
                        <li class="dropdown"><a href="#" <?php if($url2!='') { echo 'style="color:#FF1356!important"';}?> >Products</a>
                            <ul>
                                <?php
                                foreach($category as $cat)
                                {
                                    if($cat->status==1) {
                                        $cat_name =$cat->category_name ;
                                        // remove all the brackets
                                        $cat_name = str_replace(array( '(', ')' ), '', $cat_name);
                                        // remove all the special charters
                                        $cat_name = preg_replace('/[^A-Za-z0-9\-\']/', " ", $cat_name);
                                        // remove all the spaces
                                        $string =strtolower(preg_replace("/[\s]/", "-", $cat_name));
                                        echo '<li><a href="' . base_url() .'product-category/'. $string .'/' . $cat->category_id . '">' . $cat->category_name . '</a></li>';
                                    }
                                }
                                ?>
                            </ul>
                        </li>
                        <li <?php if($url=='projects') { echo 'class="current"';}?>><a href="<?php echo base_url()?>projects" <?php if ($url=='projects'){?> style="color:#FF1356!important;"<?php }?>>Projects</a></li>
                        <li <?php if($url=='contact') { echo 'class="current"';}?>><a href="<?php echo base_url()?>contact" <?php if ($url=='contact'){?> style="color:#FF1356!important;"<?php }?>>Contact</a></li>
                        <li <?php if($url=='cart') { echo 'class="current"';}?> >
                            <a href="<?php echo base_url()?>cart"><button class="btn btn-rounded"  style="border-radius: 15px; width: 100px; background-color: #202020; border-color: white; <?php if($url=='cart') { echo 'color:#FF1356!important;';}else{echo 'color:white;';}?>"><b>ORDERS</b></button></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
</header>