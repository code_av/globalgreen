<!--Page Title-->
<section class="page-title" style="background-image:url(<?php echo base_url()?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <h1>Products</h1>
        <div class="bread-crumb-outer">
            <ul class="bread-crumb clearfix">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li><a href="<?php echo base_url()?>modular-kitchen">Modular Kitchen</a></li>

            </ul>
        </div>
    </div>
</section>


<!--Sidebar Page-->
<div class="sidebar-page-container with-left-sidebar">
    <div class="auto-container">
        <div class="row clearfix">

            <!--Content Side-->
            <div class="content-side pull-right col-lg-9 col-md-8 col-sm-12 col-xs-12">
                <!--Service Details-->
                <section class="service-details">
                    <div class="row">
                            <div class="col-md-12" style="text-align: center;">
                                <figure>
                                    <a href="<?php echo base_url();?>public/assets/gallery1/images/resource/mk10.jpg">
                                        <img src="<?php echo base_url();?>public/assets/gallery1/images/resource/mk10.jpg" style="width:400px;height:400px;"/></a>
                                    <!--<figcaption><a href="<?php /*echo base_url();*/?>add-to-cart/"><button class="btn btn-rounded btn-warning" style="margin: 3px;">Add to cart</button></a></figcaption>-->

                                </figure>
                            </div>
                    </div>


                    <!--Text Content-->
                    <div class="content">
                        <h3>Modular Kitchen</h3>
                        <b>Product Code:</b>MK<br>
                        <p><b>Description:</b>
                            Modular Kitchen a term used for the modern kitchen layout, which consists of number of cabinets to hold different
                            things in different sections. There are two sections namely floor section and wall section,
                            floor section is one having cabinets attached to the floor and wall section having cabinets
                            attached to the wall and basically near to the rooftop. As we all know that we have space
                            limitations in the apartment and houses, modular kitchens help us in saving space and giving out
                            the best in limited space.
                            <br>
                            <br>
                            We give you a best designs and products for modular kitchens.
                        </p>

                        <!--Faqs-->
                        <div class="faqs">
                            <!--Accordion Box-->
                            <ul class="accordion-box">

                                <!--Block-->
                                <li class="accordion block ">
                                    <div class="acc-btn "><div class="icon-outer"><span class="icon fa fa-angle-right"></span></div> Product Dimensions</div>
                                    <div class="acc-content ">
                                        <div class="content">
                                            <!--photo gallery-->
                                            <div class="row">

                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk1.jpg"><img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk1.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk2.jpg"><img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk2.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk3.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk3.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk4.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk4.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk5.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk5.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk6.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk6.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk7.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk7.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div><div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk8.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk8.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk9.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk9.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk10.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk10.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk11.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk11.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk12.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk12.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk13.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk13.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>
                                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hd">
                                                    <a href="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk14.jpg">
                                                        <img src="<?php echo base_url(); ?>public/assets/gallery1/images/resource/mk14.jpg" style="height: 150px; width: 200px;" class="img-responsive"></a>
                                                </div>

                                            </div>
                                </li>
                            </ul><!--End Accordion Box-->
                        </div>
                    </div>
                </section>
            </div>
            <!--Content Side-->
