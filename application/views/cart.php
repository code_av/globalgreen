﻿

<!--Page Title-->
<section class="page-title" style="background-image:url(<?php echo base_url() ?>public/assets/gallery1/images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <h1>Cart</h1>
        <div class="bread-crumb-outer">
            <ul class="bread-crumb clearfix">
                <li><a href="<?php echo base_url()?>">Home</a></li>
                <li class="active">Cart</li>
            </ul>
        </div>
    </div>
</section>




<section class="why-chooseus sec-padd2">
    <div class="container">

        <!--Title-->
        <!--<div class="sec-title centered">
            <h2> <span>Cart Items</span></h2>
        </div>-->




        <div class="clearfix">
            <div class="inner">
                <div class="title"><h3>Place Your Order
                        <div class="default-form quote-form-two">



                            <form method="post" action="<?php echo base_url()?>mail_orders">
                                <div class="row clearfix">
                                    <?php $session_data = $this->session->userdata('cart');
                                    if(count($session_data) > 0) {
                                        $count=0;
                                        foreach ($session_data as $key => $value) {
                                            ?>


                                            <div class="form-group col-md-5 col-sm-5 col-xs-5">
                                                <input type="text" readonly name="items"
                                                       value="<?php echo $value["product_code"] . '-' . $value["product_name"]; ?>"
                                                       disabled required>
                                            </div>
                                            <div class="form-group col-md-1 col-sm-1 col-xs-1">
                                                <a href="<?php echo base_url().'removeCartItem/'.$value['product_id']?>/"><span class="pull-right"><i class="fa fa-remove"></i></span></a>
                                            </div>

                                            <?php
                                            $count++;
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="row clearfix">
                                    <?php
                                    $this->load->helper('form');?>
                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="fname" placeholder="Name"  value="<?=set_value('fname' ); ?>" required>
                                    </div>

                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <input type="tel" maxlength="10" minlength="10" name="phone"  value="<?php echo set_value('phone'); ?>" placeholder="Phone number" required>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <input type="text" name="address"  placeholder="Address" required  value="<?php echo set_value('address'); ?>">
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <textarea name="message" placeholder="Message" required><?php echo set_value('message'); ?></textarea>
                                    </div>

                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <button type="submit" class="theme-btn btn-style-one">Place Order
                                    </div>
                            </form>

                        </div>
                </div>
            </div>
        </div>

</section>

