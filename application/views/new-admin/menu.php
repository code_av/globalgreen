<body>
<?php
if (!$this->ion_auth->logged_in())
{
    redirect('auth/login');
}

$url=$this->uri->segment(1);

?>
<div id="navigation">
    <div class="container-fluid">
        <a href="<?php echo base_url();?>admin-dashboard" id="brand">Global Green</a>
        <ul class='main-nav'>
            <li <?php if($url=='admin-dashboard') { echo 'class="active"';}?>>
                <a href="<?php echo base_url()?>admin-dashboard" >
                    <span>Dashboard</span>
                </a>
            </li>
            <li <?php if($url=='category') { echo 'class="active"';}?>>
                <a href="<?php echo base_url()?>category">
                    <span>Category</span>
                </a>
            </li>
            <li <?php if($url=='sub-category') { echo 'class="active"';}?>>
                <a  href="<?php echo base_url()?>sub-category">
                    <span>Sub Category</span>
                </a>
            </li>
            <li <?php if($url=='distributor') { echo 'class="active"';}?>>
                <a  href="<?php echo base_url()?>distributor">
                    <span>Distributor</span>
                </a>
            </li>
            <li <?php if($url=='product') { echo 'class="active"';}?>>
                <a href="<?php echo base_url()?>product">
                    <span>Product</span>
                </a>
            </li>
            <li <?php if($url=='slider-image') { echo 'class="active"';}?>>
                <a href="<?php echo base_url()?>slider-image">
                    <span>Slider Images</span>
                </a>
            </li>
            <li <?php if($url=='manage-projects') { echo 'class="active"';}?>>
                <a href="<?php echo base_url()?>manage-projects">
                    <span>Projects</span>
                </a>
            </li>
            <li <?php if($url=='manage-services') { echo 'class="active"';}?>>
                <a href="<?php echo base_url()?>manage-services">
                    <span>Services</span>
                </a>
            </li>
        </ul>

        <div class="user">
            <ul class="icon-nav">
                <li class='dropdown'>
                    <a href="#" class='dropdown-toggle' data-toggle="dropdown">Admin</a>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="<?php echo base_url()?>change-password">Change Password</a>
                        </li>

                        <li>
                            <a href="<?php echo base_url()?>logout" >Logout </a>
                        </li>

                    </ul>
                </li>
            </ul>
        </div>

    </div>
    </div>
</div>

