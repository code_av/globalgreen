<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Sub Category</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>/admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href=""> Sub Category</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i> Sub Category Form</h3>
                    </div>
                    <div class="box-content nopadding">
                        <form action="<?php echo base_url()?>submit-sub-category" method="post"  class='form-horizontal form-bordered'>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Select Category </label>
                                <div class="controls">
                                    <select name="category" id="category" class=' input-xlarge' style="width: 285px;">
                                        <?php
                                        //var_dump($data);
                                        foreach($category as $options)
                                        {
                                            echo "<option value=".$options->category_id.">".$options->category_name."</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Sub Category Name</label>
                                <div class="controls">
                                    <input type="text" name="subcategoryname" id="subcategoryname" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Sub Category Name"><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>

                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                    </div>
            </div>
        </div>
        <!--table for category list-->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-color box-bordered">
                            <div class="box-title">
                                <h3 style="font-size: 17px;">
                                    <i class="icon-table"></i>
                                    Available Sub Catagories
                                </h3>
                            </div>
                            <div class="box-content nopadding">
                                <table class="table table-hover table-nomargin dataTable table-bordered">
                                    <thead>
                                    <tr >
                                        <th>Sub Category Name</th>
                                        <th>Category Name</th>
                                        <th class='hidden-350'>Status</th>
                                        <th class='hidden-1024'>Action</th>
                                    </tr>

                                    </thead>
                                    <tbody>




                                    <?php
                                    //var_dump($data);
                                    foreach($sub_category as $options)
                                    {   echo '<tr>';
                                    /*sub category name*/
                                        if(isset($options->sub_category_name)){
                                            echo '<td>'.$options->sub_category_name.'</td>';
                                        }
                                        else{
                                            echo '<td></td>';
                                        }
                                        /*category name*/
                                        if(isset($options->sub_category_name)) {
                                            echo '<td>' . $options->category_name . '</td>';
                                        }
                                        else{
                                            echo '<td></td>';
                                        }
                                        /*status*/
                                        if ($options->sub_category_status == 0) {?>
                                            <td class='hidden-350'><span class="label label-lightred">Inactive</span></td>
                                        <?php } else {?>
                                            <td class='hidden-350'><span class="label label-satgreen">Active</span></td>
                                        <?php }?>
                                        <!--action-->
                                        <?php
                                        echo '<td class="hidden-1024"><a href="'.base_url().'edit-sub-category/'.$options->sub_category_id.'" class="btn" rel="tooltip" title="Edit"><i class="icon-edit"></i></a>';/*<a href="'.base_url().'delete-sub-category/'.$options->sub_category_id.'">DELETE</a></td>*/?>
                                        <?php if ($options->sub_category_status == 0) {
                                        echo '| <a href="' . base_url() . 'activate-sub-category/' .$options->sub_category_id . '" onclick="return confirmChangeProductStatus();">ACTIVATE</a></td>';
                                    } if($options->sub_category_status==1)
                                    {
                                        echo '| <a href="' . base_url() .'de-activate-sub-category/'. $options->sub_category_id . '" onclick="return confirmChangeProductStatus();">DE-ACTIVATE</a></td>
                                    </tr>';
                                    }
                                    } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
</div>

        </body>
</html>