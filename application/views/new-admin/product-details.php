
<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Product Details</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>/admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="">Product details</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3 style="font-size: 17px;">
                            <i class="icon-table"></i>
                            Product Details
                        </h3>
                        <div class="span10">
                            <?php foreach($products as $data)
                            {
                                echo '<a href="'.base_url().'edit-product/'.$data['product_id'].'">';
                            }?>
                            <input type="button" value="Edit" class="btn btn-success pull-right">
                            </a>
                        </div>
                    </div>
                    <div class="box-content nopadding">
                        <table class="table table-hover table-nomargin table-bordered ">
                            <thead>
                            <tr >
                                <th style="text-align: center;">Title</th>
                                <th style="text-align: center;">Details</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($products as $data)
                            {
                                echo '<tr>
                            <td style="text-align: center;">Product Code:</td>
                            <td style="text-align: center;">'.$data['product_code'].'</td>
                            </tr>
                           <tr><td style="text-align: center;">Product Name:</td>
                           <td style="text-align: center;">'.$data['name'].'</td></tr>
                        
                        
                           <tr><td style="text-align: center;">Product Size:</td>
                           <td style="text-align: center;">'.$data['size'].'</td></tr>
                        
                        
                           <tr><td style="text-align: center;">Product Weight:</td>
                           <td style="text-align: center;">'.$data['weight'].'</td></tr>
                        
                        
                           <tr><td style="text-align: center;">Product Price:</td>
                           <td style="text-align: center;">'.$data['price'].'</td></tr>
                        
                        
                           <tr><td style="text-align: center;">Product Description:</td>
                           <td style="text-align: center;">'.$data['description'].'</td></tr>
                        
                        
                           <tr><td style="text-align: center;">Product material:</td>
                           <td style="text-align: center;">'.$data['material'].'</td></tr>
                            ';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i>Gallery</h3>
                    </div>
                    <div class="box-content ">

                        <ul class="gallery">
                            <?php
                            foreach($products as $data)
                            {
                                foreach($data['images'] as $img) {
                            ?>
                                    <li>
                                        <a href="#">
                                            <img src="<?php echo  base_url() . $img['image'] ;?>" alt="" style="width: 150px; height: 150px;">
                                        </a>
                                        <div class="extras">
                                            <div class="extras-inner">
                                                <a href="<?php echo  base_url() . $img['image'];?>" class='colorbox-image' rel="group-1">
                                                    <i class="icon-search"></i></a>
                                                <a href="<?php echo base_url().'delete-image/'. $img['gallery_id'] ;?>" class='del-gallery-pic'>
                                                    <i class="icon-trash"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


