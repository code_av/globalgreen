<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!--favicon-->

    <!-- jQuery Latest Version -->
    <script src="public/assets/js/vendor/jquery-1.12.4.min.js"></script>

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <script type="text/javascript">
        $('#example').dataTable({
            destroy: true,
            aaData: response.data
        });
    </script>


    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/bootstrap.min.css">
    <!-- Bootstrap responsive -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/bootstrap-responsive.min.css">
    <!-- jQuery UI -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/jquery-ui/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
    <!-- PageGuide -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/pageguide/pageguide.css">
    <!-- Fullcalendar -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/fullcalendar/fullcalendar.print.css" media="print">
    <!-- Tagsinput -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/tagsinput/jquery.tagsinput.css">
    <!-- chosen -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/chosen/chosen.css">
    <!-- multi select -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/multiselect/multi-select.css">
    <!-- timepicker -->
    <!-- colorpicker -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/colorpicker/colorpicker.css">
    <!-- Datepicker -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/datepicker/datepicker.css">
    <!-- Daterangepicker -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/daterangepicker/daterangepicker.css">
    <!-- Plupload -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/plupload/jquery.plupload.queue.css">
    <!-- select2 -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/select2/select2.css">
    <!-- icheck -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/icheck/all.css">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/style.css">
    <!-- Color CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/themes.css">


    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- Apple devices fullscreen -->
    <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

    <title>Global Green Eco Technologies Pvt. Ltd.</title>
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/plugins/datatable/TableTools.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/newAdminCss/admin-style.css">

    <!-- dataTables -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/datatable/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/datatable/TableTools.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/datatable/ColReorderWithResize.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/datatable/ColVis.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/datatable/jquery.dataTables.columnFilter.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/datatable/jquery.dataTables.grouping.js"></script>
    <!-- Chosen -->




    <!-- jQuery -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/jquery.min.js"></script>
    <!-- Nice Scroll -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- imagesLoaded -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/imagesLoaded/jquery.imagesloaded.min.js"></script>
    <!-- jQuery UI -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/jquery-ui/jquery.ui.spinner.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/jquery-ui/jquery.ui.slider.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/bootstrap.min.js"></script>
    <!-- Bootbox -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/bootbox/jquery.bootbox.js"></script>
    <!-- Masked inputs -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/maskedinput/jquery.maskedinput.min.js"></script>
    <!-- TagsInput -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/tagsinput/jquery.tagsinput.min.js"></script>
    <!-- Datepicker -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Daterangepicker -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/daterangepicker/moment.min.js"></script>
    <!-- Timepicker -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- Colorpicker -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/colorpicker/bootstrap-colorpicker.js"></script>
    <!-- Chosen -->
    <!-- MultiSelect -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/multiselect/jquery.multi-select.js"></script>
    <!-- CKEditor -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/ckeditor/ckeditor.js"></script>
    <!-- PLUpload -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/plupload/plupload.full.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/plupload/jquery.plupload.queue.js"></script>
    <!-- Custom file upload -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/fileupload/bootstrap-fileupload.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/mockjax/jquery.mockjax.js"></script>
    <!-- select2 -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/select2/select2.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/icheck/jquery.icheck.min.js"></script>
    <!-- complexify -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/complexify/jquery.complexify-banlist.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/complexify/jquery.complexify.min.js"></script>
    <!-- Mockjax -->

    <script src="<?php echo base_url();?>public/assets/newAdminJs/application.min.js"></script>
    <!-- Just for demonstration -->


    <!-- Touch enable for jquery UI -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/touch-punch/jquery.touch-punch.min.js"></script>
    <!-- slimScroll -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <!-- Bootstrap -->

    <!-- Theme framewrk -->
   <script src="<?php echo base_url();?>public/assets/newAdminJs/eakroko.min.js"></script>

    <script src="<?php echo base_url();?>public/assets/newAdminJs/demonstration.min.js"></script>
    <!-- jQuery UI -->
    <script src="<?php echo base_url();?>public/assets/newAdminJs/plugins/jquery-ui/jquery.ui.datepicker.min.js"></script>

    <!-- imagesLoaded -->


    <!--[if lte IE 9]>
    <script>
        $(document).ready(function() {
            $('input, textarea').placeholder();
        });
    </script>
    <![endif]-->

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" />
    <!-- Apple devices Homescreen icon -->
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>img/apple-touch-icon-precomposed.png" />


    <style type="text/css" rel="stylesheet">
        #files
        {
            display: block;
        }
    </style>

</head>
<body>

<?php
if (!$this->ion_auth->logged_in())
{
    redirect('login');
}

$this->load->view('new-admin/alert');
?>

<?php
if (!$this->ion_auth->logged_in())
{
    redirect('auth/login');
}

?>

<!--Header-part-->
