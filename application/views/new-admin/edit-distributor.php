<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Edit Distributor</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="">Edit Distributor</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i>Edit Distributor Form</h3>
                    </div>
                    <div class="box-content nopadding">
                        <?php
                        foreach ($distributor_details as $data) {?>
                        <form action="<?php echo base_url()?>update-distributor" method="post"  class='form-horizontal form-bordered'>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Distributor Name</label>
                                <div class="controls">
                                    <input type="text" name="distributorname" id="distributorname" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Distributor Name" value="<?php echo $data->distributor_name;?>">
                                    <input type="hidden" name="distributorid" value="<?php echo $data->distributor_id ;?>">
                                </div>
                            </div>
                            <?php }?>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                    </div>
            </div>
        </div>
            </div>
</div>

        </body>
</html>