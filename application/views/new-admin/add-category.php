<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>>
            <a href="#" title="Go to Home" class="tip-bottom"> Category</a>
        </div>
        <h1> Category</h1>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">

            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                        <h5>Category Form</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <form action="<?php echo base_url()?>submit-category" method="post" class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label">Category Code :</label>
                                <div class="controls">
                                    <input type="text" required class="span5" placeholder="Enter Category Code" name="categorycode" id="categorycode"/><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Category Name :</label>
                                <div class="controls">
                                    <input type="text" required class="span5" placeholder="Enter Category Name" name="categoryname" id="categoryname" /><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--table-->

        </div>

        <div class="row-fluid">


            <div class="span12" >
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon"><i class="icon-th"></i></span>
                        <h5>Available Categories</h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered data-table">
                            <thead>
                            <tr>
                                <th>Category Code</th>
                                <th>Category Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="gradeA">
                                <?php
                                //var_dump($data);
                                foreach($category as $options) {
                                    echo "<tr>";
                                    if (isset($options->category_code)) {
                                        echo '<td>' . $options->category_code . '</td>';
                                    } else {
                                        echo '<td></td>';
                                    }
                                    if (isset($options->category_name)) {
                                        echo '<td>' . $options->category_name . '</td>';
                                    } else {
                                        echo '<td></td>';
                                    }

                                    echo '<td><a href="' . base_url() . 'edit-category/' . $options->category_id . '">EDIT</a>|
                                     '; ?>
                                    <?php
                                    if ($options->status == 0) {
                                        echo '<a href="' . base_url() . 'activate-category/' . $options->category_id . '" onclick="return confirmChangeProductStatus();">Activate</a></td>
                                    </tr>';
                                    } else {
                                        echo '<a href="' . base_url() . 'de-activate-category/' . $options->category_id . '" onclick="return confirmChangeProductStatus();">De-activate</a></td>
                                    </tr>';
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


