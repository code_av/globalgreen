<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Change Slider Images</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>/admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="">Change Slider Image</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i>Header Slider Image</h3>
                    </div>
                    <div class="box-content nopadding">
                        <div class="span12 text-center" style="text-align: center;">
                            <div class="span7">
                                <form action="<?php echo base_url();?>change-header-slider-image" method="POST" class='form-horizontal  form-bordered' enctype='multipart/form-data'>

                                    <div class="control-group">
                                        <label for="textfield" class="control-label">Slider Image</label>
                                        <div class="controls">
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 100px; line-height: 20px;"></div>
                                                <div>
                                                    <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                        <span class="fileupload-exists">Change</span>
                                                        <input type="file" name='slider_image' id="slider_image" required/></span>
                                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                </div>
                                            </div>
                                            <span>Image size: less than 10mb & less than 1920 X 600</span>
                                        </div>
                                        <div class="control-group" >
                                            <label for="textfield" class="control-label">Slider Text</label>
                                            <div class="controls" >
                                                <textarea name="slider_text" id="textarea" class="input-block-level" style="width:80%!important; margin-left: 40px;" maxlength="40"> </textarea>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                </form>
                            </div>
                            </div>
                            <div class="span5">
                                <h4>Available Slider</h4>
                                <table class="table table-hover table-nomargin">
                                    <thead>
                                    <tr>
                                        <td><h6>Images</h6></td>
                                        <td><h6>Text</h6></td>
                                        <td><h6>Action</h6></td>
                                    </tr>
                                    </thead>
                                    <?php
                                    foreach($header_slider as $data)
                                    {
                                        ?>

                                        <tr>
                                        <td><img src="<?php echo base_url().$data->slider_image ;?>" style="width:150px;height:75px;"></td>
                                        <td><p><?php echo $data->slider_text ;?></p></td>
                                        <td><a href="<?php echo base_url().'delete_slider/'.$data->slider_id ;?>" class="btn ">Delete</a></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>

                            </div>
                    </div>
                </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i>About Us Section Slider Image</h3>
                    </div>
                    <div class="box-content nopadding">

                        <div class="span12 text-center" style="text-align: center;">
                            <div class="span7">
                                <form action="<?php echo base_url();?>add-about-slider-image" method="POST" class='form-horizontal  form-bordered' enctype='multipart/form-data'>
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">Slider Image</label>
                                        <div class="controls">
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 100px; line-height: 20px;"></div>
                                                <div>
                                                    <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                        <span class="fileupload-exists">Change</span>
                                                        <input type="file" name='slider_image' id="slider_image" required/></span>
                                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                </div>
                                            </div>
                                            <span>Image size: less than 10mb & less than 1920 X 250</span>
                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                </form>
                            </div>
                        </div>
                        <div class="span5">
                            <h4>Available Slider</h4>
                            <table class="table table-hover table-nomargin">
                                <thead>
                                <tr>
                                    <td><h6>Images</h6></td>
                                    <td><h6>Action</h6></td>
                                </tr>
                                </thead>
                                <?php
                                foreach($slider as $data)
                                {
                                    ?>
                                    <tr>
                                        <td><img src="<?php echo base_url().$data->sub_slider_image ;?>" style="width:150px;height:75px;"></td>
                                        <td><a href="<?php echo base_url().'delete_sub_slider/'.$data->sub_slider_id ;?>" class="btn ">Delete</a></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
        </div>
        <!--table for category list-->


    </div>
</div>
</body>
</html>