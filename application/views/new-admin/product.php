<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Product</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>/admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="">Product</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i> Product Form</h3>
                    </div>
                    <div class="box-content nopadding">
                        <?php $this->load->helper('form')?>
                        <form action="<?php echo base_url()?>submit-product" method="post"  class='form-horizontal form-bordered' name="basic_validate" id="basic_validate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label for="textfield" class="control-label">Select Category </label>
                                <div class="controls">
                                    <select class=' input-xlarge' style="width: 285px;" name="category" id="category">
                                        <option>Select</option>;
                                        <?php
                                        foreach($category as $options)
                                        {
                                            echo "<option value=".$options->category_id.">".$options->category_name."</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Select Sub Category</label>
                                <div class="controls">
                                    <select class=' input-xlarge' style="width: 285px;" name="subcategory" id="subcategory">
                                        <option ></option>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Select Distributor</label>
                                <div class="controls">
                                    <select class="input-xlarge" name="distributor" id="distributor" style="width: 285px;">
                                        <?php
                                        //var_dump($data);
                                        foreach($distributor as $options)
                                        {
                                            echo "<option value=".$options->distributor_id.">".$options->distributor_name."</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Code</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Product Code" name="productcode" id="productcode" value="<?=set_value('productcode' ); ?>"/><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Name</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2"  placeholder="Enter Product Name" name="productname" id="productname" value="<?=set_value('productname'); ?>"><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>
                                <div class="control-group">
                                    <label for="textfield" class="control-label">Product Size</label>
                                    <div class="controls">
                                        <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Product Size" name="productsize" id="productsize" value="<?=set_value('productsize'); ?>">
                                    </div>
                                </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Weight</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Product Weight" name="productweight" id="productweight" value="<?=set_value('productweight' ); ?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Price</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Product Price" name="productprice" id="productprice" value="<?=set_value('productprice' ); ?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Description</label>
                                <div class="controls">
                                    <textarea rows="5" style="width: 285px;" class="input-block-level" placeholder="Enter Product Description" name="productdescription" id="productdescription"><?=set_value('productdescription'); ?></textarea><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Material</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Product Material" name="productmaterial" id="productmaterial" value="<?=set_value('productmaterial'); ?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <h3>
                                    <button type="button" id="reset" class="btn btn-primary pull-right">Reset Selection</button>
                                </h3>
                                <label for="textfield" class="control-label">Select Images</label>
                                <div class="controls">
                                    <input type="file" id="files" name="product_img[]" multiple class="input-block-level">
                                    <span class="help-block">Only .jpg .jpeg .png (Max Size: 5MB)</span>
                                </div>
                            </div>
                                <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--table for category list-->
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3 style="font-size: 17px;">
                            <i class="icon-table"></i>
                            Available Products
                        </h3>
                    </div>
                    <div class="box-content nopadding">
                        <table class="table table-hover table-nomargin dataTable table-bordered">
                            <thead>
                            <tr >
                                <th>Product Code</th>
                                <th>Product Name</th>
                                <th>Category Name</th>
                                <th>Sub Category Name</th>
                                <th>Distributor</th>
                                <th class='hidden-350'>Status</th>
                                <th class='hidden-1024'>Action</th>
                            </tr>

                            </thead>
                            <tbody>
                            <?php
                            foreach($product_details as $data)
                            {
                                echo '<tr>';
                                /*product code*/
                                if(isset($data->product_code)) {
                                    echo '<td>' . $data->product_code. '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                            /*product Name*/
                                if(isset($data->name)) {
                                    echo '<td>' . $data->name . '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                            /*category name*/
                                if(isset($data->category_name)) {
                                    echo '<td>' . $data->category_name . '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                            /*sub categroy name*/
                                if(isset($data->sub_category_name)) {
                                    echo '<td>' . $data->sub_category_name . '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                            /*Distributor Name*/
                                if(isset($data->distributor_name)) {
                                    echo '<td>' . $data->distributor_name . '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                            /*status*/
                            if ($data->product_status == 0) {?>
                                <td class='hidden-350'><span class="label label-lightred">Inactive</span></td>
                            <?php } else {?>
                                <td class='hidden-350'><span class="label label-satgreen">Active</span></td>
                            <?php }
                            /*<!--action-->*/
                               echo ' <td>
                                    <a href="'.base_url().'view-details/'.$data->product_id.'" class="btn" rel="tooltip" title="View"><i class="icon-search"></i></a>
                                    <a href="'.base_url().'edit-product/'.$data->product_id.'" class="btn" rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                    <a href="'.base_url().'delete-product/'.$data->product_id.'" class="btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a>';
                               if ($data->product_status == 0) {
                                echo '<a href="' . base_url() . 'activate-product/' .$data->product_id . '" onclick="return confirmChangeProductStatus();">ACTIVATE</a></td>
                                    </tr>';
                            } if($data->product_status==1)
                            {
                                echo '<a href="' . base_url() .'de-activate-product/'. $data->product_id . '" onclick="return confirmChangeProductStatus();">DE-ACTIVATE</a></td>
                                    </tr>';
                            }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>

</div>
</div>


