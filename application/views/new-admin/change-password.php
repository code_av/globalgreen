<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Change Password</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>/admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="">Change Password</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
        </div>
        <br>

        <?php if(isset($message)){?> <div id="infoMessage" class="alert alert-danger"> <?php echo $message;}?>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i>Change Your Password Here</h3>
                    </div>
                    <div class="box-content nopadding">
                        <form action="<?php echo base_url()?>change_password" method="post"  class='form-horizontal form-bordered'>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Old Password</label>
                                <div class="controls">
                                    <input type="text"  name="old" id="old" class="input-xlarge" data-rule-required="true" minlength="8" placeholder="Enter Old Password" ><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>

                            <div class="control-group">
                                <label for="textfield" class="control-label">New Password</label>
                                <div class="controls">
                                    <input type="password"  name="new" id="new" class="input-xlarge" data-rule-required="true" minlength="8" placeholder="Enter Password" ><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                    <span>minimum 8 characters</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Re-enter Password</label>
                                <div class="controls">
                                    <input type="password"  name="new_confirm" id="new_confirm" class="input-xlarge" data-rule-required="true" minlength="5" placeholder="Enter Password" ><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                    <span id='message'></span>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Update Password</button>
                            </div>
                        </form>
                    </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#password, #confirm_password').on('keyup', function () {
        if ($('#password').val() == $('#confirm_password').val()) {
            $('#message').html('Matching').css('color', 'green');
        } else
            $('#message').html('Not Matching').css('color', 'red');
    });
</script>
</body>
</html>