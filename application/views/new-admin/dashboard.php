
<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Dashboard</h1>
            </div>
            <div class="pull-right">
            <ul class="minitiles">
                <li class='grey'>
                    <a href="<?php echo base_url();?>admin-dashboard"><i class="icon-refresh" ></i></a>
                </li>

            </ul>
                <ul class="stats">
                    <li class='lightgrey'>
                        <a href="<?php echo base_url();?>view-all-products">
                        <i class="icon-dashboard"></i>
                        <div class="details">
                            <span class="big"><?php echo $count;?></span>
                            <span>Products</span>
                        </div>
                    </li>
                    </a>
                </ul>
                <ul class="stats">
                    <li class='satgreen'>
                        <a>
                        <i class="icon-shopping-cart"></i>
                        <div class="details">
                            <span class="big"><?php echo $count_orders;?></span>
                            <span>New Orders</span>
                        </div>
                    </li>
                </a>
                </ul>
                    <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>

        <!--table for category list-->
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3 style="font-size: 17px;">
                            <i class="icon-table"></i>
                            New Orders
                        </h3>
                    </div>
                    <div class="box-content nopadding">
                        <table class="table table-hover table-nomargin dataTable table-bordered">
                            <thead>
                            <tr >
                                <th>Order Id</th>
                                <th>Client Name</th>
                                <th>Phone No.</th>
                                <th>Address</th>
                                <th>Date & Time</th>
                                <th>Viewed</th>
                                <th class='hidden-1024'>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($client_details as $order)
                            {
                            if($order->order_status==0){
                            $timestamp = strtotime($order->time);
                                $date = date('d-M-Y', $timestamp);
                                $time = date('h:i A', $timestamp);

                            echo '
                            <tr>
                                <td style="text-align: center;" >'.$order->order_id.'</td>
                                <td>'.$order->username.'</td>
                                <td>'.$order->phone.'</td>
                                <td>'.$order->address.'</td>
                                <td>'.$date.'|'. $time.'</td>
                                <td>No &nbsp&nbsp&nbsp 
                                <a href="'.base_url().'change-order-status-to-seen/'.$order->order_id.'" class="btn" rel="tooltip" title="add to viewed list" onclick="return confirmChangeProductStatus();"><i class="icon-edit"></i></a></td>
                                <td class="center hidden-1024">
                                <a href="'.base_url().'view-order/'.$order->order_id.'" class="btn" rel="tooltip" title="View"><i class="icon-eye-open"></i></a>
                                <a href="'.base_url().'delete-order/'.$order->order_id.'" class="btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a></td>
                            </tr>';}}?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-color box-bordered red">
                    <div class="box-title">
                        <h3 style="font-size: 17px;">
                            <i class="icon-table"></i>
                            Viewed Orders
                        </h3>
                    </div>
                    <div class="box-content nopadding">
                        <table class="table table-hover table-nomargin dataTable table-bordered">
                            <thead>
                            <tr >

                                <th>Order Id</th>
                                <th>Client Name</th>
                                <th>Phone No.</th>
                                <th>Address</th>
                                <th>Date & Time</th>
                                <th>Viewed</th>
                                <th class='hidden-1024'>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($client_details as $order)
                            {
                            if($order->order_status==1){
                                $timestamp = strtotime($order->time);
                                $date = date('d-M-Y', $timestamp);
                                $time = date('H:i A', $timestamp);
                                echo '
                            <tr >
                                <td style="text-align: center;">'.$order->order_id.'</td>
                                <td>'.$order->username.'</td>
                                <td>'.$order->phone.'</td>
                                <td>'.$order->address.'</td>
                                <td>'.$date.'|'. $time.'</td>
                                <td>Yes &nbsp&nbsp&nbsp 
                                <a href="'.base_url().'change-order-status-to-unseen/'.$order->order_id.'" class="btn" rel="tooltip" title="add to new orders list" onclick="return confirmChangeProductStatus();"><i class="icon-edit"></i></a></td>

                                <td class="center hidden-1024">
                                <a href="'.base_url().'view-order/'.$order->order_id.'" class="btn" rel="tooltip" title="View"><i class="icon-eye-open"></i></a>
                                <a href="'.base_url().'delete-order/'.$order->order_id.'" class="btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a></td>
                            </tr>';}}
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>



    </div>
