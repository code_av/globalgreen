
<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Order Details</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>/admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="">Order details</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="box box-color box-bordered">
                        <div class="box-title">
                            <h3>
                                <i class="icon-bar-chart"></i>
                                Order Details
                            </h3>
                        </div>
                        <div class="box-content">
                            <div class="statistic-big">
                                <div class="top">
                                </div>
                                <?php foreach ($client_detail as $cDetails)
                                    $timestamp = strtotime($cDetails->time);
                                $date = date('d-M-Y', $timestamp);
                                $time = date('H:i A', $timestamp);
                                ?>

                                    <div class="span3">
                                        <b>Name:</b>
                                    </div>
                                    <div class="span8">
                                        <?php echo $cDetails->username;?>
                                    </div>
                                        <div class="span3">
                                            <b>Mobile no.:</b>
                                        </div>
                                        <div class="span8">
                                            <?php echo $cDetails->phone;?>
                                        </div>
                                        <div class="span3">
                                            <b>Address:</b>
                                        </div>
                                        <div class="span8">
                                            <?php echo $cDetails->address;?>
                                        </div>
                                        <div class="span3">
                                            <b>Message:</b>
                                        </div>
                                        <div class="span8">
                                            <?php echo $cDetails->message;?>
                                        </div>
                                        <div class="span3">
                                            <b>Time :</b>
                                        </div>
                                        <div class="span8">
                                            <?php echo $date.'&nbsp&nbsp&nbsp&nbsp'.$time;?>
                                        </div>


                                <div class="span3">
                                    <b>Product Name & codes: </b>
                                </div>

                                <div class="span8">
                                    <?php
                                    foreach($order_detail as $data) {
                                        $p="";
                                        $p=$data->name.'('.$data->product_code.'),&nbsp&nbsp&nbsp&nbsp';
                                            echo $p;
                                    }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


