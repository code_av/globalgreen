<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1> Edit Category</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>/admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="">Edit Category</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i>Edit Category Form</h3>
                    </div>
                    <div class="box-content nopadding">
                        <?php
                        foreach ($category_details as $data) {?>
                        <form action="<?php echo base_url()?>update-category" method="post"  class='form-horizontal form-bordered'>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Category Code</label>
                                <div class="controls">
                                    <input type="text" name="categorycode" id="categorycode" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Category Code" value="<?php echo $data->category_code ?>"><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                    <input type="hidden" name="category_id" value="<?php echo $data->category_id ?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Category Name</label>
                                <div class="controls">
                                    <input type="text" name="categoryname" id="categoryname" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Category Name" value="<?php echo $data->category_name?>"><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>
                            <?php }?>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                            </div>
                        </form>
                    </div>
                    </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>