<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>>
            <a href="#" title="Go to Home" class="tip-bottom"> Category</a>
        </div>
        <h1> Category</h1>
    </div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5>Edit Gallery Images</h5>
                </div>
                <div class="widget-content nopadding">
                    <form action="<?php echo base_url()?>submit-sub-category" method="post" class="form-horizontal">
                        <div class="control-group">
                                <label class="control-label">Select Product:
                                 </label>


                                <div class="controls">
                                    <select class="span5" name="products">
                                     <?php
                                     //var_dump($data);
                                     foreach($product as $options)
                                     {

                                        echo "<option value=".$options->product_id.">".$options->name."</option>";
                                     } ?>
                                    </select>
                                </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">


        <div class="span12" >
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="icon-th"></i></span>
                    <h5>Available Images</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table" name="imageTable">
                        <thead>
                        <tr>
                            <th>Image Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="gradeA">
                            <?php
                            //var_dump($data);
                            foreach($sub_category as $options)
                            {   echo '<tr>';
                            if(isset($options->sub_category_name)){
                                echo '<td>'.$options->sub_category_name.'</td>';
                            }
                            else{
                               echo '<td></td>';
                            }
                            if(isset($options->sub_category_name)) {
                                echo '<td>' . $options->category_name . '</td>';
                            }
                            else{
                                echo '<td></td>';
                            }
                                echo '<td><a href="'.base_url().'edit-sub-category/'.$options->sub_category_id.'">EDIT</a>|<a href="'.base_url().'delete-sub-category/'.$options->sub_category_id.'">DELETE</a></td>';
                                echo '</tr>';
                            } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>









