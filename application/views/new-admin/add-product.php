<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>>
            <a href="#" title="Go to Home" class="tip-bottom"> product</a>
        </div>
        <h1> Product</h1>
    </div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title">
								<span class="icon">
									<i class="icon-info-sign"></i>
								</span>
                    <h5>Add Products</h5>
                </div>
                <div class="widget-content nopadding">

                    <form class="form-horizontal" method="post" action="<?php echo base_url()?>submit-product"  name="basic_validate" id="basic_validate" novalidate="novalidate" enctype="multipart/form-data">
                        <div class="control-group">
                            <label class="control-label " >Choose Category</label>
                            <div class="controls">
                                <select class="span5 category" name="category" id="category">
                                    <option>Select</option>;
                                    <?php
                                    //var_dump($data);
                                    foreach($category as $options)
                                    {

                                        echo "<option value=".$options->category_id.">".$options->category_name."</option>";
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Choose Sub Category:</label>
                            <div class="controls " >
                                <select class="span5 subcategory" name="subcategory" id="subcategory">
                                    <option ></option>
                                </select>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Choose Distributor:</label>
                            <div class="controls">
                                <select class="span5 distributor" name="distributor" id="distributor">
                                    <?php
                                    //var_dump($data);
                                    foreach($distributor as $options)
                                    {

                                        echo "<option value=".$options->distributor_id.">".$options->distributor_name."</option>";
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product Code</label>
                            <div class="controls">
                                <input type="text" required class="span5" placeholder="Enter Product Code" name="productcode" id="productcode" /><span style="color: red;font-size:25px; margin: 3px;">*</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product Name</label>
                            <div class="controls">
                                <input type="text" required class="span5" placeholder="Enter Product Name" name="productname" id="productname" /><span style="color: red;font-size:25px; margin: 3px;">*</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product Size</label>
                            <div class="controls">
                                <input type="text" class="span5" placeholder="Enter Product Size" name="productsize" id="productsize" required />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product Weight</label>
                            <div class="controls">
                                <input type="text" class="span5" placeholder="Enter Product Weight" name="productweight" id="productweight" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product Price</label>
                            <div class="controls">
                                <input type="number" class="span5" placeholder="Enter Product Price" name="productprice" id="productprice" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product Description</label>
                            <div class="controls">
                                <textarea class="span5" required placeholder="Enter Product Description" name="productdescription" id="productdescription" /></textarea><span style="color: red;font-size:25px; margin: 3px;">*</span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Product material</label>
                            <div class="controls">
                                <input type="text" class="span5" placeholder="Enter Product Material" name="productmaterial" id="productmaterial" />
                            </div>
                        </div>

                        <div class="control-group">
                            <h3>
                                <button type="button" id="reset" class="btn btn-primary pull-right">Reset Selection</button>
                            </h3>
                            <label class="control-label">Select Images</label>
                            <div class="controls">
                                <input type="file" id="files" name="product_img[]" multiple  />
                            </div>
                        </div>

                        <div class="form-actions">
                            <input type="reset" value="Reset" class="btn btn-warning">
                            <input type="submit" value="Save" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12" >
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="icon-th"></i></span>
                    <h5>Available Products</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Sub category</th>
                            <th>Distributor</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="gradeA">
                            <?php
                            foreach($product_details as $data)
                            {   echo '<tr>';
                                if(isset($data->name)) {
                                    echo '<td>' . $data->name . '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                                 if(isset($data->category_name)) {
                                    echo '<td>' . $data->category_name . '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                                if(isset($data->sub_category_name)) {
                                    echo '<td>' . $data->sub_category_name . '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                                if(isset($data->distributor_name)) {
                                    echo '<td>' . $data->distributor_name . '</td>';
                                }
                                else{
                                    echo '<td></td>';
                                }
                                echo '<td><a href="'.base_url().'edit-product/'.$data->product_id.'">EDIT</a>|<a href="'.base_url().'delete-product/'.$data->product_id.'">DELETE</a>|<a href="'.base_url().'view-details/'.$data->product_id.'">VIEW</a>|';?>
                                      <?php if ($data->product_status == 0) {
                                        echo '<a href="' . base_url() . 'activate-product/' .$data->product_id . '" onclick="return confirmChangeProductStatus();">ACTIVATE</a></td>
                                    </tr>';
                                    } if($data->product_status==1)
                                        {
                                        echo '<a href="' . base_url() .'de-activate-product/'. $data->product_id . '" onclick="return confirmChangeProductStatus();">DE-ACTIVATE</a></td>
                                    </tr>';
                                    }
                            } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>
</div>








