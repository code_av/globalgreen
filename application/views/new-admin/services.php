<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Services</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>/admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="">Services</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i>Add/Remove Services</h3>
                    </div>
                    <div class="box-content nopadding">
                        <div class="span12 text-center" style="text-align: center;">
                            <div class="span5">
                                <form action="<?php echo base_url();?>add-service" method="POST" class='form-horizontal  form-bordered' enctype='multipart/form-data'>
                                    <div class="control-group">
                                        <label for="textfield" class="control-label">Service icon</label>
                                        <div class="controls">
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image" /></div>
                                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 100px; line-height: 20px;"></div>
                                                <div>
                                                    <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                        <span class="fileupload-exists">Change</span>
                                                        <input type="file" name='service_icon' id="service_icon" required/></span>
                                                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                </div>
                                            </div>
                                            <span>Image size: less than 1mb & less than 200 X 200</span>
                                        </div>
                                        <div class="control-group" >
                                            <label for="textfield" class="control-label">Service Title</label>
                                            <div class="controls" >
                                                <input type="text" class="input-xlarge" placeholder="Service Title" name="service_title" maxlength="30" required>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label for="textarea" class="control-label">Service Description</label>
                                            <div class="controls">
                                                <textarea name="service_description" id="textarea" class="input-xlarge" minlength="20"  maxlength="250" required> </textarea>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <button type="reset" class="btn btn-primary">Reset</button>
                                        </div>
                                </form>
                            </div>
                            </div>
                            <div class="span7">
                                <h4>Available Services</h4>
                                <table style="width: 100%; margin: 2px -2px 1px;" class="table table-hover table-nomargin">
                                    <thead>
                                    <tr>
                                        <td><h6>Service Icon</h6></td>
                                        <td><h6>Title</h6></td>
                                        <td><h6>Description</h6></td>
                                        <td><h6>Action</h6></td>
                                    </tr>
                                    </thead>
                                    <?php
                                    foreach($services as $service)
                                    {
                                        ?>

                                        <tr>
                                        <td><img src="<?php echo base_url().$service->service_icon ;?>" style="width:150px;height:75px;"></td>
                                        <td><p><?php echo $service->service_title ;?></p></td>
                                        <td><p style="word-wrap: break-word;"><?php echo $service->service_description ;?></p></td>
                                        <td><a href="<?php echo base_url().'delete_service/'.$service->service_id ;?>" class="btn ">Delete</a></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>

                            </div>
                    </div>
                </div>

    </div>
        </div>
        <!--table for category list-->


    </div>
</div>

</body>
</html>