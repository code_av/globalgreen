<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Category</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>/admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="">Category</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i> Category Form</h3>
                    </div>
                    <div class="box-content nopadding">
                        <form action="<?php echo base_url()?>submit-category" method="post"  class='form-horizontal form-bordered'>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Category Code</label>
                                <div class="controls">
                                    <input type="text" name="categorycode" id="categorycode" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Category Code">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Category Name</label>
                                <div class="controls">
                                    <input type="text" name="categoryname" id="categoryname" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Category Name"><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>

                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                    </div>
            </div>
        </div>
        <!--table for category list-->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-color box-bordered">
                            <div class="box-title">
                                <h3 style="font-size: 17px;">
                                    <i class="icon-table"></i>
                                    Available Catagories
                                </h3>
                            </div>
                            <div class="box-content nopadding">
                                <table class="table table-hover table-nomargin dataTable table-bordered ">
                                    <thead>
                                    <tr>
                                        <th>Category Code</th>
                                        <th>Category Name</th>
                                        <th class='hidden-350'>Status</th>
                                        <th class='hidden-1024'>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //var_dump($data);
                                    foreach($category as $options) {
                                        echo "<tr>";
                                        /*for category code*/
                                        if (isset($options->category_code)) {
                                            echo '<td>' . $options->category_code . '</td>';
                                        } else {
                                            echo '<td></td>';
                                        }
                                        /*for category name*/
                                        if (isset($options->category_name)) {
                                            echo '<td>' . $options->category_name . '</td>';
                                        } else {
                                            echo '<td></td>';
                                        }?>
                                        <!-- for status-->
                                        <?php
                                        if ($options->status == 0) {?>
                                            <td class='hidden-350'><span class="label label-lightred">Inactive</span></td>
                                        <?php } else {?>
                                            <td class='hidden-350'><span class="label label-satgreen">Active</span></td>
                                        <?php }?>
                                        <!--Action Column-->
                                           <td class="hidden-1024"><a href="<?php echo base_url() ?>edit-category/<?php echo $options->category_id; ?>" class="btn" rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                            <?php if ($options->status == 0) {
                                            echo '<a href="' . base_url() . 'activate-category/' . $options->category_id . '" onclick="return confirmChangeProductStatus();">Activate</a></td>
                                    </tr>';
                                        } else {
                                            echo '<a href="' . base_url() . 'de-activate-category/' . $options->category_id . '" onclick="return confirmChangeProductStatus();">De-activate</a></td>
                                    </tr>';
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
</div>

        </body>
</html>