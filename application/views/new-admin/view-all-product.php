<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Product List</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>/admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="">Product</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i> Select For Product</h3>
                    </div>
                    <div class="box-content nopadding">
                        <form action="<?php echo base_url()?>view" method="post"  class='form-horizontal form-bordered' name="basic_validate" id="basic_validate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label for="textfield" class="control-label">Select Category </label>
                                <div class="controls">
                                    <select class=' input-xlarge' style="width: 285px;" name="category" id="category">
                                        <option>Select</option>;
                                        <?php
                                        foreach($category as $options)
                                        {
                                            echo "<option value=".$options->category_id.">".$options->category_name."</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Select Sub Category</label>
                                <div class="controls">
                                    <select class=' input-xlarge' style="width: 285px;" name="subcategory" id="subcategory">
                                        <option ></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">View Products</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php if(isset($products)){?>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-color box-bordered">
                    <div class="box-title">
                        <h3 style="font-size: 17px;">
                            <i class="icon-table"></i>
                            Products
                        </h3>
                    </div>
                    <div class="box-content nopadding">
                        <table class="table table-hover table-nomargin dataTable table-bordered">
                            <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Size</th>
                                <th>Weight</th>
                                <th>Material</th>
                                <th class="hidden-1024">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            foreach ($products as $data) {
                                echo '
                                    <tr>';

                                if (isset($data->name)) {
                                    echo '<td>' . $data->name . '</td>';
                                } else {
                                    echo '<td></td>';
                                }
                                if (isset($data->size)) {
                                    echo '<td>' . $data->size . '</td>';
                                } else {
                                    echo '<td></td>';
                                }
                                if (isset($data->weight)) {
                                    echo '<td>' . $data->weight . '</td>';
                                } else {
                                    echo '<td></td>';
                                }
                                if (isset($data->material)) {
                                    echo '<td>' . $data->material . '</td>';
                                } else {
                                    echo '<td></td>';
                                }
                                echo '<td>
                                    <a href="' . base_url() . 'view-details/' . $data->product_id . '" class="btn" rel="tooltip" title="View"><i class="icon-search"></i></a>
                                    <a href="' . base_url() . 'edit-product/' . $data->product_id . '" class="btn" rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                    <a href="' . base_url() . 'view-details/' . $data->product_id . '" class="btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a>';
                                if ($data->product_status == 0) {
                                    echo '<a href="' . base_url() . 'activate-product/' . $data->product_id . '" onclick="return confirmChangeProductStatus();">ACTIVATE</a></td>
                                        </tr>';
                                }
                                if ($data->product_status == 1) {
                                    echo '<a href="' . base_url() . 'de-activate-product/' . $data->product_id . '" onclick="return confirmChangeProductStatus();">DE-ACTIVATE</a></td>
                                            </tr>';
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
<?php } ?>
    </div>
</div>
