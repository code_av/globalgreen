<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- Apple devices fullscreen -->
    <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />

    <title>Global Green Login</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/AdminCss/bootstrap.min.css">
    <!-- Bootstrap responsive -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/AdminCss/bootstrap-responsive.min.css">
    <!-- icheck -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/AdminCss/plugins/icheck/all.css">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/AdminCss/style.css">
    <!-- Color CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>public/assets/AdminCss/themes.css">


    <!-- jQuery -->
    <script src="<?php echo base_url();?>public/assets/AdminJs/jquery.min.js"></script>

    <!-- Nice Scroll -->
    <script src="<?php echo base_url();?>public/assets/AdminJs/plugins/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- Validation -->
    <script src="<?php echo base_url();?>public/assets/AdminJs/plugins/validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/AdminJs/plugins/validation/additional-methods.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url();?>public/assets/AdminJs/plugins/icheck/jquery.icheck.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url();?>public/assets/AdminJs/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>public/assets/AdminJs/eakroko.js"></script>

    <!--[if lte IE 9]>
    <script src="<?php echo base_url();?>public/assets/AdminJs/plugins/placeholder/jquery.placeholder.min.js"></script>
    <script>
        $(document).ready(function() {
            $('input, textarea').placeholder();
        });
    </script>
    <![endif]-->


    <!-- Favicon -->

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo base_url()?>public/assets/gallery1/images/favicon/safari-pinned-tab.svg" color="#5bbad5">



</head>

<body class='login'>
<div class="wrapper">
    <h1><a href="index.html"><img src="<?php echo base_url();?>public/assets/gallery1/images/home/logo1.png" alt="" class='retina-ready' width="59" height="49">FLAT</a></h1>
    <div class="login-body">
        <h2>LOGIN</h2>
        <form action="<?php echo base_url();?>loginUser" method='post' name='process' enctype="multipart/form-data" class='form-validate' id="test">
            <div class="control-group">
                <div class="email controls">
                    <input type="text" name="username" placeholder="Email address" class='input-block-level' data-rule-required="true" data-rule-email="true">
                </div>
            </div>
            <div class="control-group">
                <div class="pw controls">
                    <input type="password"  name="password" placeholder="Password" class='input-block-level' data-rule-required="true">
                </div>
            </div>
            <div class="submit">
                <div class="remember">
                    <input type="checkbox" name="remember" class='icheck-me' data-skin="square" data-color="blue" id="remember"> <label for="remember">Remember me</label>
                </div>
                <input type="submit" value="Login!" class='btn btn-primary'>
            </div>
        </form>
    </div>
</div>
</body>

</html>
