
<script type="text/javascript">
    $(document).ready(function() {
        console.log('inn');
        $("#category").on('change', function () {
            var counter = 0;

            var category_id = $('#category').find(":selected").val();
            var url = "get_sub_category/" + category_id;
            //console.log(get);
            $.get(url, function (data) {
                console.log('yes');
                $('#subcategory').find('option').remove().end().append(new Option("Select", ""));
                // then append it to the select element

                //var values = JSON.parse(data);
                console.log(data[1]);

                $.each(data, function () {
                    console.log(data[counter].id);

                    $('#subcategory').append(new Option(data[counter].sub_category_name, data[counter].sub_category_id));
                    counter++;
                });
            });
        });


        if (window.File && window.FileList && window.FileReader) {
            $("#files").on("change", function (e) {
                var files = e.target.files,
                    filesLength = files.length;
                for (var i = 0; i < filesLength; i++) {
                    var f = files[i];
                    console.log(f);
                    var fileReader = new FileReader();
                    fileReader.onload = (function (e) {
                        var file = e.target;
                        $("<span class=\"pip\">" +
                            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\" style=\"width:120px;height:100px; border: 1px solid #CDCDCD; margin: 2px;" + ";\"/>" +
                            +
                                "</span>").insertAfter("#files");
                    });
                    fileReader.readAsDataURL(f);
                }
            });

            $('#reset').on("click", function () {
                $('.pip').remove();
                $('#files').val("");
            });

        }
        else {
            alert("Your browser doesn't support to File API")
        }
    });



    function confirmChangeProductStatus()
    {
        return confirm("Are you sure you want to change the product status ? ");
    }
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
    // This function is called from the pop-up menus to transfer to
    // a different page. Ignore if the value returned is a null string:
    function goPage (newURL) {

        // if url is empty, skip the menu dividers and reset the menu selection to default
        if (newURL != "") {

            // if url is "-", it is this page -- reset the menu:
            if (newURL == "-" ) {
                resetMenu();
            }
            // else, send page to designated URL
            else {
                document.location.href = newURL;
            }
        }
    }

    // resets the menu selection upon entry to this page:
    function resetMenu() {
        document.gomenu.selector.selectedIndex = 2;
    }
</script>

</body>

</html>
