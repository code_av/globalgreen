<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1> Edit Product</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>/admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href=""> Edit Product</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i>Edit Product Form</h3>
                    </div>
                    <div class="box-content nopadding">
                        <?php
                        foreach ($product_details as $data) {?>
                        <form action="<?php echo base_url()?>update-product" method="post"  class='form-horizontal form-bordered' name="basic_validate" id="basic_validate" enctype="multipart/form-data">
                            <div class="control-group">
                                <label for="textfield" class="control-label">Select Distributor</label>
                                <div class="controls">
                                    <select class="input-xlarge" name="distributor" id="distributor" style="width: 285px;">
                                        <option value="<?php echo $products[0]['distributor_id'];?>"><?php echo $products[0]['distributor_name'];?></option>
                                        <?php foreach ($distributor as $dist){
                                            if($dist->distributor_id!=$data->distributor_id){
                                                echo '<option value="'.$dist->distributor_id.'">'.$dist->distributor_name.'</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Code</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Product Code" name="productcode" id="productcode" value="<?php echo $data->product_code ;?>"/><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                    <input type="hidden" name="productid" id="productid" value="<?php echo $data->product_id;?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Name</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2"  placeholder="Enter Product Name" name="productname" id="productname" value="<?php echo $data->name ;?>"><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>
                                <div class="control-group">
                                    <label for="textfield" class="control-label">Product Size</label>
                                    <div class="controls">
                                        <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Product Size" name="productsize" id="productsize" value="<?php echo $data->size ;?>">
                                    </div>
                                </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Weight</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Product Weight" name="productweight" id="productweight" value="<?php echo $data->weight ;?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Price</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Product Price" name="productprice" id="productprice" value="<?php echo $data->price ;?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Description</label>
                                <div class="controls">
                                    <textarea rows="5" style="width: 285px;" class="input-block-level" placeholder="Enter Product Description" name="productdescription" id="productdescription"><?php echo $data->description ;?></textarea><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Product Material</label>
                                <div class="controls">
                                    <input type="text" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Product Material" name="productmaterial" id="productmaterial" value="<?php echo $data->material ;?>">
                                </div>
                            </div>

                            <div class="control-group">
                                <h3>
                                    <button type="button" id="reset" class="btn btn-primary pull-right">Reset Selection</button>
                                </h3>
                                <label for="textfield" class="control-label">Select Images</label>
                                <div class="controls">
                                    <input type="file" id="files" name="product_img[]" multiple class="input-block-level">
                                    <span class="help-block">Only .jpg .jpeg .png (Max Size: 5MB)</span>
                                </div>
                            </div>                                <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save Changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php }?>

        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i>Gallery</h3>
                    </div>
                    <div class="box-content ">

                        <ul class="gallery">
                            <?php
                            foreach($products as $data)
                            {
                                foreach($data['images'] as $img) {?>
                                    <li>
                                        <a href="#">
                                            <img src="<?php echo  base_url().$img['image'] ;?>" alt="" style="width: 150px; height: 150px;">
                                        </a>
                                        <div class="extras">
                                            <div class="extras-inner">
                                                <a href="<?php echo  base_url().$img['image'] ;?>" class='colorbox-image' rel="group-1">
                                                    <i class="icon-search"></i></a>
													<a href="<?php echo  base_url().'delete-image/'. $img['gallery_id'];?>" class='del-gallery-pic'>
                                                        <i class="icon-trash"></i>
                                                     </a>
                                            </div>
                                        </div>
                                    </li>
                            <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>