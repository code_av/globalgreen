<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>>
        <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> H</a>
        <h1>Dashboard</h1>
    </div>
</div>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5> Add product Sample Images</h5>
                </div>
                <div class="widget-content nopadding">
                    <form action="<?php echo base_url()?>submit-sample-image" enctype = "multipart/form-data" method="post" class="form-horizontal">
                    <div class="control-group">
                        <label class="control-label">Select Product:</label>
                        <div class="controls">
                            <select class="span5" name="productId">
                                <?php
                                //var_dump($data);
                                foreach($products as $options)
                                {

                                    echo "<option value=".$options->product_id.">".$options->name."</option>";
                                } ?>
                            </select>
                        </div>
                    </div>

                        <div class="control-group">
                            <label class="control-label" for="name">Title Name *</label>
                            <div class="controls">
                                <input type="text" name="name" id="name" class="form-control span5" required>
                            </div>
                        </div>
                            <div class="control-group">
                                <label class="control-label" for="address">Sample Image *</label>
                                    <div class="controls">
                                        <input type="file" name="logo" id="logo" class="form-control span5" required>
                                    </div>
                            </div>
                        <div class="control-group">
                            <label class="control-label" for="address">Color Code *</label>
                            <div class="controls">
                                <input type="text" name="colorCode" id="colorCode" class="form-control span5" required>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">Upload Image</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--
    <div class="row-fluid">


        <div class="span12" >
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="icon-th"></i></span>
                    <h5>Available Categories</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Image</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php
/*                            //var_dump($data);
                            foreach($images as $item)
                            {   echo '<tr class="gradeA">';
                            if(isset($item->name)){
                                echo '<td>'.$item->name.'</td>';
                            }
                            else{
                               echo '<td></td>';
                            }
                            if(isset($item->image)) {
                                echo '<td><img src="'.base_url().$item->image.'" height="75px" width="75px"></td>';
                            }
                            else{
                                echo '<td></td>';
                            }
                                echo '<td>
                                            <a class="btn btn-xs btn-danger " href="'.base_url().'delete_gallery/'.$item->image_id.'" onclick="return confirm(\'Are you sure ? This will delete the image from Slider Images\')">delete</a></td>';
                                echo '</tr>';
                            } */?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
-->
    </div>









