<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Distributor</h1>
            </div>
            <div class="pull-right">
                <ul class="stats">
                    <li class='lightred'>
                        <i class="icon-calendar"></i>
                        <div class="details">
                            <span class="big">February 22, 2013</span>
                            <span>Wednesday, 13:56</span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="breadcrumbs">
            <ul>
                <li>
                    <a href="<?php echo base_url();?>admin-dashboard">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="">Distributor</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
            <div class="close-bread">
                <a href="#"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="box box-bordered box-color">
                    <div class="box-title">
                        <h3 style="font-size: 17px;"> <i class="icon-th-list"></i> Distributor Form</h3>
                    </div>
                    <div class="box-content nopadding">
                        <form action="<?php echo base_url()?>submit-distributor" method="post"  class='form-horizontal form-bordered'>
                            <div class="control-group">
                                <label for="textfield" class="control-label">Distributor Name</label>
                                <div class="controls">
                                    <input type="text" name="distributorname" id="distributorname" class="input-xlarge" data-rule-required="true" data-rule-minlength="2" placeholder="Enter Distributor Name"><span style="color: red;font-size:25px; margin: 3px;">*</span>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                    </div>
            </div>
        </div>
        <!--table for category list-->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="box box-color box-bordered">
                            <div class="box-title">
                                <h3 style="font-size: 17px;">
                                    <i class="icon-table"></i>
                                    Available Distributors
                                </h3>
                            </div>
                            <div class="box-content nopadding">
                                <table class="table table-hover table-nomargin dataTable table-bordered ">
                                    <thead>
                                    <tr>
                                        <th style="text-align: center">Distributor Name</th>
                                        <th class='hidden-1024' style="text-align: center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    //var_dump($data);
                                    foreach($distributor as $vendors)
                                    {   echo '<tr>';
                                        if(isset($vendors->distributor_name)) {
                                            echo '<td style="text-align: center">' . $vendors->distributor_name . '</td>';
                                        }
                                        else{
                                            echo '<td></td>';
                                        }
                                        echo '<td class="hidden-1024" style="text-align: center">
                                            <a href="'.base_url().'edit-distributor/'.$vendors->distributor_id.'" class="btn" rel="tooltip" title="Edit"><i class="icon-edit"></i></a>
                                        <a href="'.base_url().'delete-distributor/'.$vendors->distributor_id.'" class="btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a></td>';
                                        echo '</tr>';
                                    } ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
</div>

        </body>
</html>