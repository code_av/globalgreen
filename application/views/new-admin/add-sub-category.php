<div id="content">
    <div id="content-header">
        <div id="breadcrumb"> <a href="<?php echo base_url()?>admin-dashboard" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>>
            <a href="#" title="Go to Home" class="tip-bottom">Sub category</a>
        </div>
        <h1>Sub category</h1>
    </div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5> Sub Category Form</h5>
                </div>
                <div class="widget-content nopadding">
                    <form action="<?php echo base_url()?>submit-sub-category" method="post" class="form-horizontal">
                        <div class="control-group">
                                <label class="control-label">Select category:
                                 </label>


                                <div class="controls">
                                    <select class="span5" name="category">
                                     <?php
                                     //var_dump($data);
                                     foreach($category as $options)
                                     {

                                        echo "<option value=".$options->category_id.">".$options->category_name."</option>";
                                     } ?>
                                    </select>
                                </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Sub Category Name :</label>
                            <div class="controls">
                                <input type="text" class="span5" placeholder="Enter Sub Category Name" name="subcategoryname" id="subcategoryname" required/><span style="color: red;font-size:25px; margin: 3px;">*</span>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid">


        <div class="span12" >
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon"><i class="icon-th"></i></span>
                    <h5>Available Categories</h5>
                </div>
                <div class="widget-content nopadding">
                    <table class="table table-bordered data-table">
                        <thead>
                        <tr>
                            <th>Sub Category Name</th>
                            <th>Category Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="gradeA">
                            <?php
                            //var_dump($data);
                            foreach($sub_category as $options)
                            {   echo '<tr>';
                            if(isset($options->sub_category_name)){
                                echo '<td>'.$options->sub_category_name.'</td>';
                            }
                            else{
                               echo '<td></td>';
                            }
                            if(isset($options->sub_category_name)) {
                                echo '<td>' . $options->category_name . '</td>';
                            }
                            else{
                                echo '<td></td>';
                            }
                                echo '<td><a href="'.base_url().'edit-sub-category/'.$options->sub_category_id.'">EDIT</a>';/*<a href="'.base_url().'delete-sub-category/'.$options->sub_category_id.'">DELETE</a></td>*/?>
                                <?php if ($options->sub_category_status == 0) {
                                echo '| <a href="' . base_url() . 'activate-sub-category/' .$options->sub_category_id . '" onclick="return confirmChangeProductStatus();">ACTIVATE</a></td>
                                    </tr>';
                            } if($options->sub_category_status==1)
                            {
                                echo '| <a href="' . base_url() .'de-activate-sub-category/'. $options->sub_category_id . '" onclick="return confirmChangeProductStatus();">DE-ACTIVATE</a></td>
                                    </tr>';
                            }
                            } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</div>