<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//routes for pages
$route['default_controller'] = 'PagesController/index';
$route['index'] = 'PagesController/index';
$route['404']='PagesController/error';
$route['contact']='PagesController/contact';
$route['about']='PagesController/aboutus';
$route['blog']='PagesController/blog';
$route['blog-single']='PagesController/blog_single';
$route['checkout']='PagesController/checkout';
$route['faq']='PagesController/faq';
$route['projects']='PagesController/projects';
$route['services']='PagesController/services';
$route['services-single']='PagesController/services_single';
$route['shop']='PagesController/shop';
$route['shop-single']='PagesController/shop_single';
$route['shopping-cart']='PagesController/shopping_cart';
$route['team']='PagesController/team';
$route['testimonial']='PagesController/testimonial';
$route['home']='Welcome/home';
$route['wpcDoor']='PagesController/wpcDoor';
$route['mail']='PagesController/mail';
$route['mail_orders']='PagesController/mail_orders';
$route['contactMail']='PagesController/contactMail';
$route['product-category/(:any)/(:num)']='PagesController/subCategoryDetails/$1';
$route['sub_category/(:num)']='PagesController/subCategory';
$route['sub_category/(:num)/(:num)']='PagesController/product_details';
$route['distributor/(:any)/(:num)']='PagesController/distributor';

$route['products/(:any)/(:num)/(:num)'] = 'PagesController/viewProductDetails/$1/$2';
$route['product-subcategory/(:any)/(:num)/(:num)'] = 'PagesController/viewSubCategoryProducts/$1/$2';
$route['distributors-product/(:any)/(:num)'] = 'PagesController/viewDistributorProducts/$1';
$route['add-to-cart/(:num)'] = 'PagesController/addToCart';
$route['cart'] = 'PagesController/cart';
$route['removeCartItem/(:num)'] = 'PagesController/removeCartItem/$1';
$route['modular-kitchen']='PagesController/modularKitchen';
$route['ozone']='PagesController/ozone';
$route['axilam']='PagesController/axilam';
$route['alstone']='PagesController/alstone';
$route['upvc-doors-windows']='PagesController/upvcDoorWindow';
$route['wpc-chaukhat-boards']='PagesController/wpcBoards';
$route['modular-kitchen-overview']='PagesController/overviewModularKitchen';



//end routes for pages
//auth routes
$route['login']['get'] = 'Auth/login';
$route['login']['post'] = 'Auth/login';
$route['logout']['get'] = 'Auth/logout';
$route['change_password']['get'] = 'Auth/change_password';
$route['change_password']['post'] = 'Auth/change_password';


//new admin panel routes
$route['admin-dashboard']='NewAdminController/adminDashboard';
$route['category']='NewAdminController/Category';
$route['submit-category']='NewAdminController/submitCategory';
$route['sub-category']='NewAdminController/subCategory';
$route['submit-sub-category']='NewAdminController/submitSubCategory';
$route['distributor']='NewAdminController/addDistributor';
$route['submit-distributor']='NewAdminController/submitDistributor';
$route['product']='NewAdminController/addProduct';
$route['submit-product']='NewAdminController/submitProduct';
$route['get_sub_category/(:num)']='NewAdminController/get_sub_category';
$route['edit-category/(:num)']='NewAdminController/editCategory';
$route['update-category']='NewAdminController/updateCategory';
$route['activate-category/(:num)']='NewAdminController/activateCategory';
$route['de-activate-category/(:num)']='NewNewAdminController/deactivateCategory';
$route['edit-sub-category/(:num)']='NewAdminController/editSubcategory';
$route['update-sub-category']='NewAdminController/updateSubcategory';
$route['edit-distributor/(:num)']='NewAdminController/editDistributor';
$route['delete-distributor/(:num)']='NewAdminController/deleteDistributor';
$route['update-distributor']='NewAdminController/updateDistributor';
$route['view-details/(:num)']='NewAdminController/viewDetails/$1';
$route['edit-product/(:num)']='NewAdminController/editProduct';
$route['delete-product/(:num)']='NewAdminController/deleteProduct';
$route['update-product']='NewAdminController/updateProduct';
$route['activate-product/(:num)']='NewAdminController/activateProduct';
$route['de-activate-product/(:num)']='NewAdminController/deactivateProduct';
$route['delete-image/(:num)']='NewAdminController/deleteImage';
$route['view-order/(:num)']='NewAdminController/viewOrder';
$route['delete-order/(:num)']='NewAdminController/deleteOrder';
$route['view-all-products']='NewAdminController/viewAllProduct';
$route['view']='NewAdminController/viewProduct';
$route['change-order-status-to-seen/(:num)']='NewAdminController/changeOrderToSeen';
$route['change-order-status-to-unseen/(:num)']='NewAdminController/changeOrderToUnseen';
$route['activate-sub-category/(:num)']='NewAdminController/activateSubcategory';
$route['de-activate-sub-category/(:num)']='NewAdminController/deactivateSubcategory';
$route['activate-category/(:num)']='NewAdminController/activateCategory';
$route['de-activate-category/(:num)']='NewAdminController/deactivateCategory';
$route['change-password']='NewAdminController/changePassword';
$route['slider-image']='NewAdminController/sliderImage';
$route['change-header-slider-image']='NewAdminController/addHeaderSliderImage';
$route['delete_slider/(:num)']='NewAdminController/deleteSlider';
$route['delete_sub_slider/(:num)']='NewAdminController/deleteSubSlider';
$route['add-about-slider-image']='NewAdminController/addSubSlider';
$route['manage-projects']='NewAdminController/projects';
$route['add-project']='NewAdminController/addProject';
$route['delete_project/(:num)']='NewAdminController/deleteProject';
$route['manage-services']='NewAdminController/manageServices';
$route['add-service']='NewAdminController/addService';
$route['delete_service/(:num)']='NewAdminController/deleteService';