<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */
class Role_model extends CI_Model{
    function __construct(){
        //$this->has_one['user'] = array('User_model','role_id','id');
        parent::__construct();
    }
    /*for user validation*/
        public function role($role_id)
        {
            $role=$this->db->get_where('role',array('id'=>$role_id));
            $role_details=$role->result();
            return $role_details;
        }

}
?>