<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Product_image_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }


    public function setSlider($picture_url,$product_id)
    {
        $data = array(
            'product_id' => $product_id,
            'image' => $picture_url
        );
        return $this->db->insert('product_gallery',$data);
    }
    public function getSlider()
    {
        $this->db->select('*');
        $data=$this->db->from('product_gallery')->join('product_details', 'product_details.product_id = product_gallery.product_id');
        $query = $this->db->get();
        $res=$query->result();
        return $res;
    }
public function fetch_by_productid($id)
{$image= $this -> db
    -> select('*')
    -> where('product_id', $id)
    -> get('product_gallery');
    //$res=$this->db->query($query);
    $img=$image->result();
    return $img;
}
    public function delete($id)
    {
        $this->db->delete('product_gallery', array('gallery_id' => $id));
    }


}
?>