<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Project_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

        public function get_project()
        {
            $this->db->select('*');
            $data=$this->db->from('projects');
            $query = $this->db->get();
            $res=$query->result();
            return $res;
        }


    public function addProject($url)
    {
        $project_title=$this->input->post('project_title');
        $project_subtitle=$this->input->post('project_subtitle');
        $data = array(
            'project_title'=>  $project_title ,
            'project_subtitle'=>  $project_subtitle ,
            'project_image'=>  $url);
        $this->db->insert('projects',$data);
        return;
    }

}
?>