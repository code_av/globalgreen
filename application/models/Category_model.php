<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Category_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

        public function add()
        {
             $categorycode=$this->input->post('categorycode');
             $categoryname=$this->input->post('categoryname');
             $data=array(
                 'category_code'=>$categorycode,
                 'category_name'=>$categoryname,
                 'status'=>1
             );
            $this->db->insert('category',$data);
            $id=$this->db->insert_id();
            return $id;
           }
           public function fetch()
           {
               $query ='select * from category';
               $res2=$this->db->query($query);
               //$this->db->order_by("category_id", "desc");
               $res=$res2->result();
               return $res;
              // var_dump( $res);
           }
            public function fetchCat()
            {
                $this->db->select('*');
                $data=$this->db->from('category');
                $this->db->order_by("category_id","desc");
                $query = $this->db->get();
                $res=$query->result();
                return $res;

            }
           public function fetch_by_categoryid($id)
           {
               $query='select * from category where category_id='.$id.'';
               $category=$this->db->query($query);
               $res=$category->result();
               return $res;
           }
           public function update()
           {
               $categorycode=$this->input->post('categorycode');
               $categoryname=$this->input->post('categoryname');
               $categoryid=$this->input->post('category_id');

               $data = array(
                       'category_code'=>  $categorycode ,
                        'category_name'=>  $categoryname);
                $this->db->where('category_id', $categoryid);
                $this->db->update('category', $data);

           }
           public function delete($id)
           {

               $this->db->delete('category', array('category_id' => $id));
           }
           public function activateCategory($id)
           {
               $this->db->where('category_id', $id);
               $this->db->update('category', array('status'=>1));


           }
    public function deactivateCategory($id)
    {
        $this->db->where('category_id', $id);
        $this->db->update('category', array('status'=>0));


    }
    public function fetch_by_subcategoryid($id)
    {
        $this->db->select('status');
        $data=$this->db->from('category');
        $this->db->join('sub_category', 'sub_category.category_id = category.category_id','left');
        $this->db->where('sub_category_id=', $id);
        $query = $this->db->get();
        $res=$query->result();
        return $res;
    }

}
?>