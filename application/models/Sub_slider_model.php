<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Sub_slider_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

        public function get_sub_slider()
        {
            $this->db->select('*');
            $data=$this->db->from('sub_slider');
/*            $this->db->order_by("order_id","desc");*/
            $query = $this->db->get();

            $res=$query->result();
          /*  var_dump($res);
            die();
          */  return $res;
        }
    public function addSubSlider($url)
    {

        $data = array(
            'sub_slider_image'=>  $url);
        $this->db->insert('sub_slider',$data);
        return;
    }

}
?>