<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Main_slider_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

        public function get_slider()
        {
            $this->db->select('*');
            $data=$this->db->from('main_slider');
/*            $this->db->order_by("order_id","desc");*/
            $query = $this->db->get();

            $res=$query->result();
          /*  var_dump($res);
            die();
          */  return $res;
        }
    public function addSlider($url)
    {

        $slider_text=$this->input->post('slider_text');
        $data = array(
            'slider_text'=>  $slider_text ,
            'slider_image'=>  $url);
        $this->db->insert('main_slider',$data);
         return;


    }

}
?>