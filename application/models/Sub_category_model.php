<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Sub_category_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

        public function add()
        {

            $category=$this->input->post('category');
            $subcategoryname=$this->input->post('subcategoryname');
            $data=array(
                'category_id'=>$category,
                'sub_category_name'=>$subcategoryname,
                'sub_category_status'=>1
            );
            $this->db->insert('sub_category',$data);
            return;

           }
        public function fetch()
        {
            $this->db->select('*');
            $data=$this->db->from('sub_category')->join('category', 'category.category_id = sub_category.category_id');
            $query = $this->db->get();
              $this->db->order_by("sub_category_id", "desc");
            $res=$query->result();
            return $res;


            }
    public function fetch_sub($id) {
        $res = $this -> db
            -> select('*')
            -> where('category_id', $id)
            -> get('sub_category');
        //$res=$this->db->query($query);
            return $res->result();

    }
    public function fetch_by_sub_category_id($id)
    {
        $res = $this -> db
            -> select('*')
            -> where('sub_category_id', $id)
            -> get('sub_category');
        //$res=$this->db->query($query);
        $s= $res->result();
        return $s;

    }
    public function update()
    {

        $subcategoryname=$this->input->post('subcategoryname');
        $subcategoryid=$this->input->post('subcategoryid');

        $data = array(
            'sub_category_name'=>  $subcategoryname);
        $this->db->where('sub_category_id', $subcategoryid);
        $this->db->update('sub_category', $data);

    }
    public function delete($id)
    {
        $this->db->delete('sub_category', array('sub_category_id' => $id));
    }
    public function activate($id)
    {
        $this->db->where('sub_category_id', $id);
        $this->db->update('sub_category', array('sub_category_status'=>1));
    }
    public function deactivate($id)
    {
        $this->db->where('sub_category_id', $id);
        $this->db->update('sub_category', array('sub_category_status'=>0));
    }
    public function deactivateSubcategoryByCategoryId($id)
    {
        $this->db->where('category_id', $id);
        $this->db->update('sub_category', array('sub_category_status'=>0));

    }

}
?>