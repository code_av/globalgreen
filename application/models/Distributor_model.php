<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Distributor_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

        public function add()
        {
             $name=$this->input->post('distributorname');
             $data=array(
                 'distributor_name'=>$name );
            $this->db->insert('distributor',$data);
            return;
           }
           public function fetch()
           {
               $query ='select * from distributor';
               $res2=$this->db->query($query);
               $res=$res2->result();
               return $res;
               $this->db->order_by('distributor_id','desc');
              // var_dump( $res);
           }

           public function fetch_by_distributorid($id)
           {
               $res = $this -> db
                   -> select('*')
                   -> where('distributor_id', $id)
                   -> get('distributor');
               //$res=$this->db->query($query);
               $s= $res->result();
               return $s;

           }
    public function update()
    {

        $distributorname=$this->input->post('distributorname');
        $distributorid=$this->input->post('distributorid');

        $data = array(
            'distributor_name'=>  $distributorname);
        $this->db->where('distributor_id', $distributorid);
        $this->db->update('distributor', $data);

    }
    public function delete($id)
    {
        $this->db->delete('distributor', array('distributor_id' => $id));
    }

}
?>