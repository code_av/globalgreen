<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Product_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

        public function add()
        {
             $category_id=$this->input->post('category');
             $sub_category_id=$this->input->post('subcategory');
             $distributor_id=$this->input->post('distributor');
             $name=$this->input->post('productname');
             $size=$this->input->post('productsize');
             $weight=$this->input->post('productweight');
             $price=$this->input->post('productprice');
             $description=$this->input->post('productdescription');
             $material=$this->input->post('productmaterial');
             $product_code=$this->input->post('productcode');
             $data=array(
                'category_id'=>$category_id,
                'sub_category_id'=>$sub_category_id,
                'distributor_id'=>$distributor_id,
                'product_code'=>$product_code,
                'name'=>$name,
                'size'=>$size,
                'weight'=>$weight,
                'price'=>$price,
                'description'=>$description,
                'material'=>$material,
                'product_status'=>1
             );
            $productDetail = $this->db->insert('product_details',$data);
            $id=$this->db->insert_id();

            return $id;
           }
           public function fetch()
           {
               $this->db->select('*');
               $data=$this->db->from('product_details');
                   $this->db->join('category', 'category.category_id = product_details.category_id','left');
                   $this->db->join('sub_category', 'sub_category.sub_category_id = product_details.sub_category_id');
                   $this->db->join('distributor', 'distributor.distributor_id = product_details.distributor_id');
                   $this->db->order_by("product_id","desc");
                   $query = $this->db->get();

               $res=$query->result();
               return $res;
           }
    public function fetch_productId()
    {
        $que="select * from product_details";
        $query=$this->db->query($que);
        $res=$query->result();
        //var_dump($res);
        return $res;
    }
    public function fetch_details($id)
    {
        $this->db->select('*');
        $data=$this->db->from('product_details');
        $this->db->join('product_details', 'product_details.gallery_id = product_gallery.gallery_id');
        $this->db->where('product_id=', $id);
        $query = $this->db->order_by('product_gallery.gallery_id');
        $res=$query->result();
        return $res;
    }

    public function fetch_product_by_productid($id)
    {
        $que="select * from product_details where product_id=$id";
        $query=$this->db->query($que);
        $res=$query->result();
//        var_dump($res);
        return $res;
    }

    public function fetchProductDetailWithGallery($id)
    {
        $query = $this->db->select('*')->from('product_details')->where('category_id',$id)->get()->result_array();
        $index = 0;
        foreach ($query as $item) {
            $query[$index]["images"] = $this->getGallery($item['product_id']);
            $index++;
        }
        return $query;
    }
    public function getGallery($product_details_id)
    {
        return $this->db->select('*')->from('product_gallery')->where('product_id' , $product_details_id)->get()->result_array();
    }

    public function fetchProductDetails($pro_id)
    {
        $query = $this->db->select('*')
            ->from('product_details')
            ->join('distributor', 'distributor.distributor_id =product_details.distributor_id')
            ->where('product_id',$pro_id)
            ->get()->result_array();
        $index = 0;
        foreach ($query as $item) {
            $query[$index]["images"] = $this->getGallery($item['product_id']);
            $index++;
        }
        return $query;
    }

    public function fetchSubCategoryProducts($sub_cat_id)
    {
        $query = $this->db->select('*')->from('product_details')->where('sub_category_id',$sub_cat_id)->get()->result_array();
        $index = 0;
        foreach ($query as $item) {
            $query[$index]["images"] = $this->getGallery($item['product_id']);
            $index++;
        }
        return $query;
    }

    public function fetchDistributorProducts($dis_id)
    {
        $query = $this->db->select('*')->from('product_details')->where('distributor_id',$dis_id)->get()->result_array();
        $index = 0;
        foreach ($query as $item) {
            $query[$index]["images"] = $this->getGallery($item['product_id']);
            $index++;
        }
        return $query;
    }
    public function fetch_by_categoryid($id)
    {
        $que="select * from product_details where category_id=$id";
        $query=$this->db->query($que);
        $res=$query->result();
//        var_dump($res);
        return $res;
    }
    public function fetch_by_subcategoryid($id)
    {
        $que="select * from product_details where sub_category_id=$id";
        $query=$this->db->query($que);
        $res=$query->result();
        //var_dump($res);
        return $res;
    }
    public function update($id)
    {
        $productid=$this->input->post('productid');
        $name=$this->input->post('productname');
        $size=$this->input->post('productsize');
        $weight=$this->input->post('productweight');
        $price=$this->input->post('productprice');
        $description=$this->input->post('productdescription');
        $material=$this->input->post('productmaterial');
        $product_code=$this->input->post('productcode');
        $distributor_id=$this->input->post('distributor');
        $data=array(
            'product_code'=>$product_code,
            'name'=>$name,
            'size'=>$size,
            'weight'=>$weight,
            'price'=>$price,
            'description'=>$description,
            'distributor_id'=>$distributor_id,
            'material'=>$material,
        );
        $this->db->where('product_id', $productid);
        $this->db->update('product_details', $data);
        }
        public function delete($id)
        {
            $this->db->delete('product_details', array('product_id' => $id));
        }
        public function activateProduct($id)
        {
            $this->db->where('category_id', $id);
            $this->db->update('product_details', array('product_status'=>1));
        }

    public function deactivateProduct($id)
    {
        $this->db->where('category_id', $id);
        $this->db->update('product_details', array('product_status'=>0));
    }
    public function deactivateProductByProductId($id)
    {

        $this->db->where('product_id', $id);
        $this->db->update('product_details', array('product_status'=>0));
    }

    public function activateProductByProductId($id)
    {


        $this->db->where('product_id', $id);
        $this->db->update('product_details', array('product_status'=>1));
    }
    public function activateProductBySubcategoryId($id)
    {
        $this->db->where('sub_category_id', $id);
        $this->db->update('product_details', array('product_status'=>1));
    }
    public function deactivateProductBySubcategoryId($id)
    {
        $this->db->where('sub_category_id', $id);
        $this->db->update('product_details', array('product_status'=>0));

    }


}
?>