<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Order_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

        public function fetch_orders()
        {
            $this->db->select('*');
            $data=$this->db->from('orders');
            $this->db->order_by("order_id","desc");
            $query = $this->db->get();

            $res=$query->result();
          /*  var_dump($res);
            die();
          */  return $res;


        }
        public function add_orders()
        {

            $name=$this->input->post('fname');
            $phone=$this->input->post('phone');
            $address=$this->input->post('address');
            $message=$this->input->post('message');
            $data=array(
                'phone'=>$phone,
                'address'=>$address,
                'message'=>$message,
                'username'=>$name
            );
            $this->db->insert('orders',$data);
            $id=$this->db->insert_id();
            return $id;

        }
    public function fetchByOrderId($id)
    {
        $que="select * from orders where order_id=$id";
        $query=$this->db->query($que);
        $res=$query->result();
        //var_dump($res);
        return $res;


    }
    public function changeToSeen($id)
    {
        $this->db->where('order_id', $id);
        $this->db->update('orders', array('order_status'=>1));
        return;
    }
    public function changeToUnseen($id)
    {
        $this->db->where('order_id', $id);
        $this->db->update('orders', array('order_status'=>0));
        return;
    }



}
?>