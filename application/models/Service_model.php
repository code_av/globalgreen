<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Service_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

        public function get_Services()
        {
            $this->db->select('*');
            $data=$this->db->from('services');
            $query = $this->db->get();
            $res=$query->result();
            return $res;
        }


    public function addService($url)
    {
        $service_title=$this->input->post('service_title');
        $service_description=$this->input->post('service_description');
        $data = array(
            'service_title'=>  $service_title ,
            'service_description'=>  $service_description ,
            'service_icon'=>  $url);
        $this->db->insert('services ',$data);
        return;
    }

}
?>