<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Order_details_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

        public function add_order_detail($order_id)
        {
            $session_data = $this->session->userdata('cart');
            if(count($session_data) > 0) {
                foreach ($session_data as $key => $value) {

                    $data=array(
                        'order_id'=>$order_id,
                        'product_id'=>$value['product_id']
                    );
                    $this->db->insert('order_details',$data);

                }
            }
        }
        public function fetchOrderDetails()
        {
            $this->db->select('*');
            $this->db->from('order_details');
            $this->db->join('orders', 'orders.order_id = order_details.order_id');
            $this->db->join('product_details', 'product_details.product_id = order_details.product_id');
            $this->db->order_by("order_detail_id","desc");
            $query = $this->db->get();
            $res=$query->result();
            return $res;
        }
        public function fetchByOrderDetailId($id)
        {
            $this->db->select('*');
            $this->db->from('order_details');
            $this->db->join('orders', 'orders.order_id = order_details.order_id');
            $this->db->join('product_details', 'product_details.product_id = order_details.product_id');
            $this->db->where('order_detail_id=', $id);
            $this->db->order_by("order_detail_id","desc");
            $query = $this->db->get();
            $res=$query->result();
            return $res;
        }
    public function fetchByOrderId($id)
    {
        $this->db->select('*');
        $this->db->from('order_details');
        /*$this->db->join('orders', 'orders.order_id = order_details.order_id');*/
        $this->db->join('product_details', 'product_details.product_id = order_details.product_id');
        $this->db->where('order_id=', $id);
        $query = $this->db->get();
        $res=$query->result();
        return $res;
    }



}
?>