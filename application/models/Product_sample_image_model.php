<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Product_sample_image_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }


    public function setSampleImage($picture_url)
    {
        $colorCode=$this->input->post('colorCode');
        $data = array(
            'product_id' => $this->input->post('productId'),
            'sample_image' => $picture_url,
            'color_code' => $colorCode

        );
        return $this->db->insert('product_sample',$data);
    }
    public function getSlider()
    {
      /*  $this->db->select('*');
        $data=$this->db->from('sub_category')->join('category', 'category.id = sub_category.category_id');
        $query = $this->db->get();
        $res=$query->result();
        return $res;*/



        $this->db->select('*');
        $data=$this->db->from('product_sample')->join('product_details', 'product_details.id = product_sample.product_id');
        $query = $this->db->get();
        $res=$query->result();
        return $res;
        /*$query = $this->db->get('product_gallery');
        return $query->result();
 */   }


}
?>