<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//routes for pages
$route['default_controller'] = 'PagesController/index';
$route['index'] = 'PagesController/index';
$route['404']='PagesController/error';
$route['contact']='PagesController/contact';
$route['about']='PagesController/aboutus';
$route['blog']='PagesController/blog';
$route['blog-single']='PagesController/blog_single';
$route['checkout']='PagesController/checkout';
$route['faq']='PagesController/faq';
$route['projects']='PagesController/projects';
$route['services']='PagesController/services';
$route['services-single']='PagesController/services_single';
$route['shop']='PagesController/shop';
$route['shop-single']='PagesController/shop_single';
$route['shopping-cart']='PagesController/shopping_cart';
$route['team']='PagesController/team';
$route['testimonial']='PagesController/testimonial';
$route['home']='Welcome/home';
$route['wpcDoor']='PagesController/wpcDoor';
$route['mail']='PagesController/mail';
$route['mail_orders']='PagesController/mail_orders';
$route['contactMail']='PagesController/contactMail';
$route['product-category/(:any)/(:num)']='PagesController/subCategoryDetails/$1';
$route['sub_category/(:num)']='PagesController/subCategory';
$route['sub_category/(:num)/(:num)']='PagesController/product_details';
$route['distributor/(:any)/(:num)']='PagesController/distributor';

$route['products/(:any)/(:num)/(:num)'] = 'PagesController/viewProductDetails/$1/$2';
$route['product-subcategory/(:any)/(:num)/(:num)'] = 'PagesController/viewSubCategoryProducts/$1/$2';
$route['distributors-product/(:any)/(:num)'] = 'PagesController/viewDistributorProducts/$1';
$route['add-to-cart/(:num)'] = 'PagesController/addToCart';
$route['cart'] = 'PagesController/cart';
$route['removeCartItem/(:num)'] = 'PagesController/removeCartItem/$1';
$route['modular-kitchen']='PagesController/modularKitchen';
$route['ozone']='PagesController/ozone';
$route['axilam']='PagesController/axilam';
$route['alstone']='PagesController/alstone';



//end routes for pages
//auth routes
$route['login']['get'] = 'Auth/login';
$route['login']['post'] = 'Auth/login';
$route['logout']['get'] = 'Auth/logout';
$route['change_password']['get'] = 'Auth/change_password';
$route['change_password']['post'] = 'Auth/change_password';


//admin routes
/*$route['admin-dashboard']='AdminController/adminDashboard';
$route['loginUser']='AdminController/loginUser';
$route['add-category']='AdminController/addCategory';
$route['submit-category']='AdminController/submitCategory';
$route['add-sub-category']='AdminController/addSubCategory';
$route['submit-sub-category']='AdminController/submitSubCategory';
$route['add-distributor']='AdminController/addDistributor';
$route['submit-distributor']='AdminController/submitDistributor';
$route['add-product']='AdminController/addProduct';
$route['submit-product']='AdminController/submitProduct';
$route['get_sub_category/(:num)']='AdminController/get_sub_category';
$route['add-product-images']='AdminController/addProductImages';
$route['submit-image']='AdminController/submitImage';
$route['delete_gallery/(:num)']='AdminController/deleteImage';
$route['add-product-sample-images']='AdminController/productSampleImage';
$route['submit-sample-image']='AdminController/submitSampleImage';
$route['view-details/(:num)']='AdminController/viewDetails/$1';
$route['view-order/(:num)']='AdminController/viewOrder';
$route['delete-order/(:num)']='AdminController/deleteOrder';
$route['view-all-products']='AdminController/viewAllProduct';
$route['view']='AdminController/viewProduct';*/

//edit update and delete routes
/*$route['edit-category/(:num)']='AdminController/editCategory';
$route['delete-category/(:num)']='AdminController/deleteCategory';
$route['update-category']='AdminController/updateCategory';
$route['edit-sub-category/(:num)']='AdminController/editSubcategory';
$route['delete-sub-category/(:num)']='AdminController/deleteSubcategory';
$route['update-sub-category']='AdminController/updateSubcategory';
$route['edit-distributor/(:num)']='AdminController/editDistributor';
$route['delete-distributor/(:num)']='AdminController/deleteDistributor';
$route['update-distributor']='AdminController/updateDistributor';
$route['edit-product/(:num)']='AdminController/editProduct';
$route['delete-product/(:num)']='AdminController/deleteProduct';
$route['update-product']='AdminController/updateProduct';
$route['edit-gallery']='AdminController/editGallery';
$route['delete-image/(:num)/(:num)']='AdminController/deleteImage';*/
/*
$route['activate-category/(:num)']='AdminController/activateCategory';
$route['de-activate-category/(:num)']='AdminController/deactivateCategory';*/
/*$route['activate-product/(:num)']='AdminController/activateProduct';
$route['de-activate-product/(:num)']='AdminController/deactivateProduct';
$route['activate-sub-category/(:num)']='AdminController/activateSubcategory';
$route['de-activate-sub-category/(:num)']='AdminController/deactivateSubcategory';*/



//new admin panel routes
$route['admin-dashboard']='NewAdminController/adminDashboard';
$route['category']='NewAdminController/Category';
$route['submit-category']='NewAdminController/submitCategory';
$route['sub-category']='NewAdminController/subCategory';
$route['submit-sub-category']='NewAdminController/subCategory';
$route['distributor']='NewAdminController/addDistributor';
$route['submit-distributor']='AdminController/submitDistributor';
$route['product']='NewAdminController/addProduct';
$route['submit-product']='NewAdminController/submitProduct';
$route['get_sub_category/(:num)']='NewAdminController/get_sub_category';
$route['edit-category/(:num)']='NewAdminController/editCategory';
$route['update-category']='NewAdminController/updateCategory';
$route['activate-category/(:num)']='NewAdminController/activateCategory';
$route['de-activate-category/(:num)']='NewNewAdminController/deactivateCategory';
$route['edit-sub-category/(:num)']='NewAdminController/editSubcategory';
$route['update-sub-category']='NewAdminController/updateSubcategory';
$route['edit-distributor/(:num)']='NewAdminController/editDistributor';
$route['delete-distributor/(:num)']='NewAdminController/deleteDistributor';
$route['update-distributor']='NewAdminController/updateDistributor';
$route['view-details/(:num)']='NewAdminController/viewDetails/$1';
$route['edit-product/(:num)']='NewAdminController/editProduct';
$route['delete-product/(:num)']='NewAdminController/deleteProduct';
$route['update-product']='NewAdminController/updateProduct';
$route['activate-product/(:num)']='NewAdminController/activateProduct';
$route['de-activate-product/(:num)']='NewAdminController/deactivateProduct';
$route['delete-image/(:num)']='NewAdminController/deleteImage';
$route['view-order/(:num)']='NewAdminController/viewOrder';
$route['delete-order/(:num)']='NewAdminController/deleteOrder';
$route['view-all-products']='NewAdminController/viewAllProduct';
$route['view']='NewAdminController/viewProduct';
$route['change-order-status-to-seen/(:num)']='NewAdminController/changeOrderToSeen';
$route['change-order-status-to-unseen/(:num)']='NewAdminController/changeOrderToUnseen';
$route['activate-sub-category/(:num)']='NewAdminController/activateSubcategory';
$route['de-activate-sub-category/(:num)']='NewAdminController/deactivateSubcategory';
$route['activate-category/(:num)']='NewAdminController/activateCategory';
$route['de-activate-category/(:num)']='NewAdminController/deactivateCategory';
$route['change-password']='NewAdminController/changePassword';
/*$route['update-password']='Auth/change_password';*/

